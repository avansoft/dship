export default {
	port: 3000,
	domain: 'localhost',
	baseURL: 'http://localhost',
	transactionalEmailfrom: 'no-reply@dship.ru',
	siteName: 'dship.ru',
	staticPath: '/home/nodejs/dship/public',
	viewsPath: '/home/nodejs/dship/public/views',
	defaultCacheLT: 10000,
	langs: ['ru', 'en'],
	defaultLang: 'ru',
	userSessionLT: 3600 * 48, //seconds
	favicon: {
		maxAgeMs: 60 * 60 * 24 * 365 * 1000, // 1 year
		filePath: '/home/nodejs/dship/src/assets',
	},
	password: {
		saltLength: 32,
		iterations: 1000,
		hashLength: 64,
		algorithm: 'sha512',
	},
	static: {
		issues: '/home/nodejs/dship/static/issues',
	},
	fileStorage: {
		bucketName: 'dship-support',
		bucketId: 1,
	},
	modules: {
		support: {
			antifludLimit: 10,
		}
	},
	presets: {
		issueImageTeaser: {
			// Size: width = default, height = 300px
			params: 's:0:150',
			decoding: 'async',
			loading: 'lazy',
			extension: '.jpg',
		}
	},
	services: {
		mailer: {
			emailTemplatesPath: '/home/nodejs/dship/services/mailer/emails',
			mailgun: {
				username: 'DSHIP',
				key: '260e91b48e1ffa96506249b575db5a1a-4b1aa784-19399575',
				url: 'https://api.eu.mailgun.net',
			},
			postgres: {
				host: 'postgres',
				port: 5432,
				user: 'srv_mailer',
				database: 'dship',
				password: '111',
				// Max of idle-transactions
				max: 10,
			}
		},
		uploader: {
			bucketName: 'dship-support',
			bucketId: 1,
			minio: {
				// Имя docker-сервиса или localhost
				endPoint: 'minio',
				port: 9000,
				useSSL: false,
				accessKey: 'dship',
				secretKey: 'miniotest',
			},
			postgres: {
				host: 'postgres',
				port: 5432,
				user: 'uploader',
				database: 'dship',
				password: '222',
				max: 10,
			},
		},
	},
	drivers: {
		cashDatabase: {
			//host: '127.0.0.1',
			url: 'redis://redis:6379',
			port: 6379,
		},
		storeDatabase: {
			host: 'postgres',
			port: 5432,
			user: 'dship',
			database: 'dship',
			password: '111',
			// Max of idle-transactions
			max: 20,
		}
	},
};
