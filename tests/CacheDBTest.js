import { CacheDB } from "../apps/CacheDB.js";

const config = {
    host: '127.0.0.1',
    port: 6379,
}

const cache = new CacheDB(config);

const a = 222, b = 333;

console.log(typeof a)

cache.saveString(a, b, 50000)
    .then(res => console.log(res))
    .catch(e => console.log(e));
