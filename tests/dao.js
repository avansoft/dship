class Dao {
    static sqlConstructor(entity, type, token, arg) {
        function getCallOperator() {
            switch (type) {
                case 'proc': return 'CALL';
                case 'func': return 'SELECT * FROM';
                default: throw new Error('Uncnown type');
            }
        }

        function argFormatter() {
            switch (typeof arg) {
                case 'object': return `'${JSON.stringify(arg)}'`;
                case 'string': return `'${arg}'`;
                case 'number': return arg;
                case 'undefined': return '';
                default: throw new Error('Unsupported argument type');
            }
        }

        return `${getCallOperator()} dao_${entity}.${token}(${argFormatter()});`;
    }

    static parseTable(args) {
        console.log(this.sqlConstructor(...args));
    }
}

const cortage = ['subject', 'func', 'get_subject_by_id'];
Dao.parseTable(cortage);


