import { BasePage } from '../../../core/lib/BasePage.js';
import { IssueListBlockServ } from '../../blocks/IssueListBlock/IssueListBlockServ.js';

export class IssueListPageServ extends BasePage {
	constructor(g, ctx, props) {
		super(g, ctx, props);
		this.template = 'IssueListPage';
		this.httpStatus = 200;
		this.blocks = [IssueListBlockServ];
		this.dict = {
			title: {
				ru: 'Мои запросы',
				en: 'My issues',
			},
		};
	}
}