import { IssueListBlockFront } from "../../blocks/IssueListBlock/IssueListBlockFront.js";

const issueListBlock = document.querySelector('.js-b-issueList');
if (issueListBlock) {
    new IssueListBlockFront(issueListBlock).init();
}
