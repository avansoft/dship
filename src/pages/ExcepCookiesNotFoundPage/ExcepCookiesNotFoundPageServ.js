import { BasePage } from "../../../core/lib/BasePage.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";

export class ExcepCookiesNotFoundPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'ExcepCookiesNotFoundPage';
        this.httpStatus = 400;
        this.blocks = [];
        this.data = {
            btnLink: SignupURI.signup,
        }
        this.dict = {
            title: {
                ru: 'Ошибка регистрации',
                en: 'Signup error',
            },
            header: {
                ru: 'Ошибка регистрации',
                en: 'Signup error',
            },
            content: {
                ru: 'Возможно, браузером были сброшены файлы cookies. Попробуйте снова перейти по ссылке подтверждения, либо начните регистрацию с начала.',
                en: 'Perhaps the browser was reset the cookies files. Try again by reference to the confirmation link, or start registration from the beginning.',
            },
            btnTitle: {
                ru: 'Повторить регистрацию',
                en: 'Try signup again',
            },
            errCodeLabel: {
                ru: 'Код ошибки',
                en: 'Error code',
            },
        }
    }
}