import { BasePage } from "../../../core/lib/BasePage.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";

export class ProfilePageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'ProfilePage';
        this.httpStatus = 200;
        this.blocks = [];
        this.data = {
            passwordUpdateLink: UserPasswordURI.passwordUpdatePage,
        }
        this.dict = {
            title: {
                ru: 'Профиль',
                en: 'Profile',
            },
            header: {
                ru: 'Профиль',
                en: 'Profile',
            },
            firstNameLabel: {
                ru: 'Ваше имя',
                en: 'First name',
            },
            emailLabel: {
                ru: 'Электронная почта',
                en: 'E-mail',
            },
            mobilePhoneLabel: {
                ru: 'Телефон',
                en: 'Mobile phone',
            },
            passwordLabel: {
                ru: 'Пароль',
                en: 'Password',
            },
            logoutLinkTitle: {
                ru: 'Выйти из аккаунта',
                en: 'Logout',
            },
        }
    }
}