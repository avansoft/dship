import { InputViewerComponent } from "../../basics/components/InputViewerComponent/InputViewerComponent.js";
import { AsyncComponent } from "../../basics/components/AsyncComponent/AsyncComponent.js";
import { UserSessionURI } from "../../../actions/user-session/UserSessionURI.js";
import Client from "../../basics/Client.js";

const phoneEl = document.querySelector('.js-viewbox-mobilePhone');
if (phoneEl) {
    new InputViewerComponent(phoneEl);
}

const asyncEl = document.querySelector('.js-c-async');
if (asyncEl) {
    new AsyncComponent({
        el: asyncEl,
        command: () => {
            const url = Client.getEndpoint(UserSessionURI.logoutAction);
            Client.send({ action: 'logout' }, url);
        }
    })
}
