import { FormPasswordUpdateBlockFront } from "../../blocks/FormPasswordUpdateBlock/FormPasswordUpdateBlockFront.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";

// FeedbackFormBlock init
const formEl = document.querySelector('.js-b-passwordUpdate');
if (formEl) FormPasswordUpdateBlockFront.init(formEl, UserPasswordURI.passwordUpdateAction);