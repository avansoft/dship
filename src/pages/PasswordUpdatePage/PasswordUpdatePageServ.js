import { BasePage } from "../../../core/lib/BasePage.js";
import { FormPasswordUpdateBlockServ } from "../../blocks/FormPasswordUpdateBlock/FormPasswordUpdateBlockServ.js";

export class PasswordUpdatePageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'PasswordUpdatePage';
        this.httpStatus = 200;
        this.blocks = [FormPasswordUpdateBlockServ];
        this.dict = {
            title: {
                ru: 'Новый пароль',
                en: 'New password',
            },
        }
    }
}