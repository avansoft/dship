import { FeedbackFormBlockFront } from "../../blocks/FeedbackFormBlock/FeedbackFormBlockFront.js";

// FeedbackFormBlock init
const feedbackFormBlockEl = document.querySelector('.js-b-feedbackForm');
if (feedbackFormBlockEl) FeedbackFormBlockFront.init(feedbackFormBlockEl);