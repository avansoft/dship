import { BasePage } from "../../../core/lib/BasePage.js";
import { FeedbackFormBlockServ } from "../../blocks/FeedbackFormBlock/FeedbackFormBlockServ.js"

export class FeedbackPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'FeedbackPage';
        this.httpStatus = 200;
        this.blocks = [FeedbackFormBlockServ];
        this.dict = {
            title: {
                ru: 'Обратная связь',
                en: 'Feedback',
            },
        }
    }
}