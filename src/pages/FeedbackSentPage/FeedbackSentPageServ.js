import { BasePage } from "../../../core/lib/BasePage.js";

export class FeedbackSentPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'FeedbackSentPage';
        this.httpStatus = 200;
        this.blocks = [];
        this.dict = {
            title: {
                ru: 'Сообщение получено',
                en: 'Successfully sent',
            },
            message: {
                ru: 'Сообщение получено, мы скоро ответим',
                en: 'Message received, we\'ll answer soon ',
            },
        }
    }
}