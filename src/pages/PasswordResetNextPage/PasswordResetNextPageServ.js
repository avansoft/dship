import { BasePage } from "../../../core/lib/BasePage.js";

export class PasswordResetNextPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'PasswordResetNextPage';
        this.httpStatus = 200;
        this.blocks = [];
        this.dict = {
            title: {
                ru: 'Смена пароля',
                en: 'Password change',
            },
            header: {
                ru: 'Почти готово!',
                en: 'Almost ready!',
            },
            text: {
                ru: 'Мы отправили вам письмо со ссылкой для смены пароля. Если вы не найдете его во входящих, прожалуйста, проверьте папку "Спам".',
                en: 'We sent you a letter with reference to password change. If you do not find it in the incoming, vault, check the "Spam" folder.',
            },
            btnTitleText: {
                ru: 'Перейти в почту',
                en: 'Go to inbox',
            },
        }
    }
}