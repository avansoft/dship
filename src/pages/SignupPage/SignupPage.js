import { FormSignupBlockFront } from "../../blocks/FormSignupBlock/FormSignupBlockFront.js";

const signupBlock = document.querySelector('.js-b-signup');
if (signupBlock) FormSignupBlockFront.init(signupBlock);