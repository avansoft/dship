import { BasePage } from "../../../core/lib/BasePage.js";
import { FormSignupBlockServ } from "../../blocks/FormSignupBlock/FormSignupBlockServ.js";
import { UserSessionURI } from "../../../actions/user-session/UserSessionURI.js";

export class SignupPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'SignupPage';
        this.httpStatus = 200;
        this.blocks = [FormSignupBlockServ];
        this.data = {
            loginPageLink: UserSessionURI.loginPage,
        }
        this.dict = {
            title: {
                ru: 'Регистрация',
                en: 'Signup',
            },
            loginLinkText: {
                ru: 'Уже зарегистрированы?',
                en: 'Already registered?',
            },
        }
    }
}