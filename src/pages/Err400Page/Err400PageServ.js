import { BasePage } from "../../../core/lib/BasePage.js";

export class Err400PageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'Err400Page';
        this.httpStatus = 400;
        this.blocks = [];
        this.dict = {
            header: {
                ru: 'Некорректный запрос',
                en: 'Bad Request',
            },
            message1: {
                ru: 'Вы попали на эту страницу, потому что ссылка, по которой вы перешли, содержит ошибки.',
                en: 'You fell on this page, because the link you switched to contain errors.',
            },
        }
    }
}