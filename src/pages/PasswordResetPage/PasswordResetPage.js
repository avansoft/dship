import { FormPasswordResetBlockFront } from "../../blocks/FormPasswordResetBlock/FormPasswordResetBlockFront.js";

// FeedbackFormBlock init
const formEl = document.querySelector('.js-b-passwordReset');
if (formEl) FormPasswordResetBlockFront.init(formEl);