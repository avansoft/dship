import { BasePage } from "../../../core/lib/BasePage.js";
import { FormPasswordResetBlockServ } from "../../blocks/FormPasswordResetBlock/FormPasswordResetBlockServ.js";

export class PasswordResetPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'PasswordResetPage';
        this.httpStatus = 200;
        this.blocks = [FormPasswordResetBlockServ];
        this.dict = {
            title: {
                ru: 'Сброс пароля',
                en: 'Password reset',
            },
        }
    }
}