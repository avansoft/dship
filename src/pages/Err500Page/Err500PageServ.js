import { BasePage } from "../../../core/lib/BasePage.js";

export class Err500PageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'Err500Page';
        this.httpStatus = 500;
        this.blocks = [];
        this.dict = {
            header: {
                ru: 'Увы, но даже сайты ошибаются :(',
                en: 'Site broken :(',
            },
            message1: {
                ru: 'Мы уже в курсе проблемы и скоро все починим.',
                en: 'We are already aware of the problems and will soon be reed.',
            },
            message2: {
                ru: 'Попробуйте обновить страницу чуть позже.',
                en: 'Please, try again later.',
            },
        }
    }
}