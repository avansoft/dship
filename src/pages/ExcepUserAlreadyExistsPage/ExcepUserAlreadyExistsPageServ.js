import { BasePage } from "../../../core/lib/BasePage.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";

export class ExcepUserAlreadyExistsPage extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'ExcepUserAlreadyExistsPage';
        this.httpStatus = 400;
        this.blocks = [];
        this.data = {
            btnLink: UserPasswordURI.passwordReset,
        }
        this.dict = {
            title: {
                ru: 'Ошибка регистрации',
                en: 'Signup error',
            },
            header: {
                ru: 'Пользователь уже зарегистрирован',
                en: 'User has been registered',
            },
            content: {
                ru: 'Кто-то (или вы) уже зарегистрировался с указанным адресом электронной почты и/или телефоном. Если вы забыли пароль, его можно восстановить:',
                en: 'Someone (or you) already registered with the specified email address and/or telephone. If you forgot a password, you can restore it:',
            },
            btnTitle: {
                ru: 'Восстановить доступ',
                en: 'Restore access',
            },
            errCodeLabel: {
                ru: 'Код ошибки',
                en: 'Error code',
            },
        }
    }
}