import { BasePage } from "../../../core/lib/BasePage.js";
import { FormLoginBlockServ } from "../../blocks/FormLoginBlock/FormLoginBlockServ.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";

export class LoginPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'LoginPage';
        this.httpStatus = 200;
        this.blocks = [FormLoginBlockServ]
        this.data = {
            rePassLink: UserPasswordURI.passwordResetPage,
            signupLink: SignupURI.signupPage,
        }
        this.dict = {
            title: {
                ru: 'Обратная связь',
                en: 'Feedback',
            },
            forbiddenMessage: {
                ru: 'Вы должны авторизоваться, чтобы выполнить это действие.',
                en: 'You must log in to perform this action. ',
            },
            rePassLinkText: {
                ru: 'Восстановить доступ',
                en: 'Reset pasword',
            },
            signupLinkText: {
                ru: 'Создать аккаунт',
                en: 'Create account',
            },
        }
    }
}