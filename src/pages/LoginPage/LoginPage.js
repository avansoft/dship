import { FormLoginBlockFront } from "../../blocks/FormLoginBlock/FormLoginBlockFront.js"

// FeedbackFormBlock init
const formEl = document.querySelector('.js-b-formLogin');
if (formEl) FormLoginBlockFront.init(formEl);