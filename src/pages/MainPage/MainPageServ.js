import { BasePage } from "../../../core/lib/BasePage.js";

export class MainPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'MainPage';
        this.httpStatus = 200;
        this.blocks = [];
        this.dict = {
            title: {
                ru: 'DHIP',
                en: 'DHIP',
            },
            howItWorks: {
                ru: 'Как это работает?',
                en: 'How it works?',
            },
            whoWeAre: {
                ru: 'это сервис и бесплатная обучающая программа, которые позволят вам начать зарабатывать на eBay всего через 2 недели',
            },
            joinOurCourse: {
                ru: 'Записаться на курс',
                en: 'Join the Course',
            },
            step1: {
                ru: 'Пройдите бесплатное обучение, если вы новичок на eBay',
            },
            step2: {
                ru: 'Выберите эксклюзивные товары из нашего каталога, скачайте фото',
            },
            step3: {
                ru: 'Получите оплату за проданный товар и сообщите нам, куда отправить заказ',
            },
            step4: {
                ru: 'Зарабатывайте все больше, не вкладывая денег в закупку товара!',
            },
        }
    }
}