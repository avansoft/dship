import { BasePage } from "../../../core/lib/BasePage.js";
import { FormPasswordUpdateBlockServ } from "../../blocks/FormPasswordUpdateBlock/FormPasswordUpdateBlockServ.js";

export class SignupCompletePageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'SignupCompletePage';
        this.httpStatus = 200;
        this.blocks = [FormPasswordUpdateBlockServ];
        this.dict = {
            title: {
                ru: 'Завершение регистрации',
                en: 'Signup complete',
            },
        }
    }
}