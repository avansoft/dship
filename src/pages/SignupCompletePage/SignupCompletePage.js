import { FormPasswordUpdateBlockFront } from "../../blocks/FormPasswordUpdateBlock/FormPasswordUpdateBlockFront.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";

/**
 * Завершение регистрации предполагает ввод пользователем пароля, т.к.
 * в текущем сценарии он не запрашивается на первом шаге регистрации.
 * Соответственно, к странице подключается форма обновления пароля,
 * но apiUri передается для другого действия
 */

// FeedbackFormBlock init
const formEl = document.querySelector('.js-b-passwordUpdate');
if (formEl) FormPasswordUpdateBlockFront.init(formEl, SignupURI.signupCompleteAction);