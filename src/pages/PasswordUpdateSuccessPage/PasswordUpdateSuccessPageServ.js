import { BasePage } from "../../../core/lib/BasePage.js";

export class PasswordUpdateSuccessPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'PasswordUpdateSuccessPage';
        this.httpStatus = 200;
        this.blocks = [];
        this.dict = {
            title: {
                ru: 'Пароль изменен',
                en: 'Password OK!',
            },
            message: {
                ru: 'Ваш пароль был успешно обновлен',
                en: 'Password has been successfully updated',
            },
            btnTitleText: {
                ru: 'Перейти в профиль',
                en: 'Go to profile',
            },
        }
    }
}