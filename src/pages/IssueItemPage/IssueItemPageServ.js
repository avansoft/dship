import { BasePage } from "../../../core/lib/BasePage.js";
import { IssueItemBlockServ } from "../../blocks/IssueItemBlock/IssueItemBlockServ.js";

export class IssueItemPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'IssueItemPage';
        this.httpStatus = 200;
        this.blocks = [IssueItemBlockServ]
        this.dict = {
            title: {
                ru: 'Вопрос №',
                en: 'Issue #',
            },
        }
    }
}