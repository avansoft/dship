import { IssueItemBlockFront } from "../../blocks/IssueItemBlock/IssueItemBlockFront.js";

const block = document.querySelector('.js-b-issueItem');
if (block) new IssueItemBlockFront(block).init();