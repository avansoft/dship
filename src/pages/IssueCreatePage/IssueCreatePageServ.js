import { BasePage } from "../../../core/lib/BasePage.js";
import { FormIssueCreateBlockServ } from "../../blocks/FormIssueCreateBlock/FormIssueCreateBlockServ.js";

export class IssueCreatePageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'IssueCreatePage';
        this.httpStatus = 200;
        this.blocks = [FormIssueCreateBlockServ];
        this.dict = {
            title: {
                ru: 'Новый вопрос',
                en: 'New issue',
            },
        }
    }
}