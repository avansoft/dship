import { FormIssueCreateBlockFront } from "../../blocks/FormIssueCreateBlock/FormIssueCreateBlockFront.js";

const el = document.querySelector('.js-b-issueCreate');
if (el) FormIssueCreateBlockFront.init(el);