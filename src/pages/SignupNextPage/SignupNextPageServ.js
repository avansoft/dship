import { BasePage } from "../../../core/lib/BasePage.js";

export class SignupNextPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'SignupNextPage';
        this.httpStatus = 200;
        this.blocks = [];
        this.dict = {
            title: {
                ru: 'Регистрация',
                en: 'Signup',
            },
            header: {
                ru: 'Почти готово!',
                en: 'Almost ready!',
            },
            text: {
                ru: 'Мы отправили вам письмо со ссылкой для подтверждения регистрации. Если вы не найдете его во входящих, прожалуйста, проверьте папку "Спам".',
                en: 'We sent you a letter with reference to confirm registration. If you do not find it in the incoming, vault, check the "Spam" folder.',
            },
            btnLinkTitle: {
                ru: 'Перейти в почту',
                en: 'Go to inbox',
            },
        }
    }
}