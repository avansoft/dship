import { BasePage } from "../../../core/lib/BasePage.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";

export class ExcepSignupPageServ extends BasePage {
    constructor(g, ctx, props) {
        super(g, ctx, props);
        this.template = 'ExcepSignupPage';
        this.httpStatus = 400;
        this.blocks = [];
        this.data = {
            btnLink: SignupURI.signup,
        }
        this.dict = {
            title: {
                ru: 'Ошибка регистрации',
                en: 'Signup error',
            },
            header: {
                ru: 'Ошибка регистрации',
                en: 'Signup error',
            },
            content: {
                ru: 'Произошла непредвиденная ошибка. Пожалуйста, начните регистрацию с начала. Если ошибка повторится, свяжитесь с нами.',
                en: 'An unforeseen error occurred. Please start registration from the beginning. If the error is repeated, contact us.',
            },
            btnTitle: {
                ru: 'Попробовать снова',
                en: 'Try again',
            },
            errCodeLabel: {
                ru: 'Код ошибки',
                en: 'Error code',
            },
        }
    }
}