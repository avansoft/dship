import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FormSignupBlockDict extends BaseDictionary {
    static dict = {
        formHeader: {
            ru: 'Регистрация',
            en: 'Signup',
        },
        userFirstNameLabel: {
            ru: 'Ваше имя',
            en: 'First name',
        },
        userEmailLabel: {
            ru: 'Электронная почта',
            en: 'E-mail',
        },
        userMobilePhoneLabel: {
            ru: 'Телефон',
            en: 'Mobile phone',
        },
        continueButtonTitle: {
            ru: 'Продолжить',
            en: 'Continue',
        },
    }
}
