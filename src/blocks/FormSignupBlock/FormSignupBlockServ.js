import { FormSignupBlockDict } from "./FormSignupBlockDict.js";
import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";

export class FormSignupBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = FormSignupBlockDict;
    }
}