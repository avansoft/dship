import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";

export class FormSignupBlockFront extends BaseBlockFront {
    static init(el) {
        // Form init
        const formEl = el.querySelector('.js-form-signup');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri: SignupURI.signupAction,
                fields: {
                    firstName: {
                        selector: '.js-textbox-firstName',
                        type: 'textbox',
                    },
                    email: {
                        selector: '.js-textbox-email',
                        type: 'textbox',
                    },
                    mobilePhone: {
                        selector: '.js-textbox-mobilePhone',
                        type: 'textbox',
                    },
                },
            });
            form.init();
        }
    }
}
