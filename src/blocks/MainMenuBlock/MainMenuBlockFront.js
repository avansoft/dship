import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { ScrollBoxComp } from "../../basics/components/ScrollBoxComp/ScrollBoxComp.js";

export class MainMenuBlockFront extends BaseBlockFront {
    constructor(el) {
        super(el);
        this.itemsList = el.querySelectorAll('.js-item');
        this.scrollMenuEl = el.querySelector('.js-c-scrollMenu');
    }
    init() {
        const scrollBox = new ScrollBoxComp({
            el: this.scrollMenuEl,
        });

        const onWindowLoad = () => {
            // Маркировка активности
            for (const item of this.itemsList) {
                // Путь из адресной строки браузера
                const path = document.location.pathname;

                // uri
                const linkEl = item.querySelector('a');
                if (linkEl) {
                    const uri = linkEl.getAttribute('href');

                    /**
                     * Подсветка пункта меню выполняется лишь
                     * в том случае, если адрес страницы совпадает
                     * с адресом ссылки из пункта меню
                     */
                    if (uri === path) {
                        // Удаляется ссылка для активного пункта меню
                        const text = linkEl.textContent;
                        linkEl.remove();
                        const activeItemEl = document.createElement('span');
                        activeItemEl.textContent = text;
                        activeItemEl.classList.add('__active');
                        item.append(activeItemEl);
                        /**
                         * Прокручиваем скролл-контейнер, чтобы
                         * активный элемент меню был виден
                         */
                        scrollBox.showElement(activeItemEl);
                        break;
                    }
                }
            }
        };

        if (document.readyState !== 'complete') {
            window.addEventListener('load', onWindowLoad);
        } else {
            onWindowLoad();
        }
    }
}