import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class MainMenuBlockDict extends BaseDictionary {
    static dict = {
        catalogTitle: {
            ru: 'Каталог',
            en: 'Catalog',
        },
        goodsTitle: {
            ru: 'Товары',
            en: 'Goods',
        },
        buyoutsTitle: {
            ru: 'Закупки',
            en: 'Buyouts',
        },
        buyersTitle: {
            ru: 'Байеры',
            en: 'Buyers',
        },
        coursesTitle: {
            ru: 'Курсы',
            en: 'Courses',
        },
        helpdeskTitle: {
            ru: 'Поддержка',
            en: 'Helpdesk',
        },
    }
}
