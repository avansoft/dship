import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";
import { MainMenuBlockDict } from "./MainMenuBlockDict.js";
import { HelpdeskURI } from "../../../actions/helpdesk/HelpdeskURI.js";

export class MainMenuBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = MainMenuBlockDict;
        this.data = {
            helpdeskUri: HelpdeskURI.issueListPage,
        }
    }
}