import { IssueDAO } from '../../../dao/IssueDAO.js';
import { IssueMessageDAO } from '../../../dao/IssueMessageDAO.js';
import { UploaderServiceLinker } from '../../../services/uploader/UploaderServiceLinker.js';
import { IssueItemBlockDict } from './IssueItemBlockDict.js';
import { BaseBlockServ } from '../../../core/lib/BaseBlockServ.js';

export class IssueItemBlockServ extends BaseBlockServ {
	constructor(g, ctx) {
		super(g, ctx);
		this.dict = IssueItemBlockDict;
	}

	async init() {
		const g = this.g;
		const ctx = this.ctx;
		const logger = g.logger;

		// Info
		logger.trace('Get issue info from StoreDB');
		const issueDao = new IssueDAO(g);
		const issueId = ctx.params.id;
		const issueInfo = await issueDao.getIssueInfo(issueId);

		this.addToState('info', issueInfo);

		// Images
		logger.trace('Get images for issue');
		const images = await issueDao.getIssueImageList(issueId);

		if (images.length) {
			logger.trace('Make links for tag <img>');
			const linker = new UploaderServiceLinker(g.config.presets.issueImageTeaser);
			const issueItemImages = linker.getLinks(images);

			this.addToState('images', issueItemImages);
		}

		// Messages
		logger.trace('Get messages from StoreDB');
		const issueMessageDao = new IssueMessageDAO(g);
		const issueMessageList = await issueMessageDao.getListByIssue(issueId);

		if (issueMessageList.length) {
			this.addToState('messages', issueMessageList);
		}

		return this.state;
	}
}