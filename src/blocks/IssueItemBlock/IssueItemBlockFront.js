import { BaseBlockFront } from '../../basics/abstracts/BaseBlockFront.js';
import { ChatThreadComp } from '../../basics/components/ChatThreadComp/ChatThreadComp.js';
import { TeaserListComp } from '../../basics/components/TeaserListComp/TeaserListComp.js';
import { ModalWindowComp } from '../../basics/components/ModalWindowComp/ModalWindowComp.js';
import { AsyncButtonComponent } from '../../basics/components/AsyncButtonComponent/AsyncButtonComponent.js';
import { HelpdeskURI } from '../../../actions/helpdesk/HelpdeskURI.js';
import { IssueItemBlockDict } from './IssueItemBlockDict.js';
import Client from '../../basics/Client.js';

export class IssueItemBlockFront extends BaseBlockFront {
	constructor(el) {
		super(el);
		this.issueChatEl = el.querySelector('.js-issueChat');
		this.teaserListEl = el.querySelector('.js-teaserList');
		this.issueCloseButtonEl = el.querySelector('.js-issueCloseButton');
		this.messageInfoEl = el.querySelector('.js-messageInfo');
		this.issueId = el.dataset.issueId;
	}
	init() {
		// ChatThread init
		if (this.issueChatEl) {
			if (!this.issueId) throw new Error('Not found issueId in data attribute');
			this.issueChat = new ChatThreadComp({
				el: this.issueChatEl,
				issueMessageCreateUri: HelpdeskURI.issueMessageCreateAction,
				chatId: this.issueId,
			});
			this.issueChat.init();
		}

		// TeaserList init
		if (this.teaserListEl) {
			const teaserList = new TeaserListComp({
				el: this.teaserListEl,
			});
			teaserList.addEventListener('clickOnTeaser', ev => this.openModalWindow(ev));
		}

		// CommanButton init
		if (this.issueCloseButtonEl) {
			const successEventName = 'issueArchived';
			const issueCloseButton = new AsyncButtonComponent({
				el: this.issueCloseButtonEl,
				command: () => {
					Client.send(
						{ issueId: this.issueId },
						HelpdeskURI.getIssueArchivedURI(this.issueId)
					);
				},
				successEventName,
			});
			issueCloseButton.addEventListener(successEventName, () => this.closeIssue());
		}
	}

	openModalWindow(ev) {
		const modal = new ModalWindowComp({
			beforeEl: this.el,
		});

		// Create img
		const imgEl = document.createElement('img');
		imgEl.setAttribute('src', ev.detail.fullSizeImageLink);
		imgEl.classList.add('st-c-issueItem--modalImage');

		// Show image
		modal.showContent(imgEl);
	}

	closeIssue() {
		if (!this.messageInfoEl) throw new Error('Not found messageInfoEl');

		// Hide button
		this.issueCloseButtonEl.style.display = 'none';

		// Create archived message
		const archivedMessageEl = document.createElement('div');
		archivedMessageEl.classList.add('__archivedMessage');
		this.messageInfoEl.append(archivedMessageEl);

		const textWrapper = document.createElement('p');
		textWrapper.textContent = IssueItemBlockDict.getDictionary(this.lang).closedMessageText;
		archivedMessageEl.append(textWrapper);

		// Change status tag
		const statusTagEl = this.el.querySelector('.js-statusTag');
		statusTagEl.textContent = IssueItemBlockDict.getDictionary(this.lang).statusTagClosedText;
		statusTagEl.classList.remove('__statusTagActive');
		statusTagEl.classList.add('__statusTagClosed');

		// Hide form
		this.issueChat.hideForm();
	}
}