import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class IssueItemBlockDict extends BaseDictionary {
    static dict = {
        headerWithNumber: {
            ru: 'Запрос №',
            en: 'Issue #',
        },
        closedMessageText: {
            ru: 'Этот запрос закрыт, но вы можете создать новый',
            en: 'This issue has been closed, but you can open a new',
        },
        statusTagClosedText: {
            ru: 'Закрыт пользователем',
            en: 'Closed',
        },
        statusTagActiveText: {
            ru: 'В работе',
            en: 'Active',
        },
        markResolved: {
            ru: 'Отметить решенным',
            en: 'Mark resolved',
        },
        itemNotFound: {
            ru: 'Информация о запросе не найдена',
            en: 'Not found information for this issue',
        },
        addMessageLabel: {
            ru: 'Сообщение',
            en: 'Message',
        },
        sendButtonText: {
            ru: 'Отправить',
            en: 'Send',
        },
    }
}
