import { FormPasswordResetBlockDict } from "./FormPasswordResetBlockDict.js";
import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";

export class FormPasswordResetBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = FormPasswordResetBlockDict;
    }
}