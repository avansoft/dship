import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FormPasswordResetBlockDict extends BaseDictionary {
    static dict = {
        formHeader: {
            ru: 'Сброс пароля',
            en: 'Password reset',
        },
        message: {
            ru: 'Мы отправим на вашу электронную почту ссылку, по которй можно будет создать новый пароль',
            en: 'We will send a link to your email that you can create a new password',
        },
        emailLabel: {
            ru: 'Электронная почта',
            en: 'Email',
        },
        isRemember: {
            ru: 'Запомнить меня',
            en: 'Remember me',
        },
        btnActionText: {
            ru: 'Отправить ссылку',
            en: 'Send the link',
        },
    }
}
