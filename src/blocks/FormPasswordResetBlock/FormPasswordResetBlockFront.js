import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";

export class FormPasswordResetBlockFront extends BaseBlockFront {
    static init(el) {
        const formEl = el.querySelector('.js-form-passwordReset');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri: UserPasswordURI.passwordResetAction,
                fields: {
                    email: {
                        selector: '.js-textbox-email',
                        type: 'textbox',
                    },
                },
            });
            form.init();
        }
    }
}
