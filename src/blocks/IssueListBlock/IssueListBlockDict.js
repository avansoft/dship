import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class IssueListBlockDict extends BaseDictionary {
    static dict = {
        blockHeader: {
            ru: 'Мои вопросы',
            en: 'My issues',
        },
        createIssueButtonTitle: {
            ru: 'Новый вопрос',
            en: 'Create first issue',
        },
        emptyMessage: {
            ru: 'Нет открытых запросов',
            en: 'Archive',
        },
        archiveHeader: {
            ru: 'Архив',
            en: 'Archive',
        },
        loadButtonTitle: {
            ru: 'Загрузить еще...',
            en: 'Load more...',
        },
    }
}
