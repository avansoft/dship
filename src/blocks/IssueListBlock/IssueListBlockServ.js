import { IssueListBlockDict } from './IssueListBlockDict.js';
import { BaseBlockServ } from '../../../core/lib/BaseBlockServ.js';
import { IssueDAO } from '../../../dao/IssueDAO.js';
import { HelpdeskURI } from '../../../actions/helpdesk/HelpdeskURI.js';

export class IssueListBlockServ extends BaseBlockServ {
	constructor(g, ctx) {
		super(g, ctx);
		this.dict = IssueListBlockDict;
		this.data = {
			createIssueLink: HelpdeskURI.issueCreatePage,
		};
	}
	async init() {
		const logger = this.g.logger;
		logger.action(this.selfName);

		const ctx = this.ctx;
		const g = this.g;

		const issueDao = new IssueDAO(g);

		logger.step('Get active issues from StoreDB');
		const activeIssues = await issueDao.getActiveIssueList(ctx.session.userKey);

		if (activeIssues.length) {
			this.addToSelf('activeIssues', activeIssues);
		}

		logger.step('Get archived issues from StoreDB');
		const archivedIssues = await issueDao.getClosedIssueList({
			userKey: ctx.session.userKey,
			offset: 0,
		});

		if (archivedIssues.length) {
			this.addToSelf('archivedIssues', archivedIssues);
		}

		return this.state;
	}
}