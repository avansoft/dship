import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { ListLoaderComp } from "../../basics/components/ListLoaderComp/ListLoaderComp.js";
import { IssueListBlockDict } from "./IssueListBlockDict.js";
import { HelpdeskURI } from "../../../actions/helpdesk/HelpdeskURI.js";

export class IssueListBlockFront extends BaseBlockFront {
    constructor(el) {
        super(el);
        this.archivedIssues = el.querySelector('.js-c-listLoader-archivedIssues');
    }
    init() {
        new ListLoaderComp({
            /**
             * Обертка, которая содержит тизеры. Других
             * элементов содержать не должна
             */
            el: this.archivedIssues,
            apiUrl: HelpdeskURI.issueListAction,
            loadButtonCssClassName: '__loadButton',
            loadButtonTitle: IssueListBlockDict.getDictionary(this.lang).loadButtonTitle,
            /**
             * Функция, которая конструирует блок
             * с тизерами
             */
            templMaker: arr => {
                /**
                 * Этот шаблон просто копируется из файла шаблоны
                 */
                const templates = arr.map(item => `
                    <div class="_issueTeaser">
                        <a href="${item.issueUri}" class="_title">
                            ${item.summary}
                        </a>
                        <div class="_tags">
                            <span class="_theme">${item.themeTitle}</span>
                        </div>
                    </div>
                `);
                return templates;
            },
        })
    }
}