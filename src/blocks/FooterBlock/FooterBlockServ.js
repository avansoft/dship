import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";

export class FooterBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.data = {
            currentYear: new Date().getFullYear(),
        }
        this.dict;
    }
}