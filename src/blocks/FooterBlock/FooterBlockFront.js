import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { CookiesAlertComp } from "../../basics/components/CookiesAlertComp/CookiesAlertComp.js";

export class FooterBlockFront extends BaseBlockFront {
    constructor(el) {
        super(el);
    }
    init() {
        // Cookies alert init
        new CookiesAlertComp({
            parentEl: this.el,
            lang: this.lang,
        })
    }

    cookiesAlertInit() {
        // Wrapper
        const contentEl = document.createElement('div');
        contentEl.classList.add('st-b-footer--cookiesWindow');

        // Header
        const headerEl = document.createElement('h1');
        headerEl.textContent = this.dict.cookiesHeader;
        contentEl.append(headerEl);

        // Text
        const textEl = document.createElement('p');
        textEl.textContent = this.dict.cookiesText;
        contentEl.append(textEl);

        // Link to agreement
        const linkEl = document.createElement('a');
        linkEl.setAttribute('href', '/');
        linkEl.textContent = this.dict.cookiesLinkTitle;
        contentEl.append(linkEl);

        // Accept button
        const acceptButton = document.createElement('button');
        acceptButton.setAttribute('type', 'button');
        acceptButton.textContent = this.dict.cookiesButtonTitle;
        contentEl.append(acceptButton);

        const popup = new PopupWindowComp({
            parentEl: this.el,
            contentEl,
        })

        acceptButton.addEventListener('click', () => {
            localStorage.setItem(this.localStorageKey, 'true');
            popup.closeWindow();
        })
    }
}
