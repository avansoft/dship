import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { SiteURI } from "../../../actions/site/SiteURI.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";

export class FeedbackFormBlockFront extends BaseBlockFront {
    static init(el) {
        const formEl = el.querySelector('.js-form-feedback');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri: SiteURI.feedback,
                fields: {
                    firstName: {
                        selector: '.js-textbox-firstName',
                        type: 'textbox',
                    },
                    email: {
                        selector: '.js-textbox-email',
                        type: 'textbox',
                    },
                    themeId: {
                        selector: '.js-tapSelect-theme',
                        type: 'tapSelect',
                    },
                    message: {
                        selector: '.js-textarea-message',
                        type: 'textarea',
                    },
                },
            });
            form.init();
        }
    }
}
