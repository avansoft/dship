import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FeedbackFormBlockDict extends BaseDictionary {
    static dict = {
        formHeader: {
            ru: 'Обратная связь',
            en: 'Feedback',
        },
        emailId: {
            ru: 'Код письма',
            en: 'Email ID',
        },
        emailLabel: {
            ru: 'Почта для ответа',
            en: 'Email for reply',
        },
        firstNameLabel: {
            ru: 'Вашe имя',
            en: 'Your name',
        },
        themeLabel: {
            ru: 'Тема',
            en: 'Subject',
        },
        notSelected: {
            ru: 'Тема не выбрана',
            en: 'Theme not selected',
        },
        messageLabel: {
            ru: 'Сообщение',
            en: 'Message',
        },
        sendButtonText: {
            ru: 'Отправить',
            en: 'Send',
        },
    }
}
