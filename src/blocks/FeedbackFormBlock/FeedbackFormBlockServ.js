import { FeedbackFormBlockDict } from './FeedbackFormBlockDict.js';
import { BaseBlockServ } from '../../../core/lib/BaseBlockServ.js';
import { FeedbackThemeDAO } from '../../../dao/FeedbackThemeDAO.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';

export class FeedbackFormBlockServ extends BaseBlockServ {
    constructor(g, ctx) {
        super(g, ctx);
        this.dict = FeedbackFormBlockDict;
    }
    async init() {
        const logger = this.g.logger;
        logger.action(this.name);

        const ctx = this.ctx;
        const g = this.g;

        logger.step('Get theme list from DB');
        const feedbackThemeDao = new FeedbackThemeDAO(g);
        const themes = await feedbackThemeDao.getFeedbackThemeListByLang(ctx.lang);
        this.addToState('themes', themes);

        logger.step('Get profile info');
        const userKey = ctx?.session?.userKey;
        if (userKey) {
            logger.trace(`Get profile for userKey [${userKey}]`);
            const subjectDao = new SubjectDAO(g);
            const profile = await subjectDao.getSubjectInfo(userKey);
            this.addToState('profile', profile);
        }

        return this.state;
    }
}