import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FormLoginBlockDict extends BaseDictionary {
    static dict = {
        title: {
            ru: 'Вход',
            en: 'Signin',
        },
        header: {
            ru: 'Вход',
            en: 'Signin',
        },
        login: {
            ru: 'Логин / Эл. почта',
            en: 'Login or email',
        },
        passwordLabel: {
            ru: 'Пароль',
            en: 'Password',
        },
        isRemember: {
            ru: 'Запомнить меня',
            en: 'Remember me',
        },
        btnAction: {
            ru: 'Войти',
            en: 'Signin',
        },
    }
}
