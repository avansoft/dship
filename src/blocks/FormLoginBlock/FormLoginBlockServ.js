import { FormLoginBlockDict } from "./FormLoginBlockDict.js";
import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";

export class FormLoginBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = FormLoginBlockDict;
    }
}