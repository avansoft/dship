import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { UserSessionURI } from "../../../actions/user-session/UserSessionURI.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";

export class FormLoginBlockFront extends BaseBlockFront {
    static init(el) {
        const formEl = el.querySelector('.js-form-login');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri: UserSessionURI.loginAction,
                fields: {
                    login: {
                        selector: '.js-textbox-login',
                        type: 'textbox',
                    },
                    password: {
                        selector: '.js-passbox',
                        type: 'passbox',
                    },
                    isRemember: {
                        selector: '.js-checkbox-isRemember',
                        type: 'checkbox',
                    },
                },
            });
            form.init();
        }
    }
}
