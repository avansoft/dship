import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";
import { HelpdeskURI } from "../../../actions/helpdesk/HelpdeskURI.js";

export class FormIssueCreateBlockFront extends BaseBlockFront {
    static init(el) {
        const formEl = el.querySelector('.js-form-issueCreate');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri: HelpdeskURI.issueCreateAction,
                fields: {
                    themeId: {
                        selector: '.js-cascadeSelect-theme',
                        type: 'cascadeSelect',
                        props: {
                            apiUri: HelpdeskURI.issueThemeList,
                        }
                    },
                    summary: {
                        selector: '.js-textbox-summary',
                        type: 'textbox',
                    },
                    message: {
                        selector: '.js-textarea-message',
                        type: 'textarea',
                    },
                    files: {
                        selector: '.js-multifile-files',
                        type: 'multifile',
                    },
                },
            });
            form.init();
        }
    }
}
