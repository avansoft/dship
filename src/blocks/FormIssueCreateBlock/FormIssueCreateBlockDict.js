import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FormIssueCreateBlockDict extends BaseDictionary {
    static dict = {
        formHeader: {
            ru: 'Новый вопрос',
            en: 'New issue',
        },
        summaryLabel: {
            ru: 'Суть вопроса',
            en: 'Summary',
        },
        messageLabel: {
            ru: 'Сообщение',
            en: 'Message',
        },
        notSelectedDefaultText: {
            ru: 'Тема не выбрана',
            en: 'Theme not selected',
        },
        buttonText: {
            ru: 'Отправить',
            en: 'Send',
        },
        addFilesLabel: {
            ru: 'Добавить файлы',
            en: 'Add files',
        },
    }
}
