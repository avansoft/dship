import { FormIssueCreateBlockDict } from '../../blocks/FormIssueCreateBlock/FormIssueCreateBlockDict.js';
import { BaseBlockServ } from '../../../core/lib/BaseBlockServ.js';
import { IssueThemeDAO } from '../../../dao/IssueThemeDAO.js';

export class FormIssueCreateBlockServ extends BaseBlockServ {
    constructor(g, ctx) {
        super(g, ctx);
        this.dict = FormIssueCreateBlockDict;
    }
    async init() {
        const logger = this.g.logger;
        logger.action(this.selfName);

        const ctx = this.ctx;
        const g = this.g;

        logger.step('Get theme list from DB');
        const issueThemes = new IssueThemeDAO(g);
        this.addToState('themes', await issueThemes.getIssueThemeList(ctx.lang));

        return this.state;
    }
}