import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class HeaderBlockDict extends BaseDictionary {
    static dict = {
        profileButtonText: {
            ru: 'Мой профиль',
            en: 'My profile',
        },
        signupButtonText: {
            ru: 'Регистрация',
            en: 'Signup',
        },
        loginButtonText: {
            ru: 'Вход',
            en: 'Login',
        },
        quitConfirmButtonText: {
            ru: 'Да',
            en: 'Yes',
        },
        quitCancelButtonText: {
            ru: 'Нет',
            en: 'No',
        },
        quitAskButtonText: {
            ru: 'Выйти из аккаунта?',
            en: 'Quit from account?',
        },
    }
}
