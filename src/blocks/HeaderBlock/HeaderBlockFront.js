import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { OverlayComponent } from "../../basics/components/OverlayComponent/OverlayComponent.js";
import { AsyncButtonComponent } from "../../basics/components/AsyncButtonComponent/AsyncButtonComponent.js";
import { UserSessionURI } from "../../../actions/user-session/UserSessionURI.js";
import { HeaderBlockDict } from "./HeaderBlockDict.js";
import Client from "../../basics/Client.js";

export class HeaderBlockFront extends BaseBlockFront {
    constructor(el) {
        super(el);
        this.headerLoginEl = el.querySelector('.st-b-header--login');
    }
    init() {
        if (this.headerLoginEl) this.headerLoginInit();
    }

    headerLoginInit() {
        const dict = HeaderBlockDict.getDictionary(this.lang);

        // Create wrapper for content
        const overlayEl = document.createElement('div');
        overlayEl.classList.add('__overlayContent');
        const overlay = new OverlayComponent({
            parentEl: this.el,
            contentEl: overlayEl,
        });

        // Create question text
        const textEl = document.createElement('span');
        textEl.classList.add('__text');
        textEl.textContent = dict.quitAskButtonText;
        overlayEl.append(textEl);

        // Confirm logout button
        const confirmLogoutButtonEl = document.createElement('button');
        confirmLogoutButtonEl.classList.add('__btn');
        confirmLogoutButtonEl.textContent = dict.quitConfirmButtonText;
        overlayEl.append(confirmLogoutButtonEl);
        new AsyncButtonComponent({
            el: confirmLogoutButtonEl,
            command: () => {
                const url = Client.getEndpoint(UserSessionURI.logoutAction);
                Client.send({ action: 'logout' }, url);
            },
        });

        // Cancel button
        const cancelButtonEl = document.createElement('button');
        cancelButtonEl.classList.add('__btn');
        cancelButtonEl.textContent = dict.quitCancelButtonText;
        overlayEl.append(cancelButtonEl);
        cancelButtonEl.addEventListener('click', () => overlay.hide());

        /**
         * Init login header elements
         */

        // Init logout button
        const initLogoutButtonEl = this.el.querySelector('.js-btn-initLogout');
        if (initLogoutButtonEl) {
            initLogoutButtonEl.addEventListener('click', () => overlay.show());
        }
    }
}
