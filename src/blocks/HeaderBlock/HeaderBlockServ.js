import { HeaderBlockDict } from "./HeaderBlockDict.js";
import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";
import { UserSessionURI } from "../../../actions/user-session/UserSessionURI.js";
import { SignupURI } from "../../../actions/signup/SignupURI.js";
import { UserURI } from "../../../actions/user/UserURI.js";

export class HeaderBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = HeaderBlockDict;
        this.data = {
            loginPageLink: UserSessionURI.loginPage,
            signupPageLink: SignupURI.signupPage,
            userProfilePageLink: UserURI.profilePage,
        }
    }
}