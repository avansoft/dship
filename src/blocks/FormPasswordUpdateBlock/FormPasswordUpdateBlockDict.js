import { BaseDictionary } from "../../../core/lib/BaseDictionary.js";

export class FormPasswordUpdateBlockDict extends BaseDictionary {
    static dict = {
        formHeader: {
            ru: 'Новый пароль',
            en: 'New password',
        },
        messageForPasswordUpdate: {
            ru: 'Придумайте надежный пароль для вашего аккаунта:',
            en: 'Come up with a reliable password for your account:',
        },
        messageForPasswordSet: {
            ru: 'Осталось придумать надежный пароль для входа в личный кабинет:',
            en: 'Please, set a password for access to your account',
        },
        userPasswordLabel: {
            ru: 'Пароль',
            en: 'Password',
        },
        btnSendTitle: {
            ru: 'Сохранить',
            en: 'Save',
        },
    }
}
