import { FormPasswordUpdateBlockDict } from "./FormPasswordUpdateBlockDict.js"
import { BaseBlockServ } from "../../../core/lib/BaseBlockServ.js";

export class FormPasswordUpdateBlockServ extends BaseBlockServ {
    constructor(g, ctx, props) {
        super(g, ctx);
        this.dict = FormPasswordUpdateBlockDict;
    }
}