import { BaseBlockFront } from "../../basics/abstracts/BaseBlockFront.js";
import { FormComponent } from "../../basics/components/FormComponent/FormComponent.js";
import { UserPasswordURI } from "../../../actions/user-password/UserPasswordURI.js";
import { PasswordCheckerComp } from "../../basics/components/PasswordCheckerComp/PasswordCheckerComp.js";

export class FormPasswordUpdateBlockFront extends BaseBlockFront {
    /**
     * apiUri формируется на странице, т.к. блок может инициализироваться
     * как со страницы отправки запроса на обновление пароля, так и
     * со страницы подтверждения регистрации. Запросы в обоих случаях
     * разные.
     */
    static init(el, apiUri) {
        // Form init
        const formEl = el.querySelector('.js-form-passwordUpdate');
        if (formEl) {
            const form = new FormComponent({
                el: formEl,
                apiUri,
                fields: {
                    password: {
                        selector: '.js-passbox',
                        type: 'passbox',
                    },
                },
            });
            form.init();
        }

        // PasswordChecker init
        const passwordCheckerEl = el.querySelector('.js-c-passwordChecker');
        const inputEl = el.querySelector('.js-passbox');
        if (passwordCheckerEl && inputEl) {
            new PasswordCheckerComp({
                el: passwordCheckerEl,
                inputEl,
                checkers: {
                    minLength: true,
                    containUpperCase: true,
                    containDigit: true,
                    containSpecSymbol: true,
                }
            })
        }
    }
}
