import createElement from "../helpers/createElement.js";

export class BaseComponent extends EventTarget {
    constructor(props, schema) {
        super();
        this.props = props ? propsChecker(props, schema) : {};

        function propsChecker(props, schema) {
            if (typeof props !== 'object') throw new Error('Props param should be an object');

            const propsObj = {};

            // Check required and set default value
            for (let propName in schema) {
                const propItem = props[propName];
                const isPropRequired = schema[propName]['require'];

                if (isPropRequired && !propItem) {
                    const propDefaultValue = schema[propName]['default'];
                    if (propDefaultValue) {
                        // Set default value
                        propsObj[propName] = propDefaultValue;
                    } else {
                        throw new Error(`Property ${propName} is require, but not found in props object`);
                    }
                }
            }

            // Types checking
            for (let propName in props) {
                // Search rules for property in schema
                const propRules = schema[propName];
                if (!propRules) throw new Error(`Not found schema rule for property ${propName}`);

                const propType = propRules.type;
                if (propType) {
                    const propValue = props[propName];
                    if (typeof propValue === propType) {
                        // Copy property value from origin props object
                        propsObj[propName] = props[propName];
                        continue;
                    }
                    throw new Error(`
                        Prop value '${propValue}' for propRules with name '${propName}'
                        should have a type = '${propType}' but it is ${typeof propValue}
                    `);
                } else {
                    throw new Error(`Not found type in schema for property ${propName}`);
                }
            }

            return propsObj;
        }
    }

    createElement(props) {
        return createElement(props);
    }

    get scrollbarWidth() {
        // Creating invisible container
        const outer = document.createElement('div');
        outer.style.visibility = 'hidden';

        // Forcing scrollbar to appear
        outer.style.overflow = 'scroll';

        // Needed for WinJS apps
        outer.style.msOverflowStyle = 'scrollbar';
        document.body.appendChild(outer);

        // Creating inner element and placing it in the container
        const inner = document.createElement('div');
        outer.appendChild(inner);

        // Calculating difference between container's full width and the child width
        const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

        // Removing temporary elements from the DOM
        outer.parentNode.removeChild(outer);

        return scrollbarWidth;
    }

    isScrollExists(el) {
        const checkingEl = el || document.documentElement;
        /**
         * When scrollbar is available, scrollHeight
         * will be more then clientHeight
         */
        return checkingEl.scrollHeight !== checkingEl.clientHeight;
    }
}
