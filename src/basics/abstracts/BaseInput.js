import createElement from "../../basics/helpers/createElement.js";

export class BaseInput extends EventTarget {
    constructor(el) {
        super();
        if (!el) throw new Error(`${this.constructor.name} -> El is required`);
        this.el = el;

        this.errorStyleClassName = 'js-fieldError';

        // Error message
        this.errorMessageEl = undefined;

        // Get page language
        this.lang = document.documentElement.lang || 'ru';
    }

    get value() {
        throw new Error('Контрол должен иметь метод getValue()');
    }

    createElement(props) {
        return createElement(props);
    }

    setError(message) {
        const errorMessageEl = document.createElement('p');
        errorMessageEl.classList.add('js-error');
        errorMessageEl.textContent = message;
        this.el.appendChild(errorMessageEl);
        this.errorMessageEl = errorMessageEl;

        this.el.classList.add(this.errorStyleClassName);
    }

    removeError() {
        if (this.errorMessageEl) {
            this.el.classList.remove(this.errorStyleClassName);
            this.errorMessageEl.remove();
        }
    }
}