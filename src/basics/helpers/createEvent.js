export default function (type, element, options = {}) {
    const ev = new CustomEvent(type, {
        detail: { ...options },
    });

    element.dispatchEvent(ev);
}