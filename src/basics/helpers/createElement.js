export default function (props) {
    if (!props.type) throw new Error('Props.type is required');
    const el = document.createElement(props.type);

    if (props.classes) {
        el.classList.add(...props.classes);
    }

    if (props.textContent) {
        el.textContent = props.textContent;
    }

    if (props.attributes) {
        for (const key in props.attributes) {
            el.setAttribute(key, props.attributes[key]);
        }
    }

    if (props.datasets) {
        for (const key in props.datasets) {
            el.dataset[key] = props.datasets[key];
        }
    }

    if (props.parent) {
        if (!props.parent instanceof HTMLElement) {
            throw new Error('Parent is not an HTMLElement');
        }
        props.parent.appendChild(el);
    }

    return el;
}