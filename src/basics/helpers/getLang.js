export default function () {
    const lang = document.querySelector('html').getAttribute('lang');
    if (!lang) throw new Error('Not found lang attribute in html tag');

    return lang;
}