import { AlertComponent } from '../basics/components/AlertComponent/AlertComponent.js';

const defaultApiGateway = 'http://localhost';

export default class Client {
	static getEndpoint(uri) {
		return defaultApiGateway + uri;
	}

	static send(obj = {}, url, timeout = 5000) {
		if (typeof obj !== 'object') throw new Error('First param should be an object');

		/**
         * В тело запроса может передаваться экземпляр объекта FormData. Такой запрос поступит
         * на сервер в формате multipart/form-data, который парсится, например, через Multer
         */
		const body = obj instanceof FormData ? obj : JSON.stringify(obj);

		const rs = Promise.race([
			fetch(url, {
				method: 'POST',
				body,
			}),
			new Promise((_, reject) => {
				setTimeout(() => reject(new Error('Время запроса превысило таймаут')), timeout);
			})
		]);

		return this._handler(rs);
	}

	static async _handler(rs) {
		try {
			const response = await rs;

			/**
             * Check that http-status in the range 200-299
             */
			if (!response.ok) {
				const message = `Http error ${response.status} ${response.statusText}`;
				const alert = new AlertComponent();
				alert.push(message);
				throw new Error('Http Error');
				//return;
			}

			const data = await response.json();

			if (!Object.keys(data).length) {
				/**
                 * Empty response is not an error and
                 * should handle on the call side
                 */
				return null;
			}

			if (data.nextPage) return window.location.assign(data.nextPage);

			if (data.error) {
				const alert = new AlertComponent();
				alert.push(data.error.message);
				return data.error;
			}

			if (data.alert) {
				const alert = new AlertComponent();
				return alert.push(data.alert.message);
			}

			return data;
		} catch (e) {
			/**
             * TODO
             * Ошибки таймаута можно передавать дальше,
             * т.к. вызывающий код обычно предусматривает
             * интерфейс типа "Загрузить повторно"
             */
			console.error('POST request error: ', e.message);
		}
	}
}