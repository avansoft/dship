export class InputShadowComponent {
    constructor(value) {
        this._value = value;
    }

    get value() {
        return this._value;
    }
}