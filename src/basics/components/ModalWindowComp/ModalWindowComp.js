import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class ModalWindowComp extends BaseComponent {
    constructor(props) {
        super(props, {
            beforeEl: {
                type: 'object',
                require: true,
            }
        });
        this.beforeEl = this.props.beforeEl;

        // Wrapper and background
        this.el = this.createElement({
            type: 'div',
            classes: ['st-c-modalWindow'],
        })
        this.beforeEl.appendChild(this.el);

        // Window
        this.windowEl = this.createElement({
            type: 'div',
            parent: this.el,
            classes: ['_window'],
        })

        // Header
        this.headerEl = this.createElement({
            type: 'div',
            parent: this.windowEl,
            classes: ['_header'],
        })

        // Title
        this.titleEl = this.createElement({
            type: 'div',
            parent: this.headerEl,
            classes: ['_title'],
        })

        // Close button
        this.closeBtnEl = this.createElement({
            type: 'button',
            parent: this.headerEl,
            classes: ['_closeBtn'],
        })

        // Content
        this.contentEl = this.createElement({
            type: 'div',
            parent: this.windowEl,
            classes: ['_content'],
        })

        /**
         * Посольку события всплывают, достаточно повесить один обработчик и проверять target.
         * 
         * Рездельное отслеживание событий мыши (вместо единственного click)
         * позволяет избежать ситуации, когда пользователь при выделении элемента
         * на форме (clickdown) случайно отпустил мышку в области оверлея
         * и окно закрылось.
         * 
         * Кроме того, это позволяет реализовать ожидаемое для десктопных
         * приложений поведение: если пользователь случайно нажал мышкой на кнопку
         * закрытия окна, у него остается шанс избежать закрытия, отпустив мышку
         * вне области closeBtn
         */
        this.mousedownTargetEl = null;
        this.el.addEventListener('mousedown', ev => this.mousedownTargetEl = ev.target);
        this.el.addEventListener('mouseup', ev => this.closeWindow(ev.target));

        // Hide scrollbar for maximize viewport
        document.body.style.overflow = 'hidden';
    }

    showContent(el) {
        // remove old content
        if (this.contentEl.childNodes > 0) {
            while (this.contentEl.firstChild) {
                this.contentEl.removeChild(this.contentEl.firstChild);
            }
        }

        // add new content
        this.contentEl.appendChild(el);
    }

    closeWindow(mouseupTargetEl) {
        // Список элементов, которые могут закрывать окно
        const closeEls = [this.el, this.closeBtnEl];

        /**
         * Проверяем, что мышка не только была зажата на элементе, который предусматривает
         * закрытие окна, но и отпущена на таком же элементе.
         */
        if (closeEls.includes(mouseupTargetEl) && closeEls.includes(this.mousedownTargetEl)) {
            this.el.remove();
            document.body.style.overflow = 'auto';
        }

        this.mousedownTargetEl = null;
    }
}