import { BaseInput } from "../../abstracts/BaseInput.js";

export class InputTextareaComponent extends BaseInput {
    constructor(el) {
        super(el);

        this.input = this.el.querySelector('textarea');
        if (!this.input) throw new Error(`${this.constructor.name} -> Not found textarea element`);
    }

    get value() {
        return this.input.value;
    }

    set value(v) {
        this.input.value = v;
    }

    reset() {
        this.value = '';
    }
}