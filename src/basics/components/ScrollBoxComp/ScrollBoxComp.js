import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class ScrollBoxComp extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
        });
        this.el = this.props.el;

        /**
         * Определяем контейнер, который содержит пункты меню
         * (или другие элементы) и прокручивается по горизонтали
         * относительно корневого контейнера (el)
         */
        this.contentEl = this.el.firstElementChild;
        if (!this.contentEl) {
            throw new Error('Not found contentEl for scrolling');
        }

        // Disabled draggable API
        this._stopDraggable();

        /**
         * По умолчанию левая граница прокручиваемого контейнера
         * прижата к родителю, поэтому отступ нулевой
         */
        this.marginLeft = 0;

        /**
         * Снимаем обработчик события клика по ссылке,
         * чтобы он не конфликтовал с перетаскиванием
         */
        this.contentEl.addEventListener('click', ev => {
            ev.preventDefault();
        });

        /**
         * Подписываемся на события, которые инициируют
         * перемещение прокручиваемого блока
         */
        this.contentEl.addEventListener('mousedown', ev => this._onMouseDown(ev));
        this.contentEl.addEventListener('touchstart', ev => this._onMouseDown(ev));
    }

    /**
     * По умолчанию изображения и ссылки активны для Drag&Drop.
     * Чтобы они не конфликтовали с перемещением, отключаем
     * этот режим
     */
    _stopDraggable() {
        const links = this.el.querySelectorAll('a');
        if (links) {
            for (const link of links) {
                link.setAttribute('draggable', 'false');
            }
        }

        const images = this.el.querySelectorAll('img');
        if (images) {
            for (const image of images) {
                image.setAttribute('draggable', 'false');
            }
        }
    }

    _onMouseDown(ev) {
        /**
         * Запоминаем x-координату нажатия клавиши мыши
         */
        const startPoint = ev.clientX;

        /**
         * Объявляем переменную, которая отслеживает
         * перемещение по горизонтали (delta x)
         */
        let dx = 0;

        /**
         * Предполагается, что событие будет завершено
         * в качестве клика. Однако в случае вызова функции
         * перемещения этот статус отменяется
         */
        let isClick = true;

        /**
         * Таргет позволит определить, выполнялся ли клик
         * по ссылке
         */
        const target = ev.target;

        const onMouseMove = ev => {
            /**
             * Вычисляем предельный отрицательный отступ, т.е. минимальное
             * значение marginLeft для прокручиваемого контейнера, при котором
             * правая граница этого контейнера не будет отрываться
             * правой границы вьюпорта
             */
            const marginLeftMin = this.el.offsetWidth - this.contentEl.scrollWidth;

            // При смещении вправо положительный dx
            dx = ev.clientX - startPoint;


            /**
             * Считаем, что если курсор переместился более, чем на
             * указанное число пикселей, то обрабатывать событие
             * в качестве клика не нужно
             */
            if (Math.abs(dx) > 3) isClick = false;


            if ((this.marginLeft + dx) > 0) {
                /**
                 * Положительный marginLeft означает, что прокручиваемый
                 * контейнер будет оторван от левой границы вьюпорта.
                 * Поскольку такое поведение недопустимо, запрещаем
                 * положительный margin
                 */
                this.marginLeft = dx = 0;
            } else if ((this.marginLeft + dx) < marginLeftMin) {
                /**
                 * Аналогично запрещаем отрицательный margin,
                 * при котором прокручиваемый контейнер окажется
                 * оторван от правой границы вьюпорта
                 */
                this.marginLeft = marginLeftMin;
                dx = 0;
            } else {
                /**
                 * Во всех остальный случаях меняем marginLeft
                 * и добиваемся перемещения
                 */
                this.contentEl.style.marginLeft = (this.marginLeft + dx) + 'px';
            };
        }

        const onMouseUp = () => {
            /**
             * Глобально сохраняем смещение от левого края.
             * Это позволит начать следующее смещение с
             * прежней точки отсчета
             */
            this.marginLeft = parseInt(this.contentEl.style.marginLeft) || 0;

            /**
             * Обрабатываем событие в качестве клика, если первоначальный
             * клик/тап был по ссылке и само событие не потеряло статуса
             * клика из-за перемещения прокручиваемого контейнера
             */
            if (target.tagName == 'A' && isClick) {
                const link = target.getAttribute('href');
                document.location.assign(link);
            }

            // Remove move-events
            this.contentEl.removeEventListener('mousemove', onMouseMove);
            this.contentEl.removeEventListener('touchmove', onMouseMove);

            // Remove up-events
            document.removeEventListener('mouseup', onMouseUp);
            document.removeEventListener('touchend', onMouseUp);
        }

        this.contentEl.addEventListener('mousemove', onMouseMove);
        this.contentEl.addEventListener('touchmove', onMouseMove);
        /**
         * Это событие дожно отслеживаться в пределах страницы,
         * а не только блока, на котором инициирован компонент.
         * Иначе когда пользователь будет отпускать курсор за
         * пределами компонента, прокрутка будет продолжаться
         */
        document.addEventListener('mouseup', onMouseUp);
        document.addEventListener('touchend', onMouseUp);
    }

    showElement(el) {
        // Отступ до правой границы вьюпорта
        const viewportRightOffset = this.el.getBoundingClientRect().right;

        // Отступ до правой границы активного пункта
        const itemRightOffset = el.getBoundingClientRect().right;

        /**
         * Если элемент выходит за границу вьюпорта, то координата
         * его правой границы будет больше координаты правой
         * границы вьюпорта
         */
        if (viewportRightOffset < itemRightOffset) {
            const offset = viewportRightOffset - itemRightOffset;
            this.contentEl.style.marginLeft = offset + 'px';
            this.marginLeft = offset;
        }
    }
}