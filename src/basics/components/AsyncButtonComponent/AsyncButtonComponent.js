import { BaseComponent } from '../../abstracts/BaseComponent.js';

export class AsyncButtonComponent extends BaseComponent {
	constructor(props) {
		super(props, {
			el: {
				type: 'object',
				require: true,
			},
			command: {
				type: 'function',
				require: true,
			},
			successEventName: {
				type: 'string',
				require: true,
				default: 'asyncButtonOk',
			},
		});
		
		// Others
		this.styleName = 'st-c-asyncButton';
		this.loaderStyleName = '_loader';

		// Init
		this.props.el.classList.add(this.styleName);
		this.props.el.addEventListener('click', () => this.clickHandler());
	}

	clickHandler() {
		// Set loader
		this.props.el.classList.add(this.loaderStyleName);
		const response = this.props.command();

		/**
		 * Например, в случае переадресации, Client вернет undefined
		 * вместо промиса
		 */
		if (response instanceof Promise) {
			response
				.then(obj => {
					// Make and send event
					this.dispatchEvent(new CustomEvent(this.props.successEventName, {
						detail: obj,
					}));
					// Remove loader
					this.props.el.classList.remove(this.loaderStyleName);
				})
				.catch(e => console.log(e));
		} else {
			// Remove loader
			this.props.el.classList.remove(this.loaderStyleName);
			// Make and send event
			this.dispatchEvent(new CustomEvent(this.props.successEventName));
		}
	}
}