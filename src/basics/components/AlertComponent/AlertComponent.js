import { BaseComponent } from '../../abstracts/BaseComponent.js';

export class AlertComponent extends BaseComponent {
    constructor() {
        super();
        this.el = document.querySelector('.js-alert');
        this.closeTimeout = 5000;

        if (!this.el) {
            const headerEl = document.querySelector('header');
            if (!headerEl) throw new Error('Header element not found');

            const html = `
                <div class="st-c-alert js-alert">
                    <div class="_container"></div>
                </div >`;

            headerEl.insertAdjacentHTML('afterbegin', html);
            this.el = document.querySelector('.js-alert');
        }
    }

    push(message) {
        const containerEl = this.el.querySelector('._container');
        if (!containerEl) throw new Error('Container element didn\'t found');

        // Create item
        const itemEl = document.createElement('div');
        itemEl.classList.add('_item');

        // Create message
        const messageEl = document.createElement('p');
        itemEl.classList.add('_message');
        messageEl.textContent = message;
        itemEl.appendChild(messageEl);

        // Create close button
        const removeEl = document.createElement('button');
        removeEl.classList.add('_remove');
        messageEl.after(removeEl);

        // Get first item
        const firstItem = containerEl.getElementsByClassName('_item')[0];

        // Add by reverse order
        firstItem ? firstItem.before(itemEl) : containerEl.append(itemEl);

        removeEl.addEventListener('click', _ => {
            this.removeItem(itemEl);
        });

        setTimeout(() => {
            this.removeItem(itemEl);
        }, this.closeTimeout);
    }

    removeItem(el) {
        el.classList.add('js-transparent');
        setTimeout(() => {
            el.remove();
        }, 400);
    }

    hide() {
        this.el.classList.remove('js-overlay-show');
    }

    show() {
        this.el.classList.add('js-overlay-show');
    }
}