import { BaseComponent } from '../../abstracts/BaseComponent.js';
import { FormComponent } from '../FormComponent/FormComponent.js';

export class ChatThreadComp extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
            issueMessageCreateUri: {
                type: 'string',
                require: true,
            },
            // Income as a string, not number
            chatId: {
                type: 'string',
                require: true,
            },
        });

        this.el = this.props.el;
        this.messages = new Map();
        this.formEl = this.el.querySelector('.js-form-createMessage');
        this.sendMessageForm = null;
    }

    init() {
        // Grab messages
        this.el.querySelectorAll('.js-messageItem').forEach(el => {
            this.messages.set(el.dataset.id, el);
        });

        // Form init
        if (this.formEl) {
            const formSentEventName = 'chatThreadFormSent';

            this.sendMessageForm = new FormComponent({
                el: this.formEl,
                apiUri: this.props.issueMessageCreateUri,
                formSentEventName,
                fields: {
                    issueId: {
                        type: 'shadow',
                        initValue: this.props.chatId,
                    },
                    text: {
                        type: 'textarea',
                        selector: '.js-textarea-message',
                    },
                },
            });
            this.sendMessageForm.init();

            // Catching success sent event
            this.sendMessageForm.addEventListener(
                formSentEventName,
                ev => this.formSentHandler(ev)
            );
        }
    }

    formSentHandler(ev) {
        const { messageId, content } = ev.detail;
        if (!content) return;

        const formEl = this.el.querySelector('.js-form-createMessage');

        // Build messageList
        let wrapperEl = this.el.querySelector('.js-messageList');

        if (!wrapperEl) {
            wrapperEl = document.createElement('div');
            wrapperEl.classList.add('js-messageList');
            formEl.before(wrapperEl);
        }

        // Build messageItem
        const messageEl = document.createElement('div');
        messageEl.classList.add('_messageContainer', 'js-messageItem');
        messageEl.dataset.id = messageId;
        wrapperEl.appendChild(messageEl);

        // Build info for messageItem
        const infoEl = document.createElement('div');
        infoEl.classList.add('_messageInfo');
        messageEl.appendChild(infoEl);

        // Build text for messageItem
        const textEl = document.createElement('p');
        textEl.classList.add('_messageText');
        textEl.innerText = content;
        messageEl.appendChild(textEl);

        // Push messageItem to message collection
        this.messages.set(messageId, messageEl);

        // Clear form
        const textarea = this.sendMessageForm.getFieldInstance('text');
        textarea.reset();
    }

    hideForm() {
        this.formEl.style.display = 'none';
    }
}