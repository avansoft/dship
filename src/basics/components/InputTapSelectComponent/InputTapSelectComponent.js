import { BaseInput } from "../../abstracts/BaseInput.js";

export class InputTapSelectComponent extends BaseInput {
    constructor(el) {
        super(el);

        this.button = this.el.querySelector('button');
        if (!this.button) throw new Error('Not found button-tag');

        this.itemsBlock = this.el.querySelector('ul');
        if (!this.itemsBlock) throw new Error('Empty options block (query "ul")');

        this.items = this.itemsBlock.getElementsByTagName('li');
        if (!this?.items?.length) throw new Error('Empty options list (query "li")');

        this.currentActiveEl = undefined;

        el.addEventListener('click', ev => {
            // If click on the button
            if (ev.target === this.button) {
                this.itemsDisplayToggle();
            }

            // If click on the item
            if (ev.target.tagName === 'LI') {
                this.tapItem(ev.target);
            }
        })

        /**
         * Если для пункта атрибут data имеет значение
         * 'data-is-default', имитируем клик
         */
        Array.prototype.forEach.call(this.items, el => {
            if (el.dataset.isDefault === '') this.tapItem(el);
        });
    }

    tapItem(el) {
        const content = el.textContent;

        // Change button text
        this.button.textContent = content;

        // Hide options block
        //this.el.classList.remove('js-showItems');
        this.itemsDisplayToggle();

        // Set active item
        el.classList.add('js-active');

        if (this.currentActiveEl) this.currentActiveEl.classList.remove('js-active');
        this.currentActiveEl = el;
    }

    itemsDisplayToggle() {
        const currentMode = this.itemsBlock.style.display;

        if (currentMode === 'block') {
            this.itemsBlock.style.display = 'none';
            this.el.classList.remove('js-showItems');
        } else {
            this.itemsBlock.style.display = 'block';
            this.el.classList.add('js-showItems');
        }
        // //this.el.classList.toggle('js-showItems');

        // this.itemsBlock.style.display === 'block'
        //     ? this.itemsBlock.style.display = 'none'
        //     : this.itemsBlock.style.display = 'block';
    }

    get value() {
        return this.currentActiveEl ? this.currentActiveEl.getAttribute('name') : '';
    }
}