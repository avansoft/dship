import { BaseInput } from "../../abstracts/BaseInput.js";

export class InputCheckboxComponent extends BaseInput {
    constructor(el) {
        super(el);
        this.input = this.el.querySelector('input');
    }

    get value() {
        return this.input.checked;
    }
}