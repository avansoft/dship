import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class PasswordCheckerComp extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
            // Поле, за которым следим
            inputEl: {
                type: 'object',
                require: true,
            },
            lang: {
                type: 'string',
                require: true,
                default: 'ru',
            },
            checkers: {
                type: 'object',
                require: true,
            },
        });

        this.el = this.props.el;

        /**
         * Подписка на события инпута, за которым следим
         */
        this.inputEl = this.props.inputEl;
        if (this.inputEl instanceof EventTarget) {
            this.inputEl.addEventListener('input', ev => {
                this._validate(ev.target.value);
            })
        } else {
            throw new Error('AppError: inputEl should be an instanse of EventTarget');
        }

        /**
         * Make validators array
         */
        this.validators = {
            minLength: {
                dict: {
                    ru: 'Минимум 8 символов',
                    en: 'Min 8 digits',
                },
                validator: v => v.length >= 8,
            },
            containUpperCase: {
                dict: {
                    ru: 'Хотя бы одна заглавная буква',
                    en: 'One or more uppercase',
                },
                validator: v => /[A-Z]+/.test(v),
            },
            containDigit: {
                dict: {
                    ru: 'Хотя бы одно число',
                    en: 'One or more numbers',
                },
                validator: v => /\d+/.test(v),
            },
            containSpecSymbol: {
                dict: {
                    ru: 'Любой спец-символ: _-\/.*+()$?|',
                    en: 'One of symbols: _-\/.*+()$?|',
                },
                validator: v => /[-_\\/\.*+()$?|]+/.test(v),
            },
        }

        // Создание DOM-узла для валидаторов
        const checkersGroupEl = document.createElement('ul');
        this.el.prepend(checkersGroupEl);

        // Формирования списка валидаторов и элементов DOM
        this.activeValidators = [];
        for (let propsCheckerName in this.props.checkers) {
            if (this.validators[propsCheckerName]) {
                // Создание элемента
                const checkerEl = document.createElement('li');
                const checkerElText = this.validators[propsCheckerName]['dict'][this.props.lang];
                checkerEl.textContent = checkerElText;
                checkersGroupEl.append(checkerEl);

                // Пополнение массива активных валидаторов
                this.activeValidators.push({
                    name: propsCheckerName,
                    checkerEl,
                    // Копируем правила из валидатора
                    ...this.validators[propsCheckerName],
                })
            } else {
                throw new Error(`AppError: not found rules for propsChecker with name [${propsCheckerName}]`);
            }
        }

        this.isPasswordValid = false;
    }

    _validate(value) {
        this.el.hidden = this.isPasswordValid = this.activeValidators.reduce((acc, obj) => {
            obj.checkerEl.hidden = obj.validator(value);
            return acc && obj.checkerEl.hidden;
        }, true);
    }
}
