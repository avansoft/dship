import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class TeaserListComp extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
        });
        this.el = this.props.el;
        this.el.addEventListener('click', (ev) => this.clickHandler(ev));
    }

    clickHandler(ev) {
        const fullSizeImageLink = ev.target.dataset.fs;
        if (fullSizeImageLink)
            this.dispatchEvent(new CustomEvent('clickOnTeaser', {
                detail: {
                    fullSizeImageLink,
                    fileDigestId: ev.target.dataset.id,
                },
            }));
    }
}