import { BaseComponent } from '../../abstracts/BaseComponent.js';
import Client from '../../Client.js';
// Inputs
import { InputTextlineComponent } from '../InputTextlineComponent/InputTextlineComponent.js';
import { InputPasswordComponent } from '../InputPasswordComponent/InputPasswordComponent.js';
import { InputCheckboxComponent } from '../InputCheckboxComponent/InputCheckboxComponent.js';
import { InputViewerComponent } from '../InputViewerComponent/InputViewerComponent.js';
import { InputTextareaComponent } from '../InputTextareaComponent/InputTextareaComponent.js';
import { InputTapSelectComponent } from '../InputTapSelectComponent/InputTapSelectComponent.js';
import { InputMultifileComponent } from '../InputMultifileComponent/InputMultifileComponent.js';
import { InputCascadeSelectComponent } from '../InputCascadeSelectComponent/InputCascadeSelectComponent.js';
import { InputShadowComponent } from '../InputShadowComponent/InputShadowComponent.js';
// Components
import { AsyncButtonComponent } from '../AsyncButtonComponent/AsyncButtonComponent.js';

export class FormComponent extends BaseComponent {
	constructor(props) {
		super(props, {
			el: {
				type: 'object',
				require: true,
			},
			apiUri: {
				type: 'string',
				require: true,
			},
			fields: {
				type: 'object',
				require: true,
			},
			sendButtonSelector: {
				type: 'string',
				require: true,
				default: '.js-btnSend',
			},
			formSentEventName: {
				type: 'string',
				require: true,
				default: 'formSent',
			}
		});

		// props
		this.el = this.props.el;
		this.formSentEventName = this.props.formSentEventName;
		this.apiUri = this.props.apiUri;
		this.propsFields = this.props.fields;
		this.sendButtonSelector = this.props.sendButtonSelector;

		// privates
		this.fields = [];
		this.wrongFields = [];
		this.topErrorClassName = 'js-topError';
		this.topErrorEl = null;
	}

	init() {
		// init send button
		const sendButtonEl = this.el.querySelector(this.sendButtonSelector);
		if (sendButtonEl) {
			/**
             * В качестве обработчика клика назначается метод
             * отправки формы
             */
			const sendButtonInst = new AsyncButtonComponent({
				el: sendButtonEl,
				command: () => this.sendForm(),
				successEventName: this.formSentEventName,
			});

			/**
             * Кнопка создаст кастомное событие, в которое упакует результат
             * вызова процедуры отправки формы — объект data, полученный
             * от сервера. Этот объект будет передан в обработчик
             * для возможной маркировки полей с ошибками валидации
             */
			sendButtonInst.addEventListener(
				this.formSentEventName, ev => this.responseHandler(ev.detail)
			);
		} else {
			throw new Error(`Not found btnSend element with selector [${this.sendButtonSelector}]`);
		}

		// init field components
		const fields = this.propsFields;
		if (!Object.keys(fields).length) throw new Error('Not found fields in props');
		for (const key in fields) {
			const elType = fields[key].type;
			if (!elType) throw new Error(`Absent type for field [${key}]`);

			// init shadow control
			if (elType === 'shadow') {
				const initValue = fields[key].initValue || '';
				this.addShadowValue(key, initValue);
				continue;
			}

			// init visible contols
			const selector = fields[key].selector;
			if (!selector) throw new Error(`Absent selector for field [${key}]`);

			// Некоторые компоненты могут принимать дополинетльные опции
			const props = fields[key].props || {};

			if (elType === 'textbox') { this.addTextboxField(selector, key); continue; }
			if (elType === 'passbox') { this.addPasswordField(selector, key); continue; }
			if (elType === 'checkbox') { this.addCheckbox(selector, key); continue; }
			if (elType === 'viewbox') { this.addViewbox(selector, key); continue; }
			if (elType === 'textarea') { this.addTextarea(selector, key); continue; }
			if (elType === 'tapSelect') { this.addTapSelect(selector, key); continue; }
			if (elType === 'cascadeSelect') { this.addCascadeSelect(selector, key, props); continue; }
			if (elType === 'multifile') { this.addMultifile(selector, key); continue; }

			throw new Error(`Unhandled type [${elType}] for field [${key}]`);
		}
	}

	_addField(fieldName, inst) {
		this.fields.push({
			fieldName,
			inst,
		});
	}

	/**
     * Метод возвращает экземпляр компонента, который
     * соответствует полю формы
     */
	getFieldInstance(fieldName) {
		const inst = this.fields.find(v => v.fieldName === fieldName).inst;

		if (!inst) {
			throw new Error(`Not found instance for field '${fieldName}'`);
		}

		return inst;
	}

	async sendForm() {
		// TODO Проверка, что у всех полей isValid = true (предварительная валидация);

		// Get values from fields
		const formData = await this.getFormData();

		// API request
		const url = Client.getEndpoint(this.apiUri);
		return Client.send(formData, url);
	}

	responseHandler(data) {
		console.log(data);
		/**
         * In timeout case form can get a null-response
         */
		if (data) {
			// Reset errors
			this.removeTopError();
			this.removeFieldErrors();

			const errors = data.formErrors;
			if (errors) {
				// Set errors
				if (errors.topError) this.setTopError(data.formErrors.topError);
				if (errors.fieldErrors) this.setFieldErrors(data.formErrors.fieldErrors);
			} else {
				// Send event
				this.dispatchEvent(new CustomEvent(this.formSentEventName, {
					detail: data,
				}));
			}
		}
	}

	async getFormData() {
		const formData = new FormData();

		this.fields.forEach(v => {
			if (v.inst instanceof Promise) {
				v.inst.then(inst => {
					formData.set(v.fieldName, inst.value);
				});
			} else if (v.inst instanceof InputMultifileComponent) {
				v.inst.value.forEach(file => {
					formData.append(v.fieldName, file, file.name);
				});
			} else {
				formData.set(v.fieldName, v.inst.value);
			}
		});

		return formData;
	}

	setTopError(message) {
		if (this.topErrorEl) {
			this.topErrorEl.textContent = message;
		} else {
			this.el.insertAdjacentHTML('afterbegin',
				`<p class="${this.topErrorClassName}">${message}</p>`
			);

			this.topErrorEl = this.el.querySelector(`.${this.topErrorClassName}`);
		}
	}

	removeTopError() {
		this.topErrorEl ? (this.topErrorEl.remove(), this.topErrorEl = null)
			: this.topErrorEl = null;
	}

	setFieldErrors(fieldErrors) {
		this.removeFieldErrors();

		for (let fieldName in fieldErrors) {
			// Get input instance
			const inst = this.fields.filter(v => v.fieldName === fieldName)[0].inst;
			const errorMessage = fieldErrors[fieldName];

			// Make top-error for hidden-fields
			if (inst instanceof InputShadowComponent) {
				this.setTopError(errorMessage);
				continue;
			}

			// All inputs have same error handle interface
			inst.setError(errorMessage);

			// Filling error collection
			this.wrongFields.push({
				fieldName,
				inst,
			});
		}
	}

	removeFieldErrors() {
		if (this.wrongFields) {
			this.wrongFields.forEach(v => {
				// All inputs have same error handle interface
				v.inst.removeError();
			});

			// Reset error collection
			this.wrongFields = [];
		}
	}

	addShadowValue(fieldName, value) {
		this._addField(fieldName, new InputShadowComponent(value));
	}

	addCascadeSelect(selector, fieldName, props) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputCascadeSelectComponent(el, props).init());
	}

	addMultifile(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputMultifileComponent(el).init());
	}

	addTapSelect(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputTapSelectComponent(el));
	}

	addTextarea(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputTextareaComponent(el));
	}

	addViewbox(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputViewerComponent(el));
	}

	addCheckbox(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputCheckboxComponent(el));
	}

	addTextboxField(selector, fieldName) {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputTextlineComponent(el));
	}

	addPasswordField(selector, fieldName = 'password') {
		const el = this.el.querySelector(selector);
		this._addField(fieldName, new InputPasswordComponent(el));
	}
}
