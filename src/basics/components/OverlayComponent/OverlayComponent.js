import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class OverlayComponent extends BaseComponent {
    constructor(props) {
        super(props, {
            // Элемент, который является оберткой для оверлея
            parentEl: {
                type: 'object',
                require: true,
            },
            // HTML-блок с содержимым элемента
            contentEl: {
                type: 'object',
                require: true,
            },
            // If top, then overlay will slide from up to down
            basePosition: {
                type: 'string',
                require: true,
                default: 'top',
            }
        });
        this.parentEl = this.props.parentEl;
        this.contentEl = this.props.contentEl;
        this.basePosition = this.props.basePosition;

        // Setting parentEl for the correct position
        this.parentEl.style.position = 'relative';
        this.parentEl.style.overflow = 'hidden';

        // Create overlay container
        this.el = document.createElement('div');
        this.el.classList.add('st-c-overlay');

        /**
         * Вычисление размера родительского контейнера
         * должно выполняться только после того, как были
         * загружены внешние ресурсы (стили, картинки).
         * Иначе offsetHeight может вернуть некорректные
         * данные до применения стилей.
         */
        window.addEventListener('load', () => {
            // Задать отрицательный top (пока все оверлеи выезжают сверху)
            this.hidePosition();

            // Контент вкладывается в элемент, а затем выводится из теневого DOM
            this.el.append(this.contentEl);
            this.parentEl.prepend(this.el);
        })
    }

    /**
     * Помещает элемент за границы родительского контейнера
     */
    hidePosition() {
        // Вычисляем высоту контейнера, в который помещен оверлей
        this.parentElHeight = this.parentEl.offsetHeight;

        // Позиционирование
        switch (this.basePosition) {
            case 'top': {
                this.el.style.top = 0 - this.parentElHeight + 'px';
                break;
            }
            default: {
                throw new Error(`Value [${this.basePosition}] is not valid for position`);
            }
        }
    }

    hide() {
        /**
         * Чтобы элемент не становился частично виден при изменении
         * адаптивной верстки, его нужно скрыть. Начальное позиционирование
         * нужно по этой же причине: высота родительского контейнера
         * могла поменяться. Кроме того, для свойства visibility должно
         * быть такое же значение анимации, как и для перемещения,
         * иначе элемент будет скрыт до того, как отработает анимация
         * перемещения.
         */
        this.hidePosition();
        this.el.style.visibility = 'hidden';
    }

    show() {
        this.hidePosition();
        this.el.style.visibility = 'visible';
        this.el.style.top = '0px';
    }
}