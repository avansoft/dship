import { BaseComponent } from "../../abstracts/BaseComponent.js";
import Client from "../../Client.js";

export class ListLoaderComp extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
            templMaker: {
                type: 'function',
                require: true,
            },
            apiUrl: {
                type: 'string',
                require: true,
            },
            loadButtonCssClassName: {
                type: 'string',
                require: true,
                default: '__loadButton',
            },
            loadButtonTitle: {
                type: 'string',
                require: true,
                default: 'Load more...',
            },
        });
        this.el = this.props.el;

        /**
         * Wrapper for the teasers
         */
        this.wrapper = this.el.querySelector('.js-wrapper');
        if (!this.wrapper) {
            throw new Error('Teasers wrapper isn\'t found');
        }

        /**
         * Определяем нижний элемент из списка. По мере
         * подгрузки данных этот элемент будет
         * обновляться
         */
        this.lastChild = this.wrapper.lastElementChild;
        if (this.lastChild) {
            /**
             * Создаем наблюдателя
             */
            this.observer = new IntersectionObserver(() => this._loadItems(), { threshold: 1.0 });
            this.observer.observe(this.lastChild);
        }
    }

    _loadItems() {
        /**
         * Отключаем элемент от наблюдения, чтобы не вызывать
         * повторных запросов
         */
        this.observer.unobserve(this.lastChild);

        /**
         * Вычисляем кол-во уже показанных нод,
         * чтобы определить offset для sql-запроса
         */
        const offset = this.wrapper.childElementCount;

        Client.send({ offset }, this.props.apiUrl)
            .then(arr => {
                if (arr) {
                    /**
                     * Получаем список html-шаблонов (просто тексты),
                     * из которых будет формироваться список элементов
                     */
                    const templates = this.props.templMaker(arr);

                    /**
                     * Create load button
                     */
                    const btn = this.createElement({
                        type: 'button',
                        attributes: {
                            button: 'button',
                        },
                        textContent: this.props.loadButtonTitle,
                        classes: [this.props.loadButtonCssClassName],
                        parent: this.el,
                    });

                    btn.addEventListener('click', () => this._showItemsBlock(templates, btn));
                }
            })
            .catch(() => this._createReloadButton());
    }

    _showItemsBlock(templates, btn) {
        // Remove load button
        btn.remove();

        // Add new teasers
        templates.forEach(templ => {
            this.wrapper.insertAdjacentHTML('beforeend', templ);
        });

        // Update and observe lastChild
        this.lastChild = this.wrapper.lastElementChild;
        this.observer.observe(this.lastChild);
    }

    /**
     * Метод вызывается при неуспешном обращении
     * к серверу и отобразит кнопку, по которой
     * можно отправить повторный запрос
     */
    _createReloadButton() {
        const btn = this.createElement({
            type: 'button',
            attributes: {
                button: 'button',
            },
            textContent: this.props.loadButtonTitle,
            classes: [this.props.loadButtonCssClassName],
            parent: this.el,
        });

        btn.addEventListener('click', () => {
            btn.remove();
            this._loadItems();
        });
    }
}