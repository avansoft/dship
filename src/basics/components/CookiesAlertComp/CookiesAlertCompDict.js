import { BaseDictionary } from "../../../../core/lib/BaseDictionary.js";

export class CookiesAlertCompDict extends BaseDictionary {
    static dict = {
        cookiesHeader: {
            ru: 'Уведомление о Cookies',
            en: 'Cookies notice',
        },
        cookiesText: {
            ru: 'Сайт использует файлы cookie. Продолжая пользоваться нашим сайтом, вы соглашаетесь на использование нами ваших файлов.',
            en: 'The site uses cookies. Continuing to enjoy our site, you agree to the use of your files.',
        },
        cookiesLinkTitle: {
            ru: 'Узнать больше',
            en: 'Read more',
        },
        cookiesButtonTitle: {
            ru: 'Я согласен',
            en: 'I agree',
        },
    }
}
