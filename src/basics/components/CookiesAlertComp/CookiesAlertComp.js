import { BaseComponent } from '../../abstracts/BaseComponent.js';
import { CookiesAlertCompDict } from './CookiesAlertCompDict.js';

export class CookiesAlertComp extends BaseComponent {
    constructor(props) {
        super(props, {
            parentEl: {
                type: 'object',
                require: true,
            },
            lang: {
                type: 'string',
                require: true,
            }
        });
        this.parentEl = this.props.parentEl;
        this.localStorageKey = 'isCookiesAccepted';
        this.dict = CookiesAlertCompDict.getDictionary(this.props.lang);

        if (!localStorage.getItem(this.localStorageKey)) {
            this.createWindow();
        }
    }

    createWindow() {
        // Window
        const el = this.createElement({
            type: 'div',
            parent: this.parentEl,
            classes: ['st-c-cookiesAlert'],
        });

        // Header
        this.createElement({
            type: 'h1',
            parent: el,
            textContent: this.dict.cookiesHeader,
        });

        // Text
        this.createElement({
            type: 'p',
            parent: el,
            textContent: this.dict.cookiesText,
        });

        // Link
        this.createElement({
            type: 'a',
            parent: el,
            textContent: this.dict.cookiesLinkTitle,
            attributes: {
                href: '/',
            }
        });

        // Close button
        const closeButtonEl = this.createElement({
            type: 'button',
            parent: el,
            classes: ['__closeBtn'],
            attributes: {
                type: 'button',
            },
        });
        closeButtonEl.addEventListener('click', () => this.closeWindow(el));

        // Accept button
        const acceptButtonEl = this.createElement({
            type: 'button',
            parent: el,
            textContent: this.dict.cookiesButtonTitle,
            attributes: {
                type: 'button',
            },
        });
        acceptButtonEl.addEventListener('click', () => {
            localStorage.setItem(this.localStorageKey, 'true');
            this.closeWindow(el);
        })
    };

    closeWindow(el) {
        el.remove();
    }
}