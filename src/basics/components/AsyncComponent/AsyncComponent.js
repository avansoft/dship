import { BaseComponent } from "../../abstracts/BaseComponent.js";

export class AsyncComponent extends BaseComponent {
    constructor(props) {
        super(props, {
            el: {
                type: 'object',
                require: true,
            },
            command: {
                type: 'function',
                require: true,
            },
            successEventName: {
                type: 'string',
                require: true,
                default: 'asyncRequestOk',
            },
        });

        // Init
        this.props.el.addEventListener('click', () => this.clickHandler());
    }

    clickHandler() {
        const response = this.props.command();

        /**
         * Например, в случае переадресации, Client вернет undefined
         * вместо промиса
         */
        if (response instanceof Promise) {
            response
                .then(obj => {
                    this.dispatchEvent(new CustomEvent(this.props.successEventName, {
                        detail: obj,
                    }));
                })
                .catch(e => console.log(e));
        } else {
            this.dispatchEvent(new CustomEvent(this.props.successEventName));
        }
    }
}