import './index.scss';
import { HeaderBlockFront } from './blocks/HeaderBlock/HeaderBlockFront.js';
import { MainMenuBlockFront } from './blocks/MainMenuBlock/MainMenuBlockFront.js';
import { FooterBlockFront } from './blocks/FooterBlock/FooterBlockFront.js';
import favicon from './assets/favicon.ico';

/**
 * Default blocks
 */

// Header
const headerEl = document.querySelector('.js-b-header');
if (headerEl) new HeaderBlockFront(headerEl).init();

// Main menu
const mainMenuEl = document.querySelector('.js-b-mainMenu');
if (mainMenuEl) new MainMenuBlockFront(mainMenuEl).init();

// Footer
const footerEl = document.querySelector('.js-b-footer');
if (footerEl) new FooterBlockFront(footerEl).init();
