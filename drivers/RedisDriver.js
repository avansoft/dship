import { createClient } from 'redis';

export class RedisDriver {
	constructor(config) {
		this.client = createClient(config);
		this.client.on('error', (e) => console.log('Redis Client Error -> ', e));
	}
	async _connect() {
		try {
			await this.client.connect();
		} catch (err) {
			throw new Error(`${this.constructor.name} -> _connect() -> ${err.message}`);
		}
	}

	async disconnect() {
		try {
			await this.client.disconnect();
			console.log('Disconnect!');
		} catch (err) {
			throw new Error(`${this.constructor.name} -> disconnect() -> ${err.message}`);
		}
	}

	async test() {
		try {
			if (!this.client.isOpen) await this._connect();
			const info = await this.client.info();
			console.log('Redis test: ', info);
		} catch (err) {
			throw new Error(`${this.constructor.name} -> test() -> ${err.message}`);
		}
	}

	async saveString(key, value, lifetime) {
		console.log('CacheDB.saveString()');

		if (!this.client.isOpen) await this._connect();

		try {
			await this.client.set(key.toString(), value.toString(), { LT: lifetime });
			return key;
		} catch (err) {
			throw new Error(`${this.constructor.name} -> saveString() -> ${err.message}`);
		}
	}

	async getString(key) {
		try {
			if (!this.client.isOpen) await this._connect();
			return await this.client.get(key);
		} catch (err) {
			throw new Error(`${this.constructor.name} -> getString() -> ${err.message}`);
		}
	}

	async saveObject(key, obj) {
		if (typeof obj !== 'object') {
			throw new Error(`${this.constructor.name} -> Во второй параметр должен быть передан объект`);
		}

		if (!this.client.isOpen) await this._connect();

		try {
			for (let fieldName in obj) {
				await this.client.hSet(key, fieldName, obj[fieldName]);
			}

			await this.client.hSet(key, 'upd', Date.now().toString());
			return key;
		} catch (err) {
			throw new Error(`${this.constructor.name} -> saveObject() -> ${err.message}`);
		}
	}

	async getObject(key) {
		try {
			if (!this.client.isOpen) await this._connect();
			return await this.client.hGetAll(key);
		} catch (err) {
			throw new Error(`${this.constructor.name} -> getObject() -> ${err.message}`);
		}
	}

	async remove(key) {
		if (!this.client.isOpen) await this._connect();

		try {
			await this.client.del(key);
			return key;
		} catch (err) {
			throw new Error(`${this.constructor.name} -> remove() -> ${err.message}`);
		}
	}
}
