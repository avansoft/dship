import pkg from 'pg';
const { Pool } = pkg;

export class PostgresDriver {
	constructor(config) {
		this.pool = new Pool(config);
		this.pool.on('error', err => {
			console.error('StoreDB Postgres error', err);
		});
	}

	async getPoolClient() {
		try {
			/**
             * Этот метод обрабатывает только ошибку подключения.
             * Из-за этого приходится переупаковывать промис.
             */
			return await this.pool.connect();
		} catch (e) {
			throw new Error('StoreDB -> poolConnect() ->', e.message);
		}
	}

	async query(sql) {
		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs;
		} catch (e) {
			throw new Error(`PostgresDriver -> ${sql}`);
		}
	}

	async test() {
		const sql = 'SELECT version();';
		const rs = await this.query(sql);
		console.log('StoreDB test: ', rs.rows[0].version);
	}

	async isTrue(scheme, key, obj) {
		const json = JSON.stringify(obj);
		const sql = `SELECT ${scheme}.is_${key}('${json}');`;
		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0]['is_' + key];
		} catch (e) {
			throw new Error(`StoreDB -> isTrue() -> ${sql}`);
		}
	}

	async isExist(scheme, entityName, obj) {
		const json = JSON.stringify(obj);
		const sql = `SELECT ${scheme}.is_exist_${entityName}('${json}');`;
		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0]['is_exist_' + entityName];
		} catch (e) {
			throw new Error(`StoreDB -> isExist() -> ${sql}`);
		}
	}

	async get(scheme, entityName, obj) {
		const json = JSON.stringify(obj);
		const sql = `SELECT ${scheme}.get_${entityName}('${json}');`;

		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0]['get_' + entityName];
		} catch (e) {
			throw new Error(`StoreDB -> get() -> ${sql}`);
		}
	}

	async recof(scheme, entityName, obj) {
		const json = JSON.stringify(obj);
		const sql = `SELECT * from ${scheme}.recof_${entityName}('${json}');`;

		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0];
		} catch (e) {
			throw new Error(`StoreDB -> recof() -> ${sql}`);
		}
	}

	async tabof(scheme, entityName, obj) {
		const json = obj ? JSON.stringify(obj) : '{}';
		const sql = `SELECT * from ${scheme}.tabof_${entityName}('${json}');`;

		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows;
		} catch (e) {
			throw new Error(`StoreDB -> tabof() -> ${sql}`);
		}
	}

	async upd(scheme, entityName, obj) {
		const json = JSON.stringify(obj);
		const sql = `CALL ${scheme}.upd_${entityName}(null, '${json}');`;

		try {
			const client = await this.getPoolClient();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0].p_result;
		} catch (e) {
			throw new Error(`StoreDB -> update() -> ${sql}`);
		}
	}

	/**
     * Создает связь двух сущностей и возвращает true в случае успеха.
     * Надо бы переименовать в setRel
     */
	async rel(scheme, entityName, obj) {
		const json = JSON.stringify(obj);
		const sql = `CALL ${scheme}.rel_${entityName}(null, '${json}');`;

		try {
			const client = await this.getPoolClient();
			await client.query(sql);
			client.release();
			return true;
		} catch (e) {
			throw new Error(`StoreDB -> rel() -> ${sql}`);
		}
	}
}
