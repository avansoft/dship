import { AssertionError } from '../../subs/errors/AssertionError.js';
import util from 'util';

const validTypes = [Number, String, Object, Array, Boolean, Function];
const validHttpCodes = [200, 303, 400, 403, 404, 409, 410, 500];

function isObject(v) {
    return v && (typeof v === 'object') && !Array.isArray(v);
}

export class Assert {

    fail(value, expected, message) {
        throw new AssertionError(message || `Failed value: ${util.inspect(value)}; ${expected !== undefined ? `Expect: ${util.inspect(expected.name || expected)}` : ''}`);
    }

    id(v) {
        const regexp = /^[1-9][0-9]*$/;
        return regexp.test(v) ? true : false;
    }

    name(v) {
        const regexp = /^[a-zA-Zа-яА-Я]*$/;
        return regexp.test(v) ? true : false;
    }

    login(v) {
        const regexp = /^[a-zA-Z0-9_]*$/;
        return regexp.test(v) ? true : false;
    }

    password(v) {
        return true;
    }

    email(v) {
        const regexp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
        return regexp.test(v) ? true : false;
    }

    mobilePhone(v) {
        const regexp = /^[0-9]{11,13}/;
        return regexp.test(v) ? true : false;
    }

    hash(value, type, { message } = {}) {
        let regex = undefined;
        switch (type) {
            case 'md5':
                regex = /^[0-9a-f]{32}$/;
                break;
            case 'sha256':
                regex = /^[0-9a-f]{64}$/;
                break;
            default:
                return new AssertionError(`В assert.hash() передан неизвестный тип: ${type}`);
        }

        if (!regex.test(value)) {
            this.fail(value, undefined, message || `String ${value} is not a ${type}`);
        }

        return true;
    }

    defined(value, { message } = {}) {
        if (value === undefined) this.fail(value, 'No undefined values', message);
        return true;
    }

    instanceOf(value, type, { message } = {}) {
        if (!(value instanceof type)) {
            this.fail(value, type, message || `Failed instance: ${util.inspect(value)}; Expect instance of ${util.inspect(type.name || type)} class`);
        }
        return true;
    }

    httpStatus(value, type = validHttpCodes, { message } = {}) {
        if (!validHttpCodes.includes(value)) {
            this.fail(value, validHttpCodes, message || `Assert.httpStatus accept one of ${validHttpCodes.toString()} codes`);
        }
        return true;
    }

    typeOf(value, type, { message } = {}) {
        if (!validTypes.includes(type)) {
            this.fail(value, type, message || `Assert.typeOf accept one of [${validTypes.map(t => t.name)}] types. Use another method to validate "${type}"`);
        }

        if ((type === Number) && (typeof value === 'number') && !isNaN(value)) return true
        if ((type === String) && typeof value === 'string') return true
        if ((type === Object) && isObject(value)) return true
        if ((type === Array) && Array.isArray(value)) return
        if ((type === Boolean) && typeof value === 'boolean') return true
        if ((type === Function) && typeof value === 'function') return true

        this.fail(value, type, message)
    }
}

// const assertTest = new Assert;
// console.log(assertTest.hash('1b146165357c4ab30399f055de04383', 'md5'));