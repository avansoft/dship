import { Assert } from './Assert.js';
import siteConfig from '../../_configs/siteConfig.js';

export class GlobalContext {
	constructor(apps) {
		this.logger = apps.logger;

		this.cache = apps.cache;
		this.store = apps.store;
		this.uploader = apps.uploader;
		this.mailer = apps.mailer;

		this.config = siteConfig;
		this.assert = new Assert;

		this.logger.debug('GlobalContext (g) has been initialized');
	}
}