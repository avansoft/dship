import { GlobalContext } from './GlobalContext.js';

export class AbstractBase {
	constructor(g) {
		if (!(g instanceof GlobalContext)) {
			throw new Error(`В конструктор класса ${this.constructor.name} не передан экземпляр глобального контекста'`);
		}

		this.g = g;
		this.logger = g.logger;
		this.assert = g.assert;
		this.config = g.config;
	}
}