import TokenUtility from '../../utils/TokenUtility.js';
import { AuthError } from '../../subs/errors/AuthError.js';
import { AppError } from '../../subs/errors/AppError.js';

export class BaseAction {
	static checkAccess(g, ctx, permit) {
		g.logger.trace('Checking access');

		switch (permit) {
		case 'user': {
			if (ctx.session) return true;
			throw new AuthError(g, ctx);
		}
		case 'admin': return false;
		default: throw new AppError(g, ctx);
		}
	}

	static render(page, options) {
		return {
			render: {
				template: page.template,
				placeholders: page.placeholders,
			},
			status: page.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static exception(name, options) {
		const errorId = TokenUtility.getRandomString(15);
		return {
			exception: {
				name,
				errorId,
				data: {
					nextPage: '/e/' + errorId,
				}
			},
			status: options?.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static success(name, options) {
		const token = TokenUtility.getRandomString(10);
		return {
			success: {
				name,
				token,
				data: {
					nextPage: '/success/' + token,
				}
			},
			status: options?.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static forward(path, options) {
		return {
			forward: {
				data: {
					nextPage: path,
				},
			},
			status: options?.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static redirect(url, options) {
		return {
			redirect: {
				url,
			},
			status: options?.httpStatus || 303,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static alert(message, options) {
		return {
			alert: {
				message,
			},
			status: options?.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}

	static data(obj, options) {
		return {
			data: {
				obj,
			},
			status: options?.httpStatus || 200,
			setCookies: options?.setCookies,
			removeCookies: options?.removeCookies,
		};
	}
}