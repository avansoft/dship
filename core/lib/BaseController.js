import { AbstractBase } from './AbstractBase.js';
import { ActionContext } from './ActionContext.js';
import { AppError } from '../../subs/errors/AppError.js';

export class BaseController extends AbstractBase {
	constructor(g) {
		super(g);
		this.email = g.email;
	}

	actionRunner(action) {
		return async (req, res, next) => {
			const logger = this.logger;
			logger.debug(`${this.constructor.name} -> actionRunner()`);

			const ctx = new ActionContext(this.g, req);

			try {
				logger.trace('Awaiting action result...');
				const result = await action.run({ g: this.g, ctx });

				logger.debug('Start an action response handling');
				if (!result) {
					throw new AppError('got empty result from action. Did you call RETURN.<handler>()?');
				}

				if (result?.setCookies?.length) {
					for (const cookie of result.setCookies) {
						res.cookie(cookie.name, cookie.value, cookie.options);
						logger.trace(`Set cookie with name [${cookie.name}]`);
					}
				}

				if (result?.removeCookies?.length) {
					for (const cookie of result.removeCookies) {
						res.clearCookie(cookie.name, cookie.options);
						logger.trace(`Removed cookie with name [${cookie.name}]`);
					}
				}

				if (result.render) {
					this.logger.end('page rendering');
					res.status = result.status;
					const placeholders = await result.render.placeholders;
					return res.render(
						result.render.template,
						placeholders,
					);
				}

				if (result.success) {
					this.logger.end('success');
					await this.g.cache.saveString(
						result.success.token,
						result.success.name,
						3600 * 2);
					return res
						.status(result.status)
						.json(result.success.data);
				}

				if (result.exception) {
					this.logger.end('exception');
					await this.g.cache.saveString(
						result.exception.errorId,
						result.exception.name,
						3600 * 2);
					return res
						.status(result.status)
						.json(result.exception.data);
				}

				if (result.redirect) {
					this.logger.end('redirect');
					return res.redirect(
						result.status,
						result.redirect.url,
					);
				}

				if (result.forward) {
					this.logger.end(`forwarding to ${result.forward.data.path}`);
					return res
						.status(result.status)
						.json(result.forward.data);
				}

				if (result.alert) {
					this.logger.end('alert');
					return res
						.status(result.status)
						.json(result.alert);
				}

				if (result.data) {
					this.logger.end('data');
					return res
						.status(result.status)
						.json(result.data.obj);
				}

				next(new Error('unknown result type from action: ', res));
			} catch (err) {
				/**
				 * Do not handling action errors here, throw it to the ErrorMiddleware.
				 * For example, form errors catchs here.
				 */
				next(err);
			}
		};
	}
}