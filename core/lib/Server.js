import express from 'express';
import nunjucks from 'nunjucks';
import multer from 'multer';
import cookieParser from 'cookie-parser';

import { NotFoundError } from '../../subs/errors/NotFoundError.js';

import { StartMiddleware } from '../../middlewares/StartMiddleware.js';
import { FaviconMiddleware } from '../../middlewares/FaviconMiddleware.js';
import { ErrorMiddleware } from '../../middlewares/ErrorMiddleware.js';
import { CheckSessionMiddleware } from '../../middlewares/CheckSessionMiddleware.js';
import { TempUserMiddleware } from '../../middlewares/TempUserMiddleware.js';
import { MultilangMiddleware } from '../../middlewares/MultilangMiddleware.js';

import { SiteController } from '../../actions/site/SiteController.js';
import { SignupController } from '../../actions/signup/SignupController.js';
import { UserController } from '../../actions/user/UserController.js';
import { UserSessionController } from '../../actions/user-session/UserSessionController.js';
import { UserPasswordController } from '../../actions/user-password/UserPasswordController.js';
import { HelpdeskController } from '../../actions/helpdesk/HelpdeskController.js';

const jsonParser = express.json({
	type: 'text/plain',
});
const formParser = multer();

export class Server {
	constructor(g) {
		this.g = g;
		this.logger = g.logger;
	}

	start({ port, viewsPath, staticPath }) {
		return new Promise(async (resolve, _) => {
			const engine = express();

			/**
             * View Engines
             */
			engine.set('views', viewsPath);
			engine.set('view engine', 'njk');

			nunjucks.configure(viewsPath, {
				autoescape: true,
				express: engine,
			});

			engine.use(express.static(staticPath));

			/**
             * Middlewares
             */
			engine.use(new StartMiddleware(this.g).handler());
			engine.use(new FaviconMiddleware(this.g).handler());
			engine.use(cookieParser());
			engine.use(new CheckSessionMiddleware(this.g).handler());
			engine.use(new TempUserMiddleware(this.g).handler());
			engine.use(new MultilangMiddleware(this.g).handler());

			/**
             * Body parsing
             */
			//app.post('*', new BodyParser().handler);
			//engine.post('*', jsonParser);
			engine.post('*', formParser.array('files'), jsonParser);

			/**
             * Controllers
             */
			engine.use(new SiteController(this.g).routes);
			engine.use(new SignupController(this.g).routes);
			engine.use(new UserSessionController(this.g).routes);
			engine.use(new UserController(this.g).routes);
			engine.use(new UserPasswordController(this.g).routes);
			engine.use(new HelpdeskController(this.g).routes);

			/**
             * Not found route
             */
			engine.all('*', () => {
				throw new NotFoundError(this.g);
			});

			/**
             * Error handlers
             */
			engine.use(new ErrorMiddleware(this.g).handler());

			/**
             * Start Http-server and report object return
             */
			engine.listen(port, () => resolve({
				port,
				viewEngine: engine.get('view engine'),
				viewsFolder: engine.get('views'),
			}));

		});
	}

	async shutdown(code) {
		this.logger.debug(`\nTerminating with code: ${code}`);
		try {
			//await closePool();
			process.exit(0);
		} catch (err) {
			console.error(err.message);
			process.exit(1);
		}
	}
}
