import { AbstractBase } from './AbstractBase.js';

export class BaseMiddleware extends AbstractBase {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Constructor()`);
    }

    handler() { return new Error(`${this.constructor.name} should implement 'handler' method.`) }
}