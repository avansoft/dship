import { AbstractBase } from './AbstractBase.js';

export class BaseCookie extends AbstractBase {
	constructor(g, { value, maxAge, expires, domain, path, httpOnly, sameSite } = {}) {
		super(g);
		if (!value) throw new Error(`${this.constructor.name} -> Param value is required`);

		this.value = value;
		// Время жизни куки в секундах. Для снятия ставить 0?
		this.maxAge = maxAge || 0;
		// Срок истечения
		this.expires = expires || 0;
		// Куки пойдут на этот домен и все его поддомены
		this.domain = domain || this.config.domain;
		// Куки пойдут на все адреса от указанного корня
		this.path = path || '/';
		// Эти куки не доступны из JS
		this.httpOnly = httpOnly || true;
		// Разрешить отправку куки по запросам от сайтов на другом домене?
		this.sameSite = sameSite || true;
	}

	_make(name) {
		return {
			name,
			value: this.value,
			options: {
				//maxAge: this.maxAge,
				expires: this.expires,
				domain: this.domain,
				path: this.path,
				httpOnly: this.httpOnly,
				sameSite: this.sameSite,
			}
		};
	}
}