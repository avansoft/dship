import { AbstractBase } from './AbstractBase.js';
import TokenUtility from '../../utils/TokenUtility.js';

export class BaseDAO extends AbstractBase {
	constructor(g) {
		super(g);
		this.cache = g.cache;
		this.cacheLT = g.config.defaultCacheLT;
		this.isRemovable = false;
		this.store = g.store;

		this.logger.debug(`${this.constructor.name} -> construstor()`);
	}

	async execute(token, arg) {
		// Last char in function/procedure points to returning value type
		const postfix = token.slice(-1);
		
		function argFormatter () {
			switch (typeof arg) {
				case 'object': return `'${JSON.stringify(arg)}'`;
				case 'string': return `'${arg}'`;
				case 'number': return arg;
				case 'undefined': return '';
				default: throw new Error('Unsupported argument type');
			}
		}

		const getSql = () => {
			const prefixGet = token.slice(0,4) === 'get_';
			const prefixIs = token.slice(0,3) === 'is_';
			

			if (prefixGet || prefixIs) {
				// Only functions contains 'get_' or 'is_' prefix
				return `SELECT * FROM dao_${this.entityName}.${token}(${argFormatter()});`;
			} else if (postfix === '_') {
				// Call void procedure
				return `CALL dao_${this.entityName}.${token}(${argFormatter()});`;
			} else {
				// Call procedure with out-param (p_result)
				return `CALL dao_${this.entityName}.${token}(null, ${argFormatter()});`;
			}
		};

		// Sent query to the DB
		const sql = getSql();
		console.log(sql);
		const rs = await this.store.query(sql);
		console.log(`DB response for ${token}: ${rs.rows}`);

		// Parse DB response
		switch (postfix) {
			// For void
			case '_': return null;
			// For numbers including p_result
			case 'n': return rs.rows[0][token] || rs.rows[0]['p_result'];
			// For strings
			case 's': return rs.rows[0][token] || rs.rows[0]['p_result'];
			// For records
			case 'r': return rs.rows[0];
			// for tables
			case 't': return rs.rows;
			// for booleans
			case 'b': {
				if (rs.rows[0][token] === 'true') return true;
				return false;
			}
			default: throw new Error(`unknown postfix ${postfix}`);
		}
	}

	keyGen() {
		return TokenUtility.getUUID();
	}

	async getByKey(key) {
		const obj = await this.cache.getObject(key);
		if (Object.keys(obj).length == 0) return undefined;
		return obj;
	}

	async remove(key) {
		if (!this.isRemovable) throw new Error(`${this.constructor.name} -> remove() : Для данной сущности метод не поддерживается`);

		await this.cache.remove(key);
		this.logger.trace(`${this.constructor.name} -> Entity with key ${key} removed`);

		return key;
	}
}