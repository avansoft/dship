import { Assert } from './Assert.js';
import Normalizer from '../../utils/Normalizer.js';

const assert = new Assert;

export default class InputChecker {
    static password(v) {
        return assert.password(v);
    }

    static email(v) {
        const n = Normalizer.email(v);
        return assert.email(n);
    }

    static name(v) {
        const n = Normalizer.removeSpaces(v);
        return assert.name(n);
    }

    static mobilePhone(v) {
        return assert.mobilePhone(v);
    }

    static login(v) {
        return assert.login(v);
    }

    static id(v) {
        return assert.id(v);
    }

    static string(v) {
        return assert.instanceOf(v, 'string');
    }
}