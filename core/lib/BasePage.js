import { AbstractBase } from './AbstractBase.js';
import { HeaderBlockServ } from '../../src/blocks/HeaderBlock/HeaderBlockServ.js';
import { MainMenuBlockServ } from '../../src/blocks/MainMenuBlock/MainMenuBlockServ.js';
import { FooterBlockServ } from '../../src/blocks/FooterBlock/FooterBlockServ.js';

export class BasePage extends AbstractBase {
    constructor(g, ctx, props) {
        super(g);
        this.g = g;
        this.ctx = ctx;
        this.lang = ctx.lang;

        this.props = props;
        this.defaultLang = this.config.defaultLang;
        this.httpStatus = 200;

        /**
         * Блоки, которые актуальны для всех страниц. Инициализация
         * клиентской части выполняется в файле entry.js
         */
        this.commonBlocks = [
            HeaderBlockServ,
            MainMenuBlockServ,
            FooterBlockServ,
        ];
        /**
         * Список плейсхолдеров, полученных из локального словаря страницы
         */
        this._dictPlaceholders = {};
        /**
         * Пропсы больше не вкладываются в props.placeholders.
         * Считается, что в пропсы страницы можно передать только
         * плейсхолдеры. Источником для этого объекта будут
         * как явно переданные в props, так и добавленные
         * методом setPlaceholser();
         */
        this._propsPlaceholders = {};
        /**
         * Из этого объекта формируются мета-теги для передачи
         * данных со стороны сервера, когда API-запросы
         * не целесообразны.
         */
        this._metaPlaceholders = {};
        /**
         * Содержит список объектов с плейсхолдерами, которые
         * были получены из подключенных к странице блоков
         * (массив blocks дочернего экземпляра)
         */
        this._blockPlaceholders = {};
        /**
         * Зарезервированные имена префиксов для группировки
         * плейсхолдеров. Проверяются в chackName()
         */
        // TODO urls будет удален, т.к. ссылки будут формироваться на уровне блоков
        this.reservedNames = ['urls', 'user', 'static', 'data', 'props', 'meta'];
    }

    checkName(name) {
        if (this.reservedNames.includes(name)) {
            throw new Error(`${this.constructor.name} -> _make() -> key [${name} is reserved]`);
        }
    }

    setPlaceholder(name, value) {
        /**
        * Проверяем, что имена плейсхолдеров не конфликтуют
        * с именами предустановленных параметров
        * объекта шаблонизации
        */
        this.checkName(name);
        this._propsPlaceholders[name] = value;
    }

    setMeta(name, value) {
        this._metaPlaceholders[name] = value;
    }

    setHttpStatus(value) {
        this.assert.typeOf(value, Number);
        this.assert.httpStatus(value);
    }

    /**
     * Объект шаблонизации
     * На первом уровне вложенности должны находиться только объекты.
     * Причем каждый из таких объектов группируетконечные значения
     * плейсхолдеров по источнику данных. Например, в dict.sendButtonName
     * сразу понятно, что плейсхолдер sendButtonName был инициализирован
     * в словаре самой странице. А в случае с BlockName.dict.sendButtonName
     * уже в словаре подключенного к странице компонента BlockName.
     */
    get placeholders() {
        this.logger.debug(`${this.constructor.name} -> get placeholders()`);

        if (!this.config.langs.includes(this.lang)) throw new Error(
            `${this.constructor.name} -> Идентификатор языка отсутствует в конфиге или передан некорректно`
        );

        // Make dictionary placeholders
        for (const key in this.dict) {
            this._dictPlaceholders[key] = this.dict[key][this.lang] || key;
        }

        /**
         * Если при инициализации страницы в пропсы были переданы
         * параметры шаблонизации, заполняем их здесь
         */
        for (const key in this.props) {
            this.setPlaceholder(key, this.props[key]);
        }

        // Объединяеем общие блоки с блоками конкретной страницы
        const allBlocks = this.commonBlocks.concat(this.blocks);

        // Make promises array (it's normal if empty arr will return)
        const blockPromises = allBlocks
            ? allBlocks.map(block => {
                if (block) {
                    const blockInstance = new block(this.g, this.ctx);
                    return blockInstance.init();
                }
            })
            : [];

        return Promise.all(blockPromises).then(arr => {
            arr.forEach(blockObj => {
                const blockName = blockObj.selfName;

                /**
                 * Проверяем, что корневое имя для плейсхолдеров данного
                 * блока не будет конфликтовать с системными корнями
                 * объекта шаблонизации
                 */
                this.checkName(blockName);
                this._blockPlaceholders[blockName] = blockObj;
            });

            //! when you change this object, change this.reservedNames
            const obj = {
                /**
                 * Current user session info
                 */
                user: this.ctx.session || {},
                /**
                 * На уровне страницы может быть описан блок данных.
                 * Он передается в неизменном виде.
                 */
                data: this.data,
                /**
                 * Доступ к плейсходлержам каждого блока
                 * из шаблонизатора осуществлятся по ссылке
                 * вида BlockName.dict.sendButtonText
                 */
                ...this._blockPlaceholders,
                props: this._propsPlaceholders,
                dict: this._dictPlaceholders,
                meta: this._metaPlaceholders,
                //TODO will remove
                urls: this.g.urls,
            };
            return obj;
        });
    }
}