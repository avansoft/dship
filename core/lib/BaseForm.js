import { AbstractBase } from './AbstractBase.js';
import { ActionContext } from './ActionContext.js';
import { FormError } from '../../subs/errors/FormError.js';

export default class BaseForm extends AbstractBase {
	constructor(g, ctx) {
		super(g);
		if (!(ctx instanceof ActionContext)) {
			throw new Error(
				`Вторым аргументом конструктора класса ${this.constructor.name} должен быть передан экземпляр ActionContext'`
			);
		}
		this.ctx = ctx;

		if (ctx.lang.includes(this.supportedLangs)) {
			throw new Error(`${this.constructor.name} -> Передан неверный идентификатор языка`);
		}
		this.lang = ctx.lang;

		this.supportedLangs = this.g.config.langs;
		this.defaultLang = this.g.config.defaultLang;

		this._fields = {};
		this.fieldErrors = {};

		this.defaultErrors = {
			required: {
				ru: 'Это поле должно быть заполнено',
				en: 'This field should be fill',
			},
			incorrect: {
				ru: 'Поле заполнено некорректно',
				en: 'Incorrect value',
			},
		};
	}
	_make() {
		this.lang = this.ctx.lang;

		this.logger.debug(`${this.constructor.name} -> make()`);
		for (const fieldName in this.ctx.body) {

			/**
             * get match fields
             */
			const rule = this.scheme[fieldName];

			if (!rule) {
				throw new Error(`${this.constructor.name} -> Not found value for the field '${fieldName}'`);
			}

			let value = this.ctx.body[fieldName];

			/**
             * required
             */
			if (rule.required && !value) {
				this.fieldErrors[fieldName] =
                    this.defaultErrors.required[this.ctx.lang] || this.defaultErrors.required[this.defaultLang];
				this.logger.trace(`Required error in field [ ${fieldName} ]`);
				continue;
			}

			/**
             * validation
             */
			let isError = undefined;

			for (const item in rule.matchFields) {
				isError = false;

				// Get checker function
				const checker = rule.matchFields[item];

				if (!checker) throw new Error(`Not found checker function for field [${item}]`);

				if (checker(value)) {
					this.logger.trace(`Pair [${item}]-[${value}] is valid`);
					this._fields[item] = value;
					break;
				}

				/**
                 * Out with error if there are no successful validations
                 */
				isError = true;
			}

			if (isError) {
				this.fieldErrors[fieldName] =
                    this.scheme[fieldName]['errorText'][this.ctx.lang] || this.scheme[fieldName]['errorText'][this.defaulLang];
				this.logger.trace(`Validation error in field [${fieldName}]`);
			}

			/**
            * Все поля (даже с ошибкой) добавляются в список
            */
			this._fields[fieldName] = value;
		}

		if (Object.keys(this.fieldErrors).length) {
			throw new FormError(this.g, this.ctx, {
				errors: {
					fieldErrors: this.fieldErrors,
				},
			});
		}

		return this;
	}

	getFields() {
		this.logger.debug(`${this.constructor.name} -> BaseForm -> getFields()`);
		return this._fields;
	}

	/**
     * Этот метод взывается из внешнего кода, если нужно
     * вернуть форму с какой-то специфической ошибкой 
     */
	raiseError(errCode) {
		if (!this.topErrors[errCode]) {
			throw new Error(`raiseError() -> Not found error code ${errCode} in topErrors`);
		}

		throw new FormError(this.g, this.ctx, {
			errors: {
				topError: this.topErrors[errCode][this.lang] || this.topErrors[errCode][this.defaultLang],
			},
		});
	}

	get fields() {
		return this._fields;
	}
}
