export class BaseDictionary {
    static getDictionary(lang) {
        const obj = {};

        for (const key in this.dict) {
            obj[key] = this.dict[key][lang] || key;
        }

        return obj;
    }
}