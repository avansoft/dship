export class ActionContext {
    constructor(g, req) {
        g.logger.debug(`${this.constructor.name} -> constructor()`);

        this.body = req.body;
        this.files = req.files; //from multer
        this.lang = req.lang || obj.g.config.defaultLang;
        this.method = req.method;
        this.query = req.query; // ?provider=mail.ru
        this.params = req.params; // token/dfdfdf (предварительно в роуте token/:id)
        this.session = req.session;
        this.tempUser = req.tempUser;
        this.url = req.originalUrl; // Полный url запроса от корня, не зависит от точек монтирования
        this.referer = req.headers.referer;
        this.headers = {
            'Content-Type': req.get('Content-Type'),
            Referer: req.get('referer'),
            'User-Agent': req.get('User-Agent'),
        };
    }
}