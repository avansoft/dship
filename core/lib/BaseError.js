export class BaseError extends Error {
	constructor(g, ctx) {
		super();
		this.g = g;
		this.ctx = ctx;
	}
	get description() {
		return this._description[this?.ctx?.lang] || this._description['en'] || `${this.constructor.name}: Unexpected error`;
	}
}