export class BaseBlockServ {
    constructor(g, ctx) {
        this.g = g;
        this.ctx = ctx;
        this._state = {};

        /**
         * Плейсхолдеры, которые формируетются в методах
         * экземпляров дочерних классов. Доступ к ним из шаблона
         * осуществляется по ключу BlockName.self.valueName
         */
        this._selfPlaceholders = {};
    }

    /**
     * В тех случаях, когда блок имеет асинхронную логику,
     * этот метод переопределяется на уровне класса блока.
     * При этом дочерний init() всегда будет являться
     * асинхронным и возвразать тот же state с распакованными
     * промисами
     */
    init() {
        return this.state;
    }

    addToSelf(name, value) {
        this._selfPlaceholders[name] = value;
    }

    addToState(name, obj) {
        if (typeof obj !== 'object') throw new Error('Should be an object');
        this._state[name] = obj;
    }

    get state() {
        this._state.selfName = this.selfName;

        if (this.dict) {
            this.addToState('dict', this.dict.getDictionary(this.ctx.lang));
        }

        if (Object.keys(this._selfPlaceholders).length > 0) {
            this.addToState('self', this._selfPlaceholders);
        }

        if (this.data) {
            this.addToState('data', this.data);
        }

        return this._state;
    }

    get selfName() {
        return this.constructor.name.replace('Serv', '');
    }
}