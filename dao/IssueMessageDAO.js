import { BaseDAO } from '../core/lib/BaseDAO.js';

export class IssueMessageDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'issue_message';
	}

	async createIssueMessage(fields) {
		return super.execute('new_n', fields);
	}

	async issueMessageInfo(id) {
		return super.execute('get_r', id);
	}

	async getListByIssue(issueId) {
		return super.execute('get_by_issue_t', issueId);
	}
}