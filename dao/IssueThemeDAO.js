import { BaseDAO } from '../core/lib/BaseDAO.js';

export class IssueThemeDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'issue_theme';
	}

	async getIssueThemeList(lang) {
		return super.execute('get_by_lang_t', lang);
	}
}
