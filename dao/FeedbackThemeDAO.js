import { BaseDAO } from '../core/lib/BaseDAO.js';

export class FeedbackThemeDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'feedback_theme';
	}

	async getFeedbackThemeListByLang(lang) {
		return super.execute('get_by_lang_t', lang);
	}
}
