import { BaseDAO } from '../core/lib/BaseDAO.js';

export class SubjectDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'subject';
	}

	async createSubject(fields) {
		console.log(fields);
		return super.execute('new_n', fields);
	}

	async getSubjectInfo(id) {
		return super.execute('get_r', id);
	}

	async isSubjectExistByContacts(obj) {
		return super.execute('is_exist_by_contacts_b', obj);
	}

	async isPasswordSet(subjectId) {
		return super.execute('is_password_set_b', subjectId);        
	}

	async getPasswordSaltByContacts(obj) {
		return super.execute('get_salt_by_contacts_s', obj);
	}

	async getSubjectByPasswordHash(str) {
		return super.execute('get_by_password_hash_n', str);
	}

	async getSubjectIdByContacts(obj) {
		return super.execute('get_id_by_contacts_n', obj);
	}

	async updateSubjectPassword(obj) {
		return super.execute('upd_password_', obj);
	}
}
