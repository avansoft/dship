import { BaseDAO } from '../core/lib/BaseDAO.js';

export class TempUserDAO extends BaseDAO {
    constructor(g) {
        super(g);
        this.cacheLT = 5000;
        this.scheme = 'public';
        this.isRemovable = true;
    }

    async save(obj) {
        this.logger.debug(`${this.constructor.name} -> save()`);
        const key = this.keyGen();
        await this.cache.saveObject(key, obj);
        return key;
    }
}
