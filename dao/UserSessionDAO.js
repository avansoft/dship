import { BaseDAO } from '../core/lib/BaseDAO.js';
import TokenUtility from '../utils/TokenUtility.js';

export class UserSessionDAO extends BaseDAO {
    constructor(g) {
        super(g);
        this.cacheLT = 3600 * 48;
        this.scheme = 'public';

        this.isRemovable = true;
    }

    async create(userKey, opts) {
        this.logger.debug(`${this.constructor.name} -> create()`);

        this.logger.trace('Generetion userSessionKey');
        const userSessionKey = TokenUtility.getUUID();

        const sessionLT = opts?.sessionLT ? opts.sessionLT : this.cacheLT;

        this.logger.trace(`Call with userSessionKey [${userSessionKey}], userKey [${userKey}], lifetime [${sessionLT}]`);
        return this.cache.saveString(userSessionKey, userKey, sessionLT);
    }

    async getByKey(key) {
        this.logger.debug(`${this.constructor.name} -> getByKey()`);
        return this.cache.getString(key);
    }

    async renew(sid, userKey, sessionLT) {
        this.logger.debug(`${this.constructor.name} -> renew()`);
        return this.cache.saveString(sid, userKey, sessionLT);
    }
}
