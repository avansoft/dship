import { BaseDAO } from '../core/lib/BaseDAO.js';
import { NotFoundError } from '../subs/errors/NotFoundError.js';
import { HelpdeskURI } from '../actions/helpdesk/HelpdeskURI.js';

export class IssueDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'issue';
	}

	async createIssue(fields) {
		return super.execute('new_n', fields);
	}

	async removeIssue(issueId) {
		return super.execute('del_', issueId);
	}

	async getIssueInfo(issueId) {
		const rs = await super.execute('get_issue_t', issueId);
		if (!rs) throw new NotFoundError(this.g);
		return {
			...rs,
			isArchived: rs.isArchived == 1 ? true : false,
		};
	}

	async addFile(issueId, fileId) {
		return super.execute('rel_issue_file_digest_', { issueId, fileId });
	}

	async getActiveIssueList(issueId) {
		const rs = await super.execute('get_active_issue_t', issueId);
		const arr = rs.map(v => {
			return {
				...rs,
				issueUri: HelpdeskURI.getIssueItemURI(v.issue_id),
			};
		});
		return arr;
	}

	async getClosedIssueList(props) {
		const rs = await super.execute('get_closed_issue_t', props);
		console.log('-------------', rs);
		const arr = rs.map(v => {
			return {
				...rs,
				issueUri: HelpdeskURI.getIssueItemURI(v.issue_id),
			};
		});
		return arr;
	}

	/**
     * Метод возвращает число сообщений с момента последнего ответа
     * оператора (или число новых сообщений)
     */
	async getUnhandledIssueMessageCount(obj) {
		return super.execute('get_unhandled_messages_n', obj);
	}

	async getIssueImageList(issueId) {
		return super.execute('get_issue_image_t', issueId);
	}

	async markIssueAsClosed(issueId) {
		return super.execute('mark_issue_as_closed_', issueId);
	}
}