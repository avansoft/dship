import { BaseDAO } from '../core/lib/BaseDAO.js';

export class FeedbackDAO extends BaseDAO {
	constructor(g) {
		super(g);
		this.entityName = 'feedback';
	}

	async createFeedback(fields) {
		return super.execute('new_n', fields);
	}
}
