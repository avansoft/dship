import { BaseCookie } from '../../core/lib/BaseCookie.js'

export class TempUserCookie extends BaseCookie {
    constructor(g, params) {
        super(g, params);
        return this._make('tsid');
    }
}
