import { BaseCookie } from '../../core/lib/BaseCookie.js';

export class LangCookie extends BaseCookie {
	constructor(g) {
		super(g);
		this.name = 'lang';
	}
}
