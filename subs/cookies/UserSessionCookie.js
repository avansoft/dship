import { BaseCookie } from '../../core/lib/BaseCookie.js'

export class UserSessionCookie extends BaseCookie {
    constructor(g, params) {
        super(g, params);
        return this._make('sid');
    }
}
