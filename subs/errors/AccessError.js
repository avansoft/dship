import { BaseError } from "../../core/lib/BaseError.js";

export class AccessError extends BaseError {
    constructor(g, ctx) {
        super(g, ctx);
        this.method = ctx.method;
    }
}