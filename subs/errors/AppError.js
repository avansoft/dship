import { BaseError } from "../../core/lib/BaseError.js";
import { Err500PageServ } from "../../src/pages/Err500Page/Err500PageServ.js";

export class AppError extends BaseError {
    constructor(g, ctx, options) {
        super(g, ctx);
        this.httpStatus = 500;
        this.code = 'APP_ERROR';
        this._description = {
            ru: 'Внутренняя ошибка приложения',
            en: 'Internal app error',
        }
    }

    response(res) {
        if (this.ctx.method === 'GET') {
            const page = new Err500PageServ(this.g, this.ctx);
            res.status(this.httpStatus);
            res.render(page.template, page.placeholders);
        } else {
            res.status(this.httpStatus).json({
                code: this.code,
                error: {
                    message: this?.options?.description || this.description,
                },
            });
        }
    }
}