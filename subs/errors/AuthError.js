import { BaseError } from "../../core/lib/BaseError.js";
import { LoginPageServ } from "../../src/pages/LoginPage/LoginPageServ.js";

export class AuthError extends BaseError {
    constructor(g, ctx) {
        super(g, ctx);
        this.httpStatus = 401;
        this.code = 'AUTH_ERROR';
        this._description = {
            ru: 'Пожалуйста, представьтесь для выполнения этой операции',
            en: 'You need signup or login to proceed this operation',
        }
    }

    response(res) {
        if (this.ctx.method === 'GET') {
            const page = new LoginPageServ(this.g, this.ctx, {
                isForbiddenMessageActive: true,
            });
            page.placeholders.then(placeholders => {
                res.status(this.httpStatus);
                res.render(page.template, placeholders);
            })
        } else {
            res.status(this.httpStatus).json({
                code: this.code,
                error: {
                    message: this.description,
                },
            });
        }
    }
}