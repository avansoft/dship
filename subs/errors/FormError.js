import { BaseError } from '../../core/lib/BaseError.js';

export class FormError extends BaseError {
	constructor(g, ctx, props) {
		super(g, ctx);
		/**
		 * Использовать 400-й статус для бизнес-логики все-таки
		 * не совсем корректно, т.к. это ошибка пользователя,
		 * а не клиента. Кроме того, ошибки за пределами 2xx
		 * сразу ловятся Client.js и не передаются далее
		 * (то есть обработчик формы их не увидит и не сможет
		 * маркировать неправильные поля)
		 */
		this.httpStatus = 200;
		this.code = 'FORM_ERROR';
		this._description = {
			ru: 'Ошибка формы',
			en: 'Form error',
		};
		this.errors = props.errors;
	}

	response(res) {
		res.status(this.httpStatus).json({
			code: this.code,
			formErrors: {
				...this.errors,
			},
		});
	}
}