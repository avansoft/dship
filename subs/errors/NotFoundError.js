import { BaseError } from '../../core/lib/BaseError.js';
import { Err404PageServ } from '../../src/pages/Err404Page/Err404PageServ.js';

export class NotFoundError extends BaseError {
    constructor(g, ctx) {
        super(g, ctx);
        this.httpStatus = 404;
        this.code = 'NOT_FOUND_ERROR';
        this._description = {
            ru: 'Страница или объект не найден',
            en: 'Page or object didn\'t found',
        };
    }

    response(req, res) {
        if (req.method === 'GET') {
            const page = new Err404PageServ(this.g, req);
            page.placeholders.then(placeholders => {
                /**
                 * TODO I can't understand why res.status() doesn't
                 * work here
                 */
                res.statusCode = this.httpStatus;
                res.render(page.template, placeholders);
            });
        } else {
            res.statusCode = this.httpStatus;
            res.json({
                code: this.code,
                error: {
                    message: this?.props?.description || this.description,
                },
            });
        }
    }
}