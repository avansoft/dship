import { BaseError } from "../../core/lib/BaseError.js";

export class AssertionError extends BaseError {
    constructor(message) {
        super(message);
        this.message = message || 'Assertion error';
        this.code = 'ASSERTION_ERROR';
        this.httpStatus = 500;
    }
}