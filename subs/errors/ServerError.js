import { Err500PageServ } from '../../src/pages/Err500Page/Err500PageServ.js';

export class ServerError extends Error {
    constructor(g, req) {
        super();
        this.g = g;
        this.req = req;
        this.httpStatus = 500;
        this.code = 'UNEXPECTED_ERROR';
        this.description = {
            ru: 'Ошибка на стороне сервера',
            en: 'Server error',
        };
    }

    response(res) {
        if (this.req.method === 'GET') {
            const page = new Err500PageServ(this.g, this.req);
            res.status(this.httpStatus);
            res.render(page.template, page.placeholders);
        } else {
            res.status(this.httpStatus).json({
                code: this.code,
                error: {
                    message: this.description[this.req.lang] || this.code,
                },
            });
        }
    }
}