import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class SpamReportForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            email: {
                matchFields: {
                    email: InputChecker.email,
                },
                errorText: {
                    ru: 'Некорректный формат эл. почты',
                    en: 'Incorrect e-mail',
                },
                required: true,
            },
            name: {
                matchFields: {
                    name: InputChecker.name,
                },
                errorText: {
                    ru: 'Неверное указано имя',
                    en: 'Incorrect name',
                },
                required: true,
            },
            emailId: {
                matchFields: {
                    emailId: InputChecker.string,
                },
                errorText: {
                    ru: 'Неправильный ID письма',
                    en: 'Invalid Email ID',
                },
                required: true,
            },
            message: {
                matchFields: {
                    message: InputChecker.string,
                },
                errorText: {
                    ru: 'Поле заполнено с ошибками',
                    en: 'This field contain errors',
                },
                required: true,
            },
        }

        this._make();
    }
}
