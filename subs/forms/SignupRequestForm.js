import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class SignupRequestForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            email: {
                matchFields: {
                    email: InputChecker.email,
                },
                control: 'textbox',
                required: true,
                errorText: {
                    ru: 'Неверный формат электронной почты',
                    en: 'Incorrect e-mail format',
                },
            },
            mobilePhone: {
                matchFields: {
                    mobilePhone: InputChecker.mobilePhone,
                },
                control: 'textbox',
                required: true,
                errorText: {
                    ru: 'Укажите телефон в формате 7XXXXXXXXXX',
                    en: 'Please, use the format 7XXXXXXXXXX',
                },
            },
            firstName: {
                matchFields: {
                    firstName: InputChecker.name,
                },
                control: 'textbox',
                required: true,
                errorText: {
                    ru: 'Имя заполнено некорректно',
                    en: 'Incorrect first name',
                },
            },
        }

        this.topErrors = {
            alreadyExist: {
                ru: 'Пользователь с такими учетными данными уже зарегистрирован',
                en: 'User with such credentials already registered ',
            }
        }

        this._make();
    }
}
