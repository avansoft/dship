import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class LoginForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            login: {
                matchFields: {
                    email: InputChecker.email,
                    login: InputChecker.login,
                },
                errorText: {
                    ru: 'Формат не соответствует ни логину, ни эл. почте',
                    en: 'Incorrect e-mail or login',
                },
                control: 'TextboxControl',
                required: true,
            },
            password: {
                matchFields: {
                    password: InputChecker.password,
                },
                control: 'TextboxControl',
                required: true,
                errorText: {
                    ru: 'Некорректный пароль',
                    en: 'Incorrect password',
                },
            },
            isRemember: {
                matchFields: {
                    isRemember: v => true,
                },
                control: 'CheckboxControl',
                errorText: {
                    ru: 'Получено некорректное значение для параметра isRemember',
                    en: 'Incorrect parameter for isRemember',
                },
            }
        }

        this.topErrors = {
            userNotFound: {
                ru: 'Пользователь с такими учетными данными не найден',
                en: 'User not found',
            }
        }

        this._make();
    }
}
