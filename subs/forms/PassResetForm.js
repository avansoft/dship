import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class PassResetForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            email: {
                matchFields: {
                    email: InputChecker.email,
                },
                errorText: {
                    ru: 'Некорректный формат эл. почты',
                    en: 'Incorrect e-mail',
                },
                control: 'TextboxControl',
                required: true,
            },
        }

        this.topErrors = {
            userNotFound: {
                ru: 'Пользователь с такими учетными данными не найден',
                en: 'User with this login or email not exists',
            }
        }

        this._make();
    }
}
