import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class IssueMessageForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            issueId: {
                matchFields: {
                    issueId: InputChecker.id,
                },
                errorText: {
                    ru: 'Неверный формат issueId',
                    en: 'Incorrect format issueId',
                },
                required: true,
            },
            text: {
                matchFields: {
                    text: () => { return true },
                },
                errorText: {
                    ru: 'Поле заполнено с ошибками',
                    en: 'This field contain errors',
                },
                required: true,
            },
        }

        this._make();
    }
}
