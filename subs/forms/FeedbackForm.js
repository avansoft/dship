import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class FeedbackForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            email: {
                matchFields: {
                    email: InputChecker.email,
                },
                errorText: {
                    ru: 'Некорректный формат эл. почты',
                    en: 'Incorrect e-mail',
                },
                required: true,
            },
            firstName: {
                matchFields: {
                    firstName: InputChecker.name,
                },
                errorText: {
                    ru: 'Неверное указано имя',
                    en: 'Incorrect name',
                },
                required: true,
            },
            themeId: {
                matchFields: {
                    themeId: InputChecker.id,
                },
                errorText: {
                    ru: 'Неизвестная код темы обращения',
                    en: 'ThemeID not found',
                },
                required: true,
            },
            message: {
                matchFields: {
                    message: () => { return true },
                },
                errorText: {
                    ru: 'Поле заполнено с ошибками',
                    en: 'This field contain errors',
                },
                required: true,
            },
        }

        this._make();
    }
}
