import BaseForm from "../../core/lib/BaseForm.js";
import InputChecker from "../../core/lib/InputChecker.js";

export class PasswordUpdateForm extends BaseForm {
    constructor(g, ctx) {
        super(g, ctx);
        this.scheme = {
            password: {
                matchFields: {
                    password: InputChecker.password,
                },
                control: 'textbox',
                required: true,
                errorText: {
                    ru: 'Некорректный пароль',
                    en: 'Incorrect password',
                },
            },
        }
        this._make();
    }
}
