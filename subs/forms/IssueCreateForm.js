import BaseForm from '../../core/lib/BaseForm.js';
import InputChecker from '../../core/lib/InputChecker.js';

export class IssueCreateForm extends BaseForm {
	constructor(g, ctx) {
		super(g, ctx);
		this.scheme = {
			themeId: {
				matchFields: {
					themeId: InputChecker.id,
				},
				errorText: {
					ru: 'Неизвестная код темы обращения',
					en: 'ThemeID not found',
				},
				required: true,
			},
			message: {
				matchFields: {
					message: () => { return true; },
				},
				errorText: {
					ru: 'Поле заполнено с ошибками',
					en: 'This field contain errors',
				},
				required: true,
			},
			summary: {
				matchFields: {
					summary: () => { return true; },
				},
				errorText: {
					ru: 'Поле заполнено с ошибками',
					en: 'This field contain errors',
				},
				required: true,
			},
		};

		this._make();
	}
}
