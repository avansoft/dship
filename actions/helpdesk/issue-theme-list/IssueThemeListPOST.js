import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueThemeDAO } from '../../../dao/IssueThemeDAO.js';

export class IssueThemeListPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Get theme list from DB');
        const issueThemes = new IssueThemeDAO(g);
        const themes = await issueThemes.getIssueThemeList(ctx.lang);

        return this.data(themes);
    }
}