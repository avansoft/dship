import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueItemPageServ } from "../../../src/pages/IssueItemPage/IssueItemPageServ.js";

export class IssueItemGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        this.checkAccess(g, ctx, 'user');

        return this.render(new IssueItemPageServ(g, ctx));
    }
}
