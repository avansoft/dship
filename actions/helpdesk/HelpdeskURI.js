import { BaseURI } from "../../core/lib/BaseURI.js";

export class HelpdeskURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get support() {
        return '/support';
    }

    static get issueListPage() {
        return '/issues';
    }

    static get issueListAction() {
        return '/issues';
    }

    static get issueItemPage() {
        return '/issues/:id';
    }

    static get issueCreatePage() {
        return '/issue-create';
    }

    static get issueCreateAction() {
        return '/issue-create';
    }

    static get issueThemeList() {
        return '/issue-themes';
    }

    static get issueMessageCreateAction() {
        return '/issue-message-create';
    }

    static get issueArchivedAction() {
        return '/issue/:id/archived';
    }

    static getIssueArchivedURI(issueId) {
        return `/issue/${issueId}/archived`;
    }

    static getIssueItemURI(issueId) {
        return `/issues/${issueId}`;
    }
}
