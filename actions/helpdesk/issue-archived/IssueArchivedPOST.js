import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueDAO } from '../../../dao/IssueDAO.js';

export class IssueArchivedPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        this.checkAccess(g, ctx, 'user');

        const issueId = ctx.body.issueId;

        const issueDao = new IssueDAO(g);
        await issueDao.markIssueAsClosed(issueId);

        return this.data({ issueId });
    }
}