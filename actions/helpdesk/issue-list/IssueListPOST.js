import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueDAO } from '../../../dao/IssueDAO.js';

export class IssueListPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        this.checkAccess(g, ctx, 'user');

        const offset = ctx.body.offset;

        const issueDao = new IssueDAO(g);
        const obj = {
            userKey: ctx.session.userKey,
            offset,
        };
        const archivedIssues = await issueDao.getClosedIssueList(obj);

        return this.data(archivedIssues);
    }
}