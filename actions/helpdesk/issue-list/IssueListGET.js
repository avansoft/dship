import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueListPageServ } from '../../../src/pages/IssueListPage/IssueListPageServ.js';

export class IssueListGET extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);
		this.checkAccess(g, ctx, 'user');
		return this.render(new IssueListPageServ(g, ctx));
	}
}
