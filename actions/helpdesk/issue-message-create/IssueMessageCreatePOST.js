import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueMessageForm } from '../../../subs/forms/IssueMessageForm.js';
import { IssueDAO } from '../../../dao/IssueDAO.js';
import { IssueMessageDAO } from '../../../dao/IssueMessageDAO.js';
import { AppError } from '../../../subs/errors/AppError.js';

export class IssueMessageCreatePOST extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);

		this.checkAccess(g, ctx, 'user');

		logger.step('User data checking');
		const form = new IssueMessageForm(g, ctx);

		logger.step('Antiflud');
		const antifludLimit = g.config.modules?.support?.antifludLimit;

		if (!antifludLimit) throw new AppError(g, ctx, {
			description: 'Not found antifludLimit in config.modules.support'
		});

		const issueDao = new IssueDAO(g);
		const messageCount = await issueDao.getUnhandledIssueMessageCount({
			userKey: ctx.session.userKey,
			issueId: form.fields.issueId,
		});

		logger.trace(`Checking limit for ${messageCount} messages`);

		logger.step('Create message');
		const messageDao = new IssueMessageDAO(g);
		const messageId = await messageDao.createIssueMessage({
			userKey: ctx.session.userKey,
			issueId: form.fields.issueId,
			text: form.fields.text,
		});

		return this.data({
			messageId,
			content: form.fields.text,
			limit: antifludLimit - messageCount,
		});
	}
}