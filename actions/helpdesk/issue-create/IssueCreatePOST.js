import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueCreateForm } from '../../../subs/forms/IssueCreateForm.js';
import { IssueDAO } from '../../../dao/IssueDAO.js';
import { HelpdeskURI } from '../HelpdeskURI.js';

export class IssueCreatePOST extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);

		this.checkAccess(g, ctx, 'user');

		logger.step('User data checking');
		const form = new IssueCreateForm(g, ctx);

		logger.step('Save issue to store DB');
		const issueDao = new IssueDAO(g);
		const issueId = await issueDao.createIssue({
			userKey: ctx.session.userKey,
			...form.fields,
		});

		if (ctx.files) {
			logger.step('Start files saving...');

			try {
				for (const file of ctx.files) {
					logger.trace('Save file to the object storage');
					const fileId = await g.uploader.saveFile(file);

					logger.trace(`Associate file id=${fileId} with issueId id=${issueId}`);
					await issueDao.addFile(issueId, fileId);
				}
			} catch (err) {
				await issueDao.removeIssue(issueId);
				logger.trace(`Issue with id ${issueId} has been deleted`);

				// TODO add dictionsry for alerts or use IssueCreateError();
				return this.alert('Cannot create issue with files. Please, try again later.');
			}
		}

		return this.forward(HelpdeskURI.issueListPage);
	}
}