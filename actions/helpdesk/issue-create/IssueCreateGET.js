import { BaseAction } from '../../../core/lib/BaseAction.js';
import { IssueCreatePageServ } from '../../../src/pages/IssueCreatePage/IssueCreatePageServ.js';

export class IssueCreateGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);
        this.checkAccess(g, ctx, 'user');
        return this.render(new IssueCreatePageServ(g, ctx));
    }
}
