import { BaseController } from '../../core/lib/BaseController.js';
import { HelpdeskURI as urls } from './HelpdeskURI.js';
import { IssueListGET } from './issue-list/IssueListGET.js';
import { IssueCreateGET } from './issue-create/IssueCreateGET.js';
import { IssueItemGET } from './issue-item/IssueItemGET.js';
import { IssueThemeListPOST } from './issue-theme-list/IssueThemeListPOST.js';
import { IssueCreatePOST } from './issue-create/IssueCreatePOST.js';
import { IssueMessageCreatePOST } from './issue-message-create/IssueMessageCreatePOST.js';
import { IssueArchivedPOST } from './issue-archived/IssueArchivedPOST.js';
import { IssueListPOST } from './issue-list/IssueListPOST.js';
import express from 'express';

const router = express.Router();

export class HelpdeskController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        this.logger.debug(`${this.constructor.name} -> Get routes`);

        // Get
        router.get(urls.issueListPage, this.actionRunner(IssueListGET));
        router.get(urls.issueCreatePage, this.actionRunner(IssueCreateGET));
        router.get(urls.issueItemPage, this.actionRunner(IssueItemGET));

        // Post
        router.post(urls.issueThemeList, this.actionRunner(IssueThemeListPOST));
        router.post(urls.issueCreateAction, this.actionRunner(IssueCreatePOST));
        router.post(urls.issueMessageCreateAction, this.actionRunner(IssueMessageCreatePOST));
        router.post(urls.issueArchivedAction, this.actionRunner(IssueArchivedPOST));
        router.post(urls.issueListAction, this.actionRunner(IssueListPOST));

        return router;
    }
}