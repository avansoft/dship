import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PasswordResetNextPageServ } from '../../../src/pages/PasswordResetNextPage/PasswordResetNextPageServ.js';
import EmailProviders from '../../../utils/EmailProviders.js';

export class PasswordResetNextGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        /**
         * Получаем идентификатор провайдера, чтобы
         * сформировать ссылку для проверки почты
         */
        const domain = ctx.query.provider;

        return this.render(new PasswordResetNextPageServ(g, ctx, {
            btnLink: EmailProviders.getLinkByDomain(domain),
        }));
    }
}