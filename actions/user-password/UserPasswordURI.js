import { BaseURI } from "../../core/lib/BaseURI.js";
import Base64 from '../../utils/Base64.js';

export class UserPasswordURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get passwordUpdatePage() {
        return '/password-update';
    }

    static get passwordUpdateAction() {
        return '/password-update';
    }

    static get passwordUpdateSuccessPage() {
        return '/password-update-ok';
    }

    static get passwordSetSuccessTest() {
        return '/password/set/ok';
    }

    static get passwordResetPage() {
        return '/password-reset';
    }

    static get passwordResetAction() {
        return '/password-reset';
    }

    static get passwordResetNextPage() {
        return '/password-reset/next';
    }

    getPasswordUpdateUrl(token) {
        return this.baseURL + '/password-update?token=' + Base64.encode(token);
    }
}
