import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PasswordUpdateForm } from '../../../subs/forms/PasswordUpdateForm.js';
import { PasswordUtility } from '../../../utils/PasswordUtility.js';
import { UserPasswordURI } from '../UserPasswordURI.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';

export class PasswordUpdatePOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Checking user data');
        const form = new PasswordUpdateForm(g, ctx);

        logger.step('Checkin session');
        this.checkAccess(g, ctx, 'user');
        const userKey = ctx.session.userKey;

        logger.step('Hash and salt generation');
        const passwordUtility = new PasswordUtility(g);
        const secret = await passwordUtility.getHashWithSalt(form.fields.password);

        logger.step('Call StoreDB');
        const subjectDao = new SubjectDAO(g);
        await subjectDao.updateSubjectPassword({
            userKey,
            hash: secret.hash,
            salt: secret.salt,
        });

        return this.forward(UserPasswordURI.passwordUpdateSuccessPage);
    }
}

