import { BaseAction } from '../../../core/lib/BaseAction.js';
import { UserSessionDAO } from '../../../dao/UserSessionDAO.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';
import { UserSessionCookie } from '../../../subs/cookies/UserSessionCookie.js';
import { PasswordUpdatePageServ } from '../../../src/pages/PasswordUpdatePage/PasswordUpdatePageServ.js';
import DateUtility from '../../../utils/DateUtility.js';
import Base64 from '../../../utils/Base64.js';

export class PasswordUpdateGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        const subjectDao = new SubjectDAO(g);

        /**
         * Сперва проверяем наличие стандартной сессии
         */
        let userKey = ctx?.session?.userKey;
        if (userKey) {
            const isPasswordSet = await subjectDao.isPasswordSet(userKey);
            return this.render(new PasswordUpdatePageServ(g, ctx, { isPasswordSet }));
        }

        /**
        * Если сессии нет, пробуем авторизовать по токену
        */
        const token = ctx.query.token;
        if (token) {
            logger.trace(`Decode token [${token}]`);
            const sid = Base64.decode(token);

            logger.trace(`Get userId for sid [${sid}]`);
            const sessions = new UserSessionDAO(g);
            userKey = await sessions.getByKey(sid);

            if (userKey) {
                /**
                 * Устанавливаем сессионную куку, чтобы при отправке
                 * POST-запроса с новым паролем сервер мог
                 * авторизовать пользовательский запрос
                 */
                logger.trace(`Make one-day cookie for userKey [${userKey}] with sid [${sid}]`);
                const expires = DateUtility.tomorrow(1);

                const setCookies = [
                    new UserSessionCookie(g, {
                        value: sid,
                        expires,
                    }),
                ];

                /**
                * Проверяем, задан ли пароль для пользователя.
                * От этого зависит формат шаблона
                */
                const isPasswordSet = await subjectDao.isPasswordSet(userKey);
                return this.render(new PasswordUpdatePageServ(g, ctx, { isPasswordSet }), { setCookies });
            }
        }
        // Выбросит исключение
        this.checkAccess(g, ctx, 'user');
    }
}

