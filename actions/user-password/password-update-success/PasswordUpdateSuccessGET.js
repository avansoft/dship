import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PasswordUpdateSuccessPageServ } from '../../../src/pages/PasswordUpdateSuccessPage/PasswordUpdateSuccessPageServ.js';
import { UserURI } from "../../user/UserURI.js";

export class PasswordUpdateSuccessGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        return this.render(new PasswordUpdateSuccessPageServ(g, ctx, {
            btnLink: UserURI.profilePage,
        }));
    }
}