import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PasswordResetPageServ } from '../../../src/pages/PasswordResetPage/PasswordResetPageServ.js';

export class PasswordResetGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);
        return this.render(new PasswordResetPageServ(g, ctx));
    }
}