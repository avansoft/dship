import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PassResetForm } from '../../../subs/forms/PassResetForm.js';
import { UserSessionDAO } from '../../../dao/UserSessionDAO.js';
import { UserPasswordURI } from '../UserPasswordURI.js';
import EmailProviders from '../../../utils/EmailProviders.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';

export class PasswordResetPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Checking user data');
        const form = new PassResetForm(g, ctx);

        logger.step('Get userKey by contact');
        const subjectDao = new SubjectDAO(g);
        const userKey = await subjectDao.getSubjectIdByContacts({
            email: form.fields.email,
        });

        if (!userKey) {
            logger.trace('User not found');
            form.raiseError('userNotFound');
        }

        logger.step('Make a one-hour session');
        const sessions = new UserSessionDAO(g);
        const sid = await sessions.create(userKey, { sessionLT: 60 * 60 * 1 });

        logger.trace(`Make pass reset url for sid [${sid}]`);
        const urls = new UserPasswordURI(g);
        const url = urls.getPasswordUpdateUrl(sid);

        /**
         * Email sending
         */
        logger.trace('Get username from DB');
        const subjectInfo = await subjectDao.getSubjectInfo(userKey);
        const userEmail = form.fields.email;

        const report = await g.mailer.sendPasswordResetEmail(ctx, {
            to: userEmail,
            firstName: subjectInfo.firstName,
            btnLink: url,
        });
        logger.trace(`Email with ID [${report} has been sent]`);

        const forwardUrl = UserPasswordURI.passwordResetNextPage + '?provider=' + EmailProviders.getDomain(userEmail);
        return this.forward(forwardUrl);
    }
}