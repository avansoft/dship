import { BaseController } from "../../core/lib/BaseController.js";
import { UserPasswordURI as urls } from './UserPasswordURI.js';
import { PasswordResetGET } from "./password-reset/PasswordResetGET.js";
import { PasswordResetNextGET } from "./password-reset-next/PasswordResetNextGET.js";
import { PasswordUpdateGET } from "./password-update/PasswordUpdateGET.js";
import { PasswordResetPOST } from "./password-reset/PasswordResetPOST.js";
import { PasswordUpdatePOST } from "./password-update/PasswordUpdatePOST.js";
import { PasswordUpdateSuccessGET } from "./password-update-success/PasswordUpdateSuccessGET.js";
import express from 'express';

const router = express.Router();

export class UserPasswordController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        // Get
        router.get(urls.passwordResetPage, this.actionRunner(PasswordResetGET));
        router.get(urls.passwordResetNextPage, this.actionRunner(PasswordResetNextGET));
        router.get(urls.passwordUpdatePage, this.actionRunner(PasswordUpdateGET));
        router.get(urls.passwordUpdateSuccessPage, this.actionRunner(PasswordUpdateSuccessGET));

        // Post
        router.post(urls.passwordResetAction, this.actionRunner(PasswordResetPOST));
        router.post(urls.passwordUpdateAction, this.actionRunner(PasswordUpdatePOST));

        return router;
    }
}