import { BaseURI } from "../../core/lib/BaseURI.js";

export class SiteURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get index() {
        return '/';
    }

    static get actionError() {
        return '/e/:id';
    }

    static get actionSuccess() {
        return '/success/:id';
    }

    static get feedback() {
        return '/feedback';
    }

    static get testPage() {
        return '/test/:id';
    }
}
