import { BaseAction } from '../../../core/lib/BaseAction.js';
import { FeedbackSentPageServ } from '../../../src/pages/FeedbackSentPage/FeedbackSentPageServ.js';
import { AppError } from '../../../subs/errors/AppError.js';

export class ActionSuccessGET extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);

		const token = ctx.params.id;

		const pageName = await g.cache.getString(token);

		pageName
			? logger.trace(`Got page name ${pageName} by key [ ${token} ]`)
			: logger.trace(`Not found page with key [ ${token} ]`);

		const props = {
			placeholders: {
				token,
			}
		};

		switch (pageName) {
		case 'feedbackSent': return this.render(new FeedbackSentPageServ(g, ctx, props));
		default: throw new AppError(g, ctx);
		}
	}
}
