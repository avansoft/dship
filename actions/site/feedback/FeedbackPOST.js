import { BaseAction } from '../../../core/lib/BaseAction.js';
import { FeedbackForm } from '../../../subs/forms/FeedbackForm.js';
import { FeedbackDAO } from '../../../dao/FeedbackDAO.js';

export class FeedbackPOST extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);

		logger.step('Checking user data');
		const form = new FeedbackForm(g, ctx);

		logger.step('Save to Store');
		const feedbackDao = new FeedbackDAO(g);
		const id = await feedbackDao.createFeedback({
			firstName: form.fields.firstName,
			email: form.fields.email,
			themeId: form.fields.themeId,
			message: form.fields.message,
			userKey: ctx?.session?.userKey || '',
		});
		logger.trace(`Feedback record [${id}] has been created`);

		//TODO Create email for admin

		return this.success('feedbackSent');
	}
}