import { BaseAction } from '../../../core/lib/BaseAction.js';
import { FeedbackPageServ } from '../../../src/pages/FeedbackPage/FeedbackPageServ.js';

export class FeedbackGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);
        return this.render(new FeedbackPageServ(g, ctx));
    }
}
