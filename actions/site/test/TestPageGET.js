/**
 * Используетс для тестирования шаблонов страниц, верстки
 */
import { BaseAction } from '../../../core/lib/BaseAction.js';
import { ExcepSignupPageServ } from '../../../src/pages/ExcepSignupPage/ExcepSignupPageServ.js';
import { ExcepCookiesNotFoundPageServ } from '../../../src/pages/ExcepCookiesNotFoundPage/ExcepCookiesNotFoundPageServ.js';
import { ExcepUserAlreadyExistsPage } from '../../../src/pages/ExcepUserAlreadyExistsPage/ExcepUserAlreadyExistsPageServ.js';

export class TestPageGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        const pageName = ctx.params.id;

        switch (pageName) {
            case 'excep-signup': return this.render(new ExcepSignupPageServ(g, ctx, { errId: '555' }));
            case 'excep-cookies': return this.render(new ExcepCookiesNotFoundPageServ(g, ctx, { errId: '555' }));
            case 'excep-user-exists': return this.render(new ExcepUserAlreadyExistsPage(g, ctx, { errId: '555' }));
            // TODO NotFoundError
            default: throw new Error('Not found page');
        }
    }
}
