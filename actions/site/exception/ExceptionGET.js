import { BaseAction } from '../../../core/lib/BaseAction.js';
import { ExcepSignupPageServ } from '../../../src/pages/ExcepSignupPage/ExcepSignupPageServ.js';
import { ExcepCookiesNotFoundPageServ } from '../../../src/pages/ExcepCookiesNotFoundPage/ExcepCookiesNotFoundPageServ.js';
import { ExcepUserAlreadyExistsPage } from '../../../src/pages/ExcepUserAlreadyExistsPage/ExcepUserAlreadyExistsPageServ.js';
import { AppError } from '../../../subs/errors/AppError.js';

export class ExceptionGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        // Getting from Redis
        const errId = ctx.params.id;
        const pageName = await g.cache.getString(errId);

        pageName
            ? logger.trace(`Got page name ${pageName} by key [ ${errId} ]`)
            : logger.trace(`Not found page with key [ ${errId} ]`);

        const props = {
            placeholders: {
                errId,
            }
        }

        switch (pageName) {
            case 'userAlreadyExists': return this.render(new ExcepUserAlreadyExistsPage(g, ctx, props));
            case 'cookiesNotFound': return this.render(new ExcepCookiesNotFoundPageServ(g, ctx, props));
            case 'signupError': return this.render(new ExcepSignupPageServ(g, ctx, props));
            default: throw new AppError(g, ctx);
        }
    }
}
