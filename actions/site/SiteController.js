import { BaseController } from '../../core/lib/BaseController.js';
import { SiteURI as urls } from './SiteURI.js';
import express from 'express';

import { IndexGET } from './index/IndexGET.js';
import { ActionSuccessGET } from './action-success/ActionSuccessGET.js';
import { FeedbackGET } from './feedback/FeedbackGET.js';
import { TestPageGET } from './test/TestPageGET.js';
import { FeedbackPOST } from './feedback/FeedbackPOST.js';

const router = express.Router();

export class SiteController extends BaseController {
	constructor(g) {
		super(g);
		this.logger.debug(`${this.constructor.name} -> Initilazed`);
	}
	get routes() {
		this.logger.debug(`${this.constructor.name} -> Get routes`);

		/**
         * GET
         */
		router.get(urls.index, this.actionRunner(IndexGET));
		router.get(urls.actionSuccess, this.actionRunner(ActionSuccessGET));
		router.get(urls.feedback, this.actionRunner(FeedbackGET));
		router.get(urls.testPage, this.actionRunner(TestPageGET));

		/**
         * POST
         */
		router.post(urls.feedback, this.actionRunner(FeedbackPOST));

		return router;
	}
}