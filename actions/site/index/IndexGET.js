import { BaseAction } from '../../../core/lib/BaseAction.js';
import { MainPageServ } from '../../../src/pages/MainPage/MainPageServ.js';

export class IndexGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        return this.render(new MainPageServ(g, ctx));
    }
}
