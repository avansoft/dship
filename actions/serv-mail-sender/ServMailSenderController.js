import { BaseController } from '../../core/lib/BaseController.js';
import { AntispamURI as urls } from './ServMailSenderURI.js';
import { SpamReportCreatePOST } from './spam-report/SpamReportCreatePOST.js';
import express from 'express';

const router = express.Router();

export class ServMailSenderController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        this.logger.debug(`${this.constructor.name} -> Get routes`);

        // Get

        // Post
        router.post(urls.spamReportCreateAction, this.actionRunner(SpamReportCreatePOST));

        return router;
    }
}