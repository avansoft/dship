import { BaseURI } from "../../core/lib/BaseURI.js";

export class ServMailSenderURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get spamReportCreatePage() {
        return '/spam-report';
    }

    static get spamReportCreateAction() {
        return '/spam-report';
    }

    static getSpamReportUrl() {
        return ``;
    }
}
