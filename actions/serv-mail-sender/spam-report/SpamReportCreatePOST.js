import { BaseAction } from '../../../core/lib/BaseAction.js';
import { AntispamDAO } from '../../../dao/ServMailSenderDAO.js';
import { ServMailSenderDAO } from '../../../dao/ServMailSenderDAO.js';
import { SpamReportForm } from "../../../subs/forms/SpamReportForm.js";

export class SpamReportCreatePOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('User data checking');
        const form = new SpamReportForm(g, ctx);

        const antispam = new AntispamDAO(g);
        const reportId = await antispam.createReport({
            name: form.name,
            email: form.email,
            message: form.message,
            emailId: form.emailId,
        });
        logger.step(`Report with id ${reportId} has been created`);
    }
}