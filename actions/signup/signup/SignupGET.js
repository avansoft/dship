import { BaseAction } from '../../../core/lib/BaseAction.js';
import { SignupPageServ } from "../../../src/pages/SignupPage/SignupPageServ.js";

export class SignupGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);
        return this.render(new SignupPageServ(g, ctx));
    }
}