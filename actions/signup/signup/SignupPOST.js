import { BaseAction } from '../../../core/lib/BaseAction.js';
import { SignupRequestForm } from '../../../subs/forms/SignupRequestForm.js';
import { TempUserDAO } from '../../../dao/TempUserDAO.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';
import { SignupURI } from '../SignupURI.js';
import EmailProviders from '../../../utils/EmailProviders.js';

export class SignupPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('User data checking');
        const form = new SignupRequestForm(g, ctx);
        const userEmail = form.fields.email;


        logger.step('Check user duplicate');
        const subjectDao = new SubjectDAO(g);
        const isSubjectExist = await subjectDao.isSubjectExistByContacts({
            email: userEmail,
            mobilePhone: form.fields.mobilePhone,
        });

        if (isSubjectExist) {
            logger.trace('Break because user already exists');
            form.raiseError('alreadyExist');
        }

        logger.step('Saving tempUser');
        const tempUserKey = await new TempUserDAO(g)
            .save({
                ...form.getFields(),
            });

        logger.step('Creation email');
        const urls = new SignupURI(g);

        const emailId = await g.mailer.sendSignupConfirmEmail(ctx, {
            to: userEmail,
            confirmUrl: urls.getSignupConfirmURL(tempUserKey),
        });
        logger.trace(`Email with ID [${emailId} has been sent]`);

        return this.forward(SignupURI.signupNextPage + '?provider=' + EmailProviders.getDomain(userEmail));
    }
}