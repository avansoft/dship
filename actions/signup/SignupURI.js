import { BaseURI } from '../../core/lib/BaseURI.js';
import Base64 from '../../utils/Base64.js';

export class SignupURI extends BaseURI {
	constructor(g) {
		super(g);
	}

	static get signupPage() {
		return '/signup';
	}

	static get signupAction() {
		return '/signup';
	}

	static get signupNextPage() {
		return this.signupPage + '-next';
	}

	static get signupCompletePage() {
		return this.signupPage + '-complete';
	}

	static get signupCompleteAction() {
		return this.signupPage + '-complete';
	}

	getSignupConfirmURL(token) {
		return this.baseURL + this.constructor.signupCompletePage + '?token=' + Base64.encode(token);
	}
}
