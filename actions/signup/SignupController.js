import { BaseController } from "../../core/lib/BaseController.js";
import { SignupURI as urls } from './SignupURI.js';
import { SignupGET } from "./signup/SignupGET.js";
import { SignupNextGET } from "./signup-next/SignupNextGET.js";
import { SignupPOST } from "./signup/SignupPOST.js";
import { SignupCompleteGET } from "./signup-complete/SignupCompleteGET.js";
import { SignupCompletePOST } from "./signup-complete/SignupCompletePOST.js";
import express from 'express';

const router = express.Router();

export class SignupController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        this.logger.debug(`${this.constructor.name} -> Get routes`);
        // Get
        router.get(urls.signupPage, this.actionRunner(SignupGET));
        router.get(urls.signupNextPage, this.actionRunner(SignupNextGET));
        router.get(urls.signupCompletePage, this.actionRunner(SignupCompleteGET));

        // Post
        router.post(urls.signupAction, this.actionRunner(SignupPOST));
        router.post(urls.signupCompleteAction, this.actionRunner(SignupCompletePOST));

        return router;
    }
}