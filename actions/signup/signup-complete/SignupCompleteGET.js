import { BaseAction } from '../../../core/lib/BaseAction.js';
import { TempUserDAO } from '../../../dao/TempUserDAO.js';
import { TempUserCookie } from '../../../subs/cookies/TempUserCookie.js';
import { SignupCompletePageServ } from '../../../src/pages/SignupCompletePage/SignupCompletePageServ.js';
import { ProfilePageServ } from "../../../src/pages/ProfilePage/ProfilePageServ.js";
import { Err400PageServ } from '../../../src/pages/Err400Page/Err400PageServ.js';
import { ExcepSignupPageServ } from '../../../src/pages/ExcepSignupPage/ExcepSignupPageServ.js';
import Base64 from '../../../utils/Base64.js';

export class SignupCompleteGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        if (ctx.session) {
            /**
             * Если пользователь имеет постоянную сессию, то предполагается,
             * что временный пользователь удален и, следовательно, все
             * операции с ним не актуальны.
             */
            logger.trace('Return ProfilePage, because user already auth');
            return this.render(new ProfilePageServ(g, ctx));
        }

        const urlToken = ctx.query.token;
        if (!urlToken) {
            logger.trace('Return bad request error, because token not found');
            return this.render(new Err400PageServ(g, ctx));
        }

        logger.step('Get tempUser from CacheDB');
        const TempUserKey = Base64.decode(urlToken);
        const tempUser = await new TempUserDAO(g).getByKey(TempUserKey);
        if (tempUser) {
            logger.trace(`tempUser with key ${TempUserKey} has been found`);
            const page = new SignupCompletePageServ(g, ctx);
            const setCookies = [
                new TempUserCookie(g, { value: TempUserKey }),
            ]

            return this.render(page, { setCookies });
        }

        logger.warn(`tempUser with key ${TempUserKey} not found`);
        return this.render(new ExcepSignupPageServ(g, ctx));
    }
}

