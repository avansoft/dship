import { BaseAction } from '../../../core/lib/BaseAction.js';
import { PasswordUtility } from '../../../utils/PasswordUtility.js';
import { PasswordUpdateForm } from '../../../subs/forms/PasswordUpdateForm.js';
import { UserSessionCookie } from '../../../subs/cookies/UserSessionCookie.js';
import { TempUserCookie } from '../../../subs/cookies/TempUserCookie.js';
import { TempUserDAO } from '../../../dao/TempUserDAO.js';
import { UserSessionDAO } from '../../../dao/UserSessionDAO.js';
import DateUtility from '../../../utils/DateUtility.js';
import { UserURI } from '../../user/UserURI.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';

export class SignupCompletePOST extends BaseAction {
	static async run({ g, ctx } = {}) {
		const logger = g.logger;
		logger.action(this.name);

		logger.step('Loading tempUser from ctx');
		const tempUser = ctx.tempUser;
		if (!tempUser) {
			logger.trace('Not found tempUser cookies');
			return this.exception('cookiesNotFound');
		}

		logger.step('Checking user data');
		const form = new PasswordUpdateForm(g, ctx);

		logger.step('Check user duplicate');
		const subjectDao = new SubjectDAO(g);
		const isUserExists = await subjectDao.isSubjectExistByContacts({
			email: tempUser.email,
			mobilePhone: tempUser.mobilePhone,
		});
        
		if (isUserExists) {
			logger.trace('Break because user already exists');
			return this.exception('userAlreadyExists');
		}

		logger.step('Password hash and salt generation');
		const passwordUtility = new PasswordUtility(g);
		const secret = await passwordUtility.getHashWithSalt(form.fields.password);

		logger.step('Save user in StoreDB');
		const subjectId = await subjectDao.createSubject({
			firstName: tempUser.firstName,
			email: tempUser.email,
			mobilePhone: tempUser.mobilePhone,
			passwordHash: secret.hash,
			passwordSalt: secret.salt,
		});
		logger.trace(`Subject has been created with key ${subjectId}`);

		logger.step('Remove tempUser from CacheDB');
		const tempUsers = new TempUserDAO(g);
		await tempUsers.remove(tempUser.key);

		logger.step('Save user session to CacheDB');
		const userSessions = new UserSessionDAO(g);
		const userSessionKey = await userSessions.create(subjectId);

		logger.step('Make WelcomeEmail instance');
		const emailId = await g.mailer.sendWelcomeEmail(ctx, {
			to: tempUser.email,
			btnLink: g.config.baseURL,
		});
		logger.trace(`Email with ID [${emailId} has been sent]`);

		logger.step('Cookies manage');
		const expires = DateUtility.tomorrow(5);
		const setCookies = [
			new UserSessionCookie(g, {
				value: userSessionKey,
				expires,
			}),
		];
		const removeCookies = [
			new TempUserCookie(g, { value: tempUser.key }),
		];

		logger.step('Action final with page forwarding');
		return this.forward(UserURI.profilePage, { setCookies, removeCookies });
	}
}
