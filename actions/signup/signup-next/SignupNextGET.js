import { BaseAction } from '../../../core/lib/BaseAction.js';
import { SignupNextPageServ } from '../../../src/pages/SignupNextPage/SignupNextPageServ.js';
import EmailProviders from '../../../utils/EmailProviders.js';

export class SignupNextGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        const domain = ctx.query.provider;
        const btnLink = EmailProviders.getLinkByDomain(domain);

        return this.render(new SignupNextPageServ(g, ctx, { btnLink }));
    }
}