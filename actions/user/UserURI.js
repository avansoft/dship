import { BaseURI } from "../../core/lib/BaseURI.js";

export class UserURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get profilePage() {
        return '/profile';
    }
}
