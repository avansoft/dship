import { BaseController } from '../../core/lib/BaseController.js';
import { UserURI as urls } from './UserURI.js'
import { ProfileGET } from './profile/ProfileGET.js';

import express from 'express';
const router = express.Router();

export class UserController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        this.logger.debug(`${this.constructor.name} -> Get routes`);

        // Get
        router.get(urls.profilePage, this.actionRunner(ProfileGET));

        return router;
    }
}