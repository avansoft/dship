import { BaseAction } from '../../../core/lib/BaseAction.js';
import { ProfilePageServ } from '../../../src/pages/ProfilePage/ProfilePageServ.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';

export class ProfileGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        this.checkAccess(g, ctx, 'user');
        const userKey = ctx.session.userKey;

        logger.step('Get profile info from DB');
        const subject = new SubjectDAO(g);
        const profile = await subject.getSubjectInfo(userKey);

        return this.render(new ProfilePageServ(g, ctx, { profile }));
    }
}
