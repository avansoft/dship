import { BaseController } from "../../core/lib/BaseController.js";
import { UserSessionURI as urls } from './UserSessionURI.js';
import { LoginGET } from "./login/LoginGET.js";
import { LoginPOST } from "./login/LoginPOST.js";
import { LogoutPOST } from "./logout/LogoutPOST.js";
import express from 'express';

const router = express.Router();

export class UserSessionController extends BaseController {
    constructor(g) {
        super(g);
        this.logger.debug(`${this.constructor.name} -> Initilazed`);
    }
    get routes() {
        // GET
        router.get(urls.loginPage, this.actionRunner(LoginGET));

        // POST
        router.post(urls.loginAction, this.actionRunner(LoginPOST));
        router.post(urls.logoutAction, this.actionRunner(LogoutPOST));

        return router;
    }
}