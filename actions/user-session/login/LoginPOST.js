import { BaseAction } from '../../../core/lib/BaseAction.js';
import { LoginForm } from '../../../subs/forms/LoginForm.js';
import { PasswordUtility } from '../../../utils/PasswordUtility.js';
import { UserSessionCookie } from '../../../subs/cookies/UserSessionCookie.js';
import { UserSessionDAO } from '../../../dao/UserSessionDAO.js';
import { UserSessionURI } from '../UserSessionURI.js';
import { UserURI } from '../../user/UserURI.js';
import { SubjectDAO } from '../../../dao/SubjectDAO.js';
import DateUtility from '../../../utils/DateUtility.js';

export class LoginPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Check user data');
        const form = new LoginForm(g, ctx);
        const subjectDao = new SubjectDAO(g);

        logger.step('Get salt from DB');
        const salt = await subjectDao.getPasswordSaltByContacts({
            email: form.fields.email,
            mobilePhone: form.fields.mobilePhone,
        });

        if (!salt) {
            logger.trace('Break, because salt not found by user contacts');
            form.raiseError('userNotFound');
        }

        logger.step('Make hash with salt');
        logger.trace(`Salt: ${salt}`);
        const passwordUtility = new PasswordUtility(g);
        const hash = await passwordUtility.getHashBySalt(form.fields.password, salt);

        logger.step('Get userKey by hash');
        logger.trace(`Hash: [${hash}]`);
        const subjectId = await subjectDao.getSubjectByPasswordHash(hash);

        if (!subjectId) {
            logger.trace(`Break, because user with hash [${hash} not found]`);
            form.raiseError('userNotFound');
        }

        logger.step('Create and save session in cache');
        const sid = await new UserSessionDAO(g).create(subjectId);

        const expires = DateUtility.tomorrow(3);
        const isRemember = form.fields.isRemember;

        logger.step('Make session cookies');
        const setCookies = [
            new UserSessionCookie(g, {
                value: sid,
                expires: isRemember ? expires : 0,
            }),
        ];

        logger.step('Get forward path and forwarding');
        // referer содержит URL страницы, с которой перешел пользователь
        const referer = new URL(ctx.referer);
        const forwardPath = referer.pathname === UserSessionURI.loginPage
            ? UserURI.profilePage
            : referer.pathname;
        return this.forward(forwardPath, { setCookies });
    }
}