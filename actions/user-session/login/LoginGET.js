import { BaseAction } from '../../../core/lib/BaseAction.js';
import { LoginPageServ } from '../../../src/pages/LoginPage/LoginPageServ.js';
import { ProfilePageServ } from '../../../src/pages/ProfilePage/ProfilePageServ.js';

export class LoginGET extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Check session');
        if (ctx.session) {
            logger.trace('Forward because session exists');
            return this.render(new ProfilePageServ(g, ctx));
        }

        return this.render(new LoginPageServ(g, ctx));
    }
}