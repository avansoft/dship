import { BaseAction } from '../../../core/lib/BaseAction.js';
import { UserSessionCookie } from '../../../subs/cookies/UserSessionCookie.js';
import { UserSessionDAO } from '../../../dao/UserSessionDAO.js';
import { SiteURI } from '../../site/SiteURI.js';

export class LogoutPOST extends BaseAction {
    static async run({ g, ctx } = {}) {
        const logger = g.logger;
        logger.action(this.name);

        logger.step('Check session');
        if (!ctx.session) {
            logger.trace('Forward, because session not found');
            return this.forward(SiteURI.index);
        }

        logger.step('Remove session');
        const sid = await new UserSessionDAO(g).remove(ctx.session.sid);

        logger.step('Make cookies for remove');
        const removeCookies = [
            new UserSessionCookie(g, { value: sid }),
        ]

        return this.forward(SiteURI.index, { removeCookies });
    }
}