import { BaseURI } from "../../core/lib/BaseURI.js";

export class UserSessionURI extends BaseURI {
    constructor(g) {
        super(g);
    }

    static get loginPage() {
        return '/login';
    }

    static get loginAction() {
        return '/login';
    }

    static get logoutAction() {
        return '/logout';
    }
}
