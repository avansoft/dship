import { Server } from './core/index.js';
import { GlobalContext } from './core/lib/GlobalContext.js';

import { RedisDriver } from './drivers/RedisDriver.js';
import { PostgresDriver } from './drivers/PostgresDriver.js';

import { MailerService } from './services/mailer/MailerService.js';
import { LoggerService } from './services/logger/LoggerService.js';
import { UploaderService } from './services/uploader/UploaderService.js';

import siteConfig from './_configs/siteConfig.js';

async function appsConstructor() {
	const logger = new LoggerService();
	const mailer = new MailerService(siteConfig.services.mailer);
	const uploader = new UploaderService(siteConfig.services.uploader);
	const cache = new RedisDriver(siteConfig.drivers.cashDatabase);
	const store = new PostgresDriver(siteConfig.drivers.storeDatabase);

	/**
     * Ready tests
     */
	await store.test();
	await uploader.test();
	await cache.test();

	return {
		logger,
		mailer,
		cache,
		store,
		uploader,
	};
}

appsConstructor().then(apps => {
	const g = Object.freeze(new GlobalContext(apps));

	const server = new Server(g);

	server.start(g.config)
		.then(serverParams => {
			g.logger.info(`Server has been started on the port ${serverParams.port}`);
			g.logger.debug(`viewEngine: ${serverParams.viewEngine}`);
			g.logger.debug(`viewsFolder: ${serverParams.viewsFolder}`);
		})
		.catch(err => console.dir(err));

	process
		.once('SIGINT', () => server.shutdown('SIGINT'))
		.once('SIGTERM', () => server.shutdown('SIGTERM'));
}).catch(err => console.dir(err));
