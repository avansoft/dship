export class StateManager {
    constructor() {
        this.metaTagsObj = {};

        document.querySelectorAll('meta').forEach(v => {
            const nameAttribute = v.getAttribute('name');
            const regex = new RegExp('^app:');

            if (regex.test(nameAttribute)) {
                this.metaTagsObj[nameAttribute.slice(4)] = v.getAttribute('content');
            }
        });
    }

    getValue(key) {
        return this.metaTagsObj[key];
    }
}