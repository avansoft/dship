import { AbstractInput } from '../core/AbstractInput.js';
import Client from '../Client.js';

export class CascadeSelectControl extends AbstractInput {
    constructor(el, props) {
        super(el);
        if (!props.apiUri) throw new Error('Отсутствует ссылка для загрузчика');

        // props
        this.apiUri = props.apiUri;
        this.subSelectDefaultText = props.subSelectDefaultText || '-- Уточните тему --';

        // private fields
        this.items = [];
        this.activeItem = undefined;
        this.selects = [];
        this.currentLevel = 1;

        this.selectsWrapper = this.el.querySelector('.__selectsWrapper');
        if (!this.selectsWrapper) throw new Error('selectWrapper didn\'t found');
    }

    init() {
        // Init root select
        const rootSelect = this.selectsWrapper.querySelector('[data-level="1"]');
        if (!rootSelect) throw new Error('Не найден родительский селект');

        const button = rootSelect.querySelector('button');
        if (!button) throw new Error('Не найдена кнопка раскрытия корневого селекта');
        button.addEventListener('click', () => this._showItems(rootSelect));

        this.selects.push(rootSelect);

        // api request
        const url = Client.getEndpoint(this.apiUri);
        const response = Client.send({}, url);
        response.then(items => {
            if (!items?.length) throw new Error('Сервер не предоставил данные');

            this.items = items;

            const rootSelectItems = items.filter(v => !v.t_parent_id);
            if (!rootSelectItems.length) throw new Error('Not found root items');

            this._createItems(rootSelectItems);
        }).catch(e => {
            console.log('CascadeSelectControl', e.message);
        })

        return this;
    }

    _createItems(items) {
        // Items list always creates for select with max level
        const select = this.selects[this.currentLevel - 1];

        const itemsWrapper = this.createElement({
            type: 'ul',
            classes: ['__items']
        });
        select.appendChild(itemsWrapper);
        itemsWrapper.addEventListener('click', (ev) => this._tapItem(ev.target));

        items.forEach(v => {
            const itemEl = this.createElement({
                type: 'li',
                classes: ['__item'],
                textContent: v.t_title,

            })
            itemEl.dataset.id = v.t_id;
            itemEl.dataset.itemLevel = this.currentLevel;

            itemsWrapper.appendChild(itemEl);
        })

    }

    _tapItem(el) {
        const itemLevel = el.dataset.itemLevel;
        if (!itemLevel) return;

        const content = el.textContent;

        // Get current select and hide items block
        const select = this.selectsWrapper.querySelector(`[data-level="${itemLevel}"]`);
        select.classList.remove('js-showItems');

        // Set active item for current select
        select.dataset.activeItem = el.dataset.id;

        // Change button text
        const button = select.querySelector('button');
        button.textContent = content;

        if (this.activeItem) this.activeItem.classList.remove('js-active');

        this.activeItem = el;

        // Set active item
        el.classList.add('js-active');

        // Load child for theme id
        const subSelectItems = this.items.filter(v => v.t_parent_id == el.dataset.id);

        // If chils items exists, create sub select
        if (subSelectItems.length) {
            this._createSelect();
            this._createItems(subSelectItems);
        }
        console.log('active item-->', this.activeItem.dataset.id);
    }

    _createSelect() {
        /**
         * При создании нового селекта активный item сбрасывается, т.к. предполагается,
         * что пользователь должен выбрать уточняющее значение
         */
        if (this.activeItem) this.activeItem.classList.remove('js-active');
        this.activeItem = undefined;

        const level = this.selects.length + 1;

        // Make select wrapper
        const select = document.createElement('div');
        select.classList.add('__select');
        this.selectsWrapper.insertAdjacentElement('beforeend', select);
        select.dataset.level = level;

        // Make button
        const button = document.createElement('button');
        button.setAttribute('type', 'button');
        button.textContent = this.subSelectDefaultText;
        select.appendChild(button);
        button.addEventListener('click', () => this._showItems(select));

        this.selects.push(select);

        this.currentLevel = level;
    }

    _resetActiveItem() {
        if (this.activeItem) this.activeItem.classList.remove('js-active');
        this.activeItem = undefined;
    }

    _showItems(selectEl) {
        const level = selectEl.dataset.level;

        // Если клик сделан на родительском селекте
        if (this.currentLevel > level) {

            // Делаем активным item, которая была у выбранного селекта
            const activeItemId = selectEl.dataset.activeItem;
            this.activeItem = selectEl.querySelector(`[data-id="${activeItemId}"]`);
            this.activeItem.classList.add('js-active');

            // Удаляем дочерние селекты
            this.selects.forEach((v, i) => {
                if (i + 1 > level) {
                    v.remove();
                }
            });

            // Приводим массив селектов в соответствие с интерфейсом
            this.selects = this.selects.slice(0, level);
        }

        const selector = 'js-showItems';

        selectEl.classList.contains(selector)
            ? selectEl.classList.remove(selector)
            : selectEl.classList.add(selector);

        this.currentLevel = level;
    }

    get value() {
        return this.activeItem ? this.activeItem.dataset.id : '';
    }
}