import { AbstractInput } from '../core/AbstractInput.js';

export class TextboxControl extends AbstractInput {
    constructor(el) {
        super(el);
        this.input = this.el.querySelector('input');
        this.errorMessageEl;
    }

    get value() {
        return this.input.value;
    }

    stream(cb) {
        this.inputElement.addEventListener('input', () => cb(this.input.value));
    }

    setValue(value) {
        this.input.value = value;
    }

    setFieldValue(value) {
        this.input = value;
    }

    removeFieldValue() {
        this.input = '';
    }
}