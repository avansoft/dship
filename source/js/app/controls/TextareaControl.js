import { AbstractInput } from "../core/AbstractInput.js";

export class TextareaControl extends AbstractInput {
    constructor(el) {
        super(el);

        this.input = this.el.querySelector('textarea');
        if (!this.input) throw new Error(`${this.constructor.name} -> Not found textarea element`);
    }

    get value() {
        return this.input.value;
    }

    set value(v) {
        this.input.value = v;
    }

    reset() {
        this.value = '';
    }
}