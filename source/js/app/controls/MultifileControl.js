import { AbstractInput } from "../core/AbstractInput.js";
import createElement from "../helpers/createElement.js";
import byteConverter from "../helpers/byteConverter.js";

export class MultifileControl extends AbstractInput {
    constructor(el, opts) {
        super(el);

        this.wrapper = this.el.querySelector('.__wrapper');
        this.inputEl = this.el.querySelector('input[type=file]');
        this.uploadEl = this.el.querySelector('.__upload');
        this.labelEl = this.el.querySelector('label');

        this.maxFiles = opts?.maxFiles || 5;
        this.maxSize = opts?.maxSize || 1024 * 1024 * 2;

        this._files = new Map();
        this._teasers = new Map();

        this.isError = false;

        // Create teasers gallery
        this.galleryEl = createElement('div', ['__gallery']);
        this.wrapper.appendChild(this.galleryEl);
        this.galleryEl.addEventListener('click', event => {
            this._galleryClickHandler(event)
        });
    }

    init() {
        // Listener for gallery
        this.inputEl.onchange = (event) => this.addFile(event);
        return this;
    }

    _getErrorMessage(type) {
        const dict = {
            tooManyFiles: {
                ru: `Можно добавить не более ${this.maxFiles} файлов`,
                en: `You can attach no more than ${this.maxFiles} files`,
            },
            tooBigSize: {
                ru: `Размер файлов не должен превышать ${byteConverter(this.maxSize)}`,
                en: `The total file size should not exceed ${byteConverter(this.maxSize)}`,
            },
        }

        return dict[type][this.lang] || 'Unknown error';
    }

    _galleryClickHandler(event) {
        const data = event.target.dataset;

        if (data.id) {
            event.target.closest('div.__teaser').remove();
            this._teasers.delete(data.id);
            this._files.delete(data.id);

            this._check();
        }

        return;
    }

    _check() {
        this.removeError();
        this._enableUploadEl();
        this.isError = false;

        // Check max size
        const totalSize = this._totalSize(this.value);
        if (totalSize > this.maxSize) {
            this.setError(this._getErrorMessage('tooBigSize'));
            this._disableUploadEl();
            this.isError = true;
            return;
        }

        // Check max files count
        const totalFiles = this.value.length;
        if (totalFiles > this.maxFiles) {
            this.setError(this._getErrorMessage('tooManyFiles'));
            this._disableUploadEl();
            this.isError = true;
            return;
        }
    }

    _enableUploadEl() {
        this.inputEl.removeAttribute('disabled', 'disabled');
        this.labelEl.classList.remove('js-disabled');
    }

    _disableUploadEl() {
        this.inputEl.setAttribute('disabled', 'disabled');
        this.labelEl.classList.add('js-disabled');
    }

    addFile(event) {
        // Convert to array
        const fileList = Array.from(event.target.files);

        fileList.forEach(file => this.addTeaser(file));
    }

    _totalSize(fileList) {
        return fileList.reduce(function (acc, file) {
            return acc + file.size;
        }, 0);
    }

    addTeaser(file) {
        // Create teaser
        const teaser = document.createElement('div');
        teaser.classList.add('__teaser');
        this.galleryEl.appendChild(teaser);

        // Reading file
        const reader = new FileReader();
        reader.readAsDataURL(file);

        // Fill teaser after read
        reader.onloadend = () => {
            teaser.insertAdjacentHTML('afterbegin', `
                <img src="${reader.result}">
                <div>
                    <span class="__fileName">${file.name}</span>
                    <span class="__fileSize">${byteConverter(file.size)}</span>
                </div>
            `);

            // Create remove btn
            const removeEl = createElement('div', ['__remove']);
            removeEl.dataset.id = file.name;
            teaser.appendChild(removeEl);

            this._files.set(file.name, file);
            this._teasers.set(file.name, teaser);

            this._check();
        }
    }

    get value() {
        return Array.from(this._files, ([key, value]) => { return value });
    }
}