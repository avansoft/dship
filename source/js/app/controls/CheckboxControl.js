import { AbstractInput } from '../../../../source/js/app/core/AbstractInput.js';

export class CheckboxControl extends AbstractInput {
    constructor(el) {
        super(el);
        this.input = this.el.querySelector('input');
    }

    get value() {
        return this.input.checked;
    }
}