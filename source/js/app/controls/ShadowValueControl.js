export class ShadowValueControl {
    constructor(value) {
        this._value = value;
    }

    get value() {
        return this._value;
    }
}