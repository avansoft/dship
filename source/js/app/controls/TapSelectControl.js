import { AbstractInput } from "../core/AbstractInput.js";

export class TapSelectControl extends AbstractInput {
    constructor(el) {
        super(el);

        this.button = this.el.querySelector('button');
        if (!this.button) throw new Error('Not found button-tag');

        this.itemsBlock = this.el.querySelector('ul');
        if (!this.itemsBlock) throw new Error('Empty options block (query "ul")');

        this.items = this.itemsBlock.getElementsByTagName('li');
        if (!this?.items?.length) throw new Error('Empty options list (query "li")');

        this.currentActiveEl = undefined;
    }

    _tapItem(el) {
        const content = el.textContent;

        // Change button text
        this.button.textContent = content;


        // Hide options block
        this.el.classList.remove('js-showItems');


        // Set active item
        el.classList.add('js-active');


        if (this.currentActiveEl) this.currentActiveEl.classList.remove('js-active');


        this.currentActiveEl = el;
    }

    _showItems() {
        const selector = 'js-showItems';

        this.el.classList.contains(selector)
            ? this.el.classList.remove(selector)
            : this.el.classList.add(selector);
    }

    get value() {
        return this.currentActiveEl ? this.currentActiveEl.getAttribute('name') : '';
    }

    init() {
        Array.prototype.forEach.call(this.items, el => {
            el.addEventListener('click', (event) => this._tapItem(event.target));

            // If 'data-is-default', make click imitation
            if (el.dataset.isDefault === '') this._tapItem(el);
        })

        this.button.addEventListener('click', () => this._showItems());

        return this;
    }
}