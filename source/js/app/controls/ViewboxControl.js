export class ViewboxControl {
    constructor(el) {
        if (!el) throw new Error('El is required');

        this.el = el;

        this.input = this.el.querySelector('input');
        if (!this.input) throw new Error('Input not found');

        this._value = this.input.value;
    }
    get value() {
        return this.input.value;
    }

    format() {
        const type = this.input.getAttribute('type');

        if (type === 'tel') this.input.value = this.phoneFormatter(this._value);

        return this;
    }

    edit() {
        const el = this.el.querySelector('.__edit');
    }

    phoneFormatter(phone) {
        if (!phone) return;

        const len = phone.length;
        let f = phone.split('');
        if (len == 11) {
            f.splice(1, '', '(');
            f.splice(5, '', ')');
            f.splice(9, '', '-');
            f.splice(12, '', '-');
        } else if (len == 12) {
            f.splice(2, '', '(');
            f.splice(6, '', ')');
            f.splice(10, '', '-');
            f.splice(13, '', '-');
        }
        return '+' + f.join('');
    }
}