import { TextboxControl } from '../controls/TextboxControl.js';
import { PasswordControl } from '../controls/PasswordControl.js';
import { PasswordChecker } from './PasswordChecker.js';
import { CheckboxControl } from '../controls/CheckboxControl.js';
import { ViewboxControl } from '../controls/ViewboxControl.js';
import { TextareaControl } from '../controls/TextareaControl.js';
import { TapSelectControl } from '../controls/TapSelectControl.js';
import { MultifileControl } from '../controls/MultifileControl.js';
import { CascadeSelectControl } from '../controls/CascadeSelectControl.js';
import { ShadowValueControl } from '../controls/ShadowValueControl.js';

import { ButtonAsync } from '../../../../components/_shared/ButtonAsync/ButtonAsync.js';

import Client from '../Client.js';

export class Form extends EventTarget {
    constructor(el, props) {
        super();
        this.el = el;

        // props
        this.formSentEventName = props.formSentEventName || 'formSent';
        this.apiUri = props.apiUri;
        this.propsFields = props.fields;

        // privates
        this.fields = [];
        this.wrongFields = [];
        this.topErrorClassName = 'js-topError';
        this.topErrorEl = null;
    }

    init() {
        const fields = this.propsFields;

        if (!Object.keys(fields).length) throw new Error('Not found fields in props');

        // init fields component
        for (const key in fields) {
            const elType = fields[key].type;
            if (!elType) throw new Error(`Absent type for field [${key}]`);

            // init shadow control
            if (elType === 'shadow') {
                const initValue = fields[key].initValue || '';
                this.addShadowValue(key, initValue)
                continue;
            }

            // init visible contols
            const selector = fields[key].selector;
            if (!selector) throw new Error(`Absent selector for field [${key}]`);

            const options = fields[key].props || {};

            if (elType === 'textbox') { this.addTextboxField(selector, key); continue }
            if (elType === 'passbox') { this.addPasswordField(selector, key); continue }
            if (elType === 'checkbox') { this.addCheckbox(selector, key); continue }
            if (elType === 'viewbox') { this.addViewbox(selector, key); continue }
            if (elType === 'textarea') { this.addTextarea(selector, key); continue }
            if (elType === 'tapSelect') { this.addTapSelect(selector, key); continue }
            if (elType === 'cascadeSelect') { this.addCascadeSelect(selector, key, options); continue }
            if (elType === 'multifile') { this.addMultifile(selector, key); continue }

            throw new Error(`Unhandled type [${elType}] for field [${key}]`);
        }

        this.addSendButton('.js-btnSend');
    }

    _addField(fieldName, inst) {
        this.fields.push({
            fieldName,
            inst,
        });
    }

    getFieldInstance(fieldName) {
        return this.fields.find(v => v.fieldName === fieldName).inst;
    }

    async sendForm() {
        // TODO Проверка, что у всех полей isValid = true (предварительная валидация);

        // get values from fields
        const formData = await this.getFormData();

        // api request
        const url = Client.getEndpoint(this.apiUri)
        return Client.send(formData, url);
    }

    responseHandler(data) {
        // clear errors
        this.removeTopError();
        this.removeFieldErrors();

        const errors = data.formErrors;
        if (errors) {
            // set errors
            if (errors.topError) this.setTopError(data.formErrors.topError);
            if (errors.fieldErrors) this.setFieldErrors(data.formErrors.fieldErrors);
        } else {
            // sent event
            this.dispatchEvent(new CustomEvent(this.formSentEventName, {
                detail: data,
            }));
        }
    }

    async getFormData() {
        const formData = new FormData();

        this.fields.forEach(v => {
            if (v.inst instanceof Promise) {
                v.inst.then(inst => {
                    formData.set(v.fieldName, inst.value);
                });
            } else if (v.inst instanceof MultifileControl) {
                v.inst.value.forEach(file => {
                    formData.append(v.fieldName, file, file.name)
                })
            } else {
                formData.set(v.fieldName, v.inst.value);
            }
        })

        return formData;
    }

    setTopError(message) {
        if (this.topErrorEl) {
            this.topErrorEl.textContent = message;
        } else {
            this.el.insertAdjacentHTML('afterbegin',
                `<p class="${this.topErrorClassName}">${message}</p>`
            );

            this.topErrorEl = this.el.querySelector(`.${this.topErrorClassName}`);
        }
    }

    removeTopError() {
        this.topErrorEl ? (this.topErrorEl.remove(), this.topErrorEl = null)
            : this.topErrorEl = null;
    }

    setFieldErrors(fieldErrors) {
        this.removeFieldErrors();

        for (let fieldName in fieldErrors) {
            // get input instance
            const inst = this.fields.filter(v => v.fieldName === fieldName)[0].inst;
            const errorMessage = fieldErrors[fieldName];

            // make top-error for hidden-fields
            if (inst instanceof ShadowValueControl) {
                this.setTopError(errorMessage);
                continue;
            }

            // all inputs have same error handle interface
            inst.setError(errorMessage);

            // filling error collection
            this.wrongFields.push({
                fieldName,
                inst,
            });
        }
    }

    removeFieldErrors() {
        if (this.wrongFields) {
            this.wrongFields.forEach((v, i, arr) => {
                // all inputs have same error handle interface
                v.inst.removeError();
            });

            // reset error collection
            this.wrongFields = [];
        }
    }

    addShadowValue(fieldName, value) {
        this._addField(fieldName, new ShadowValueControl(value));
    }

    addCascadeSelect(selector, fieldName, options) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new CascadeSelectControl(el, options).init());
    }

    addMultifile(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new MultifileControl(el).init());
    }

    addTapSelect(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new TapSelectControl(el).init());
    }

    addTextarea(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new TextareaControl(el));
    }

    addViewbox(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new ViewboxControl(el));
    }

    addCheckbox(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new CheckboxControl(el));
    }

    addTextboxField(selector, fieldName) {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new TextboxControl(el));
    }

    addPasswordField(selector, fieldName = 'password') {
        const el = this.el.querySelector(selector);
        this._addField(fieldName, new PasswordControl(el));
    }

    // TODO move to component
    addPasswordChecker(selector) {
        const el = this.el.querySelector(selector);
        const passwordChecker = new PasswordChecker(el);

        // pure predicate functions for validation
        const regExpValidator = (value, regExp) => regExp.test(value);
        const minLengthValidator = (value, minLen) => value.length >= minLen;
        const passwordControl = this.fields.filter(v => v.inst instanceof PasswordControl)[0].inst;

        passwordChecker.addValidator(minLengthValidator, 8, '.js-minLengthChecker');
        passwordChecker.addValidator(regExpValidator, /[A-Z]+/, '.js-upperCaseChecker');
        passwordChecker.addValidator(regExpValidator, /\d+/, '.js-digitChecker');
        passwordChecker.addValidator(regExpValidator, /[-_\\/\.*+()$?|]+/, '.js-specialSymbolChecker');

        passwordChecker.watch(passwordControl);
    }

    addSendButton(selector) {
        const el = this.el.querySelector(selector);
        if (!el) throw new Error(`${this.constructor.name} -> not found btnSend element with selector [${selector}]`);

        const btnSend = new ButtonAsync({
            el,
            command: () => this.sendForm(),
            successEventName: this.formSentEventName,
        });

        btnSend.addEventListener(this.formSentEventName, ev => this.responseHandler(ev.detail));
    }
}
