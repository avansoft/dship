import { Form } from './Form.js';

export class ChatThread {
    constructor(el, props) {
        this.issueMessageCreateUri = props.issueMessageCreateUri;

        this.messages = new Map();

        this.formEl = this.el.querySelector('js-form-createMessage');
    }

    init() {
        // grab messages
        this.el.querySelectorAll('.js-messageItem').forEach(el => {
            this.messages.set(el.dataset.id, el);
        });

        // form init
        if (this.formEl) {
            const formSentEvent = 'chatThreadFormSent';

            const form = new Form(this.formEl, {
                apiUri: this.issueMessageCreateUri,
                formSentEvent,
                fields: {
                    issueId: {
                        type: 'shadow',
                        initValue: StateManager.getValue('issueId'),
                    },
                    text: {
                        type: 'textarea',
                        selector: '.js-textarea-message',
                    },
                },
            });

            form.init();

            // Catching success sent event
            form.addEventListener(formSentEvent, ev => this.formSentHandler(ev));
        }
    }

    formSentHandler(ev) {
        const { messageId, content } = ev.detail;
        if (!content) return;

        const formEl = this.el.querySelector('.js-form-createMessage');

        // build messageList
        let wrapperEl = this.el.querySelector('.js-messageList');

        if (!wrapperEl) {
            wrapperEl = document.createElement('div');
            wrapperEl.classList.add('js-messageList');
            formEl.before(wrapperEl);
        }

        // build messageItem
        const messageEl = document.createElement('div');
        messageEl.classList.add('__messageContainer', this.messageItemSelector);
        messageEl.dataset.id = messageId;
        wrapperEl.appendChild(messageEl);

        // build info for messageItem
        const infoEl = document.createElement('div');
        infoEl.classList.add('__messageInfo');
        messageEl.appendChild(infoEl);

        // build text for messageItem
        const textEl = document.createElement('p');
        textEl.classList.add('__messageText');
        textEl.textContent = content;
        messageEl.appendChild(textEl);

        // push messageItem to message collection
        this.messages.set(messageId, messageEl);

        // clear form
        const textarea = this.form.getFieldInstance('text');
        textarea.reset();
    }
}