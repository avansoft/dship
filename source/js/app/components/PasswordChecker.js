export class PasswordChecker {
    constructor(el) {
        this.el = el;
        this.validators = [];
        this.isPasswordValid = false;
    }

    _validate(value) {
        this.el.hidden = this.isPasswordValid = this.validators.reduce((acc, obj) => {
            obj.el.hidden = obj.validator(value, obj.param);
            return acc && obj.el.hidden;
        }, true);
    }

    addValidator(validator, param, selector) {
        this.validators.push({
            validator,
            param,
            el: this.el.querySelector(selector),
        });
    };

    watch(passwordControlInstance) {
        const input = passwordControlInstance.input;
        input.addEventListener('input', (event) => {
            this._validate(event.target.value);
        });
    }
}
