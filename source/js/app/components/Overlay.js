export class Overlay {
    constructor(el) {
    }

    hide() {
        this.el.classList.remove('js-overlay-show');
    }

    show() {
        this.el.classList.add('js-overlay-show');
    }
}