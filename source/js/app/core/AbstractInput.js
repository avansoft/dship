export class AbstractInput {
    constructor(el) {
        if (!el) throw new Error(`${this.constructor.name} -> El is required`);
        this.el = el;

        this.errorStyleClassName = 'js-fieldError';

        // Error message
        this.errorMessageEl = undefined;

        // Get page language
        this.lang = document.querySelector('html').getAttribute('lang') || 'ru';
    }

    get value() {
        throw new Error('Контрол должен иметь метод getValue()');
    }

    onChange(cb) {
        // Сработает при изменении элемента
    }

    setError(message) {
        const errorMessageEl = document.createElement('p');
        errorMessageEl.classList.add('js-error');
        errorMessageEl.textContent = message;
        this.el.appendChild(errorMessageEl);
        this.errorMessageEl = errorMessageEl;

        this.el.classList.add(this.errorStyleClassName);
    }

    removeError() {
        if (this.errorMessageEl) {
            this.el.classList.remove(this.errorStyleClassName);
            this.errorMessageEl.remove();
        }
    }
}