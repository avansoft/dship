export default function (tag, classes = [], textContent) {
    const el = document.createElement(tag);
    el.classList.add(...classes);

    if (textContent) el.textContent = textContent;

    return el;
}