import { Alert } from "../app/components/Alert.js";

export default class Client {
    static defaultApiGateway = 'http://localhost';

    static getEndpoint(uri) {
        return this.defaultApiGateway + uri;
    }

    static send(obj, url, timeout = 5000) {
        /**
         * В тело запроса может передаваться экземпляр объекта FormData. Такой запрос поступит
         * на сервер в формате multipart/form-data, который парсится, например, через Multer
         */
        const body = obj instanceof FormData ? obj : JSON.stringify(obj);

        const rs = Promise.race([
            fetch(url, {
                method: 'POST',
                body,
            }),
            new Promise((resolve, reject) => {
                setTimeout(() => reject(new Error('Время запроса превысило таймаут')), timeout);
            })
        ])

        return this._handler(rs);
    }

    static async _handler(rs) {
        try {
            const result = await rs;

            const data = await result.json();

            if (!Object.keys(data).length) {
                throw new Error('Empty server response');
            }

            if (data.nextPage) return window.location.assign(data.nextPage);

            if (data.error) {
                const alertContainer = new Alert();
                alertContainer.push(data.error.message);
                return data.error;
            }

            if (data.alert) {
                const alertContainer = new Alert();
                return alertContainer.push(data.alert.message);
            }

            return data;

        } catch (e) {
            // TODO добавить errors/TimeoutError.js
            console.error('POST request error: ', e.message);
        }
    }
}