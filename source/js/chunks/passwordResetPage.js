const props = {
    apiUri: '/password/reset',
    fields: {
        email: {
            selector: '.js-textbox-email',
            type: 'textbox',
            require: true,
        }
    },
}

const formEl = document.querySelector('.js-form-passResetByEmail');
const form = new Form(formEl, props);
form.init();