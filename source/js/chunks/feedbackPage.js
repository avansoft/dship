const props = {
    apiUri: '/feedback',
    fields: {
        firstName: {
            selector: '.js-textbox-firstName',
            type: 'textbox',
        },
        email: {
            selector: '.js-textbox-email',
            type: 'textbox',
        },
        themeId: {
            selector: '.js-tapSelect-theme',
            type: 'tapSelect',
        },
        message: {
            selector: '.js-textarea-message',
            type: 'textarea',
        },
    },
}

const formEl = document.querySelector('.js-form-feedback');
const form = new Form(formEl, props);
form.init();