// Form
const props = {
    apiUri: '/login',
    fields: {
        login: {
            selector: '.js-textbox-login',
            type: 'textbox',
        },
        password: {
            selector: '.js-passbox-password',
            type: 'passbox',
        },
        isRemember: {
            selector: '.js-checkbox-isRemember',
            type: 'checkbox',
        },
    },
}

const formEl = document.querySelector('.js-form-login');
const form = new Form(formEl, props);
form.init();