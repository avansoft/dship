const props = {
    apiUri: '/signup',
    fields: {
        firstName: {
            selector: '.js-textbox-firstName',
            type: 'textbox',
        },
        email: {
            selector: '.js-textbox-email',
            type: 'textbox',
        },
        mobilePhone: {
            selector: '.js-textbox-mobilePhone',
            type: 'textbox',
        },
    },
}

const formEl = document.querySelector('.js-form-signup');
const form = new Form(formEl, props);
form.init();
