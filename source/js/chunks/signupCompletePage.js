// Form
const props = {
    apiUri: '/signup/complete',
    fields: {
        password: {
            selector: '.js-passbox-password',
            type: 'passbox',
        }
    },
}

const formEl = document.querySelector('.js-form-passwordSet');
const form = new Form(formEl, props);
form.init();

// PasswordCheckerControl
form.addPasswordChecker('.js-passwordChecker');
