import headerLoginBlock from "./blocks/headerLoginBlock.js";
import { ViewboxControl } from "../app/controls/ViewboxControl.js";

headerLoginBlock();

const phoneEl = document.querySelector('.js-viewbox-mobilePhone');

const fields = [
    new ViewboxControl(phoneEl).format(),
]
