const props = {
    apiUri: '/issue-create',
    fields: {
        themeId: {
            selector: '.js-cascadeSelect-theme',
            type: 'cascadeSelect',
            props: {
                apiUri: '/issue-themes',
            }
        },
        summary: {
            selector: '.js-textbox-summary',
            type: 'textbox',
        },
        message: {
            selector: '.js-textarea-message',
            type: 'textarea',
        },
        files: {
            selector: '.js-multifile-files',
            type: 'multifile',
        },
    },
}

const formEl = document.querySelector('.js-form-issue');
const form = new Form(formEl, props);
form.init();