import { Overlay } from '../../app/components/Overlay.js';
import { ButtonAsync } from '../../../../components/_shared/ButtonAsync/ButtonAsync.js';
import Client from '../../app/Client.js';

export default function () {
    //Header
    const overlayEl = document.querySelector('.js-overlay');
    const overlay = new Overlay(overlayEl);

    document.querySelector('.js-btn-logout').addEventListener('click', () => overlay.show());
    document.querySelector('.js-no').addEventListener('click', () => overlay.hide());

    //Button
    const btnLogoutEl = document.querySelector('.js-logout');

    new ButtonAsync({
        el: btnLogoutEl,
        command: () => {
            const url = Client.getEndpoint('/logout');
            Client.send({ action: 'logout' }, url);
        },
    });
}
