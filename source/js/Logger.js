export default class Logger {
    constructor(module, mode) {
        this.mode = mode;
        this.module = module;

        console.log(`Logger started for module '${this.module}'`);
    }

    info(descr) {
        console.log(descr);
    }

    error(obj) {
        console.error(obj);
    }

    debug(note, obj) {
        if (this.mode !== 'debug') {
            if (note) console.log(note);
            if (obj) console.log(obj);
        }
    }

    trace(descr) {
        console.log(descr);
    }
}
