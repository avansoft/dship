/* eslint-disable no-undef */
module.exports = {
	// Настройки парсера
	parserOptions: {
		// Декларация ECMAScript модулей (для commonJS использовать script)
		sourceType: 'module',
		ecmaVersion: 2021,
	},
	extends: [
		// Иначе не будут действовать правила, отмеченные флагами в rules
		'eslint:recommended',
		/**
         * Тип модульной системы определяется по package.json/type, но можно указать явно:
         * plugin:node/recommended-module. Причем .cjs-файлы обрабатываются как
         * CommonJS
         */
		'plugin:node/recommended'
	],
	env: {
		browser: true,
		node: true,
	},
	/**
     * Файлы/директории, которые должны быть исключены из проверки. В том же формате,
     * который используется в gitignore. В качестве альтернативы, можно создать файл
     * .eslintignore
     */
	ignorePatterns: ['node_modules/'],
	rules: {
		// Точки с запятой обязательны
		semi: ['error', 'always'],
		// Разрешены одинарные кавычки вместо двойных (double)
		quotes: ['error', 'single'],
		/**
		 * Указывает, что отступ должен осуществляться табуляцией, а не пробелами.
		 * Если отступ нужно указать в кол-ве пробелов, запись будет такой:
		 * "indent": ["error", 2]
		 * https://eslint.org/docs/rules/indent
		 * 
		 * SwitchCase добавляет отступ в 1 tab ко вложенным case.
		 */
		indent: ['error', 'tab', {'SwitchCase': 1}],
	}
};