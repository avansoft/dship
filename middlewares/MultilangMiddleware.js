import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';

export class MultilangMiddleware extends BaseMiddleware {
	constructor(g) {
		super(g);
	}

	handler() {
		return (req, res, next) => {
			this.logger.debug('MultilangMiddleware -> handler()');

			const langFromCookie = req.cookies.lang;

			this.config.langs.includes(langFromCookie)
				? (req.lang = langFromCookie, this.logger.trace(`Set lang [${req.lang}] from cookies`))
				: (req.lang = this.config.defaultLang, this.logger.trace(`Set DEFAULT lang [${req.lang}]`));

			// Если нет куки, определить страну по IP и подставить страну из таблицы

			// const cookie = new LangCookie(this.g).make({
			//     value: 'ru',
			// });
			// res.cookie(cookie.name, cookie.value, cookie.options);

			if (!req.lang) this.logger.warn('Res.lang is undefined');

			next();
		};
	}
}