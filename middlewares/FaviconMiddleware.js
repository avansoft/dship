/**
 * Based on https://github.com/expressjs/serve-favicon
 */

import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';
import { readFile } from 'fs';
import etag from 'etag';
import fresh from 'fresh';

export class FaviconMiddleware extends BaseMiddleware {
	constructor(g) {
		super(g);
	}

	handler() {
		return (req, res, next) => {
			const logger = this.logger;
			logger.debug('FaviconMiddleware -> handler');

			if (req.path !== '/favicon.ico') return next();

			const { filePath, maxAgeMs } = this.g.config.favicon;
						
			function createIcon(buf) {
				return {
					body: buf,
					headers: {
						'Cache-Control': 'public, max-age=' + Math.floor(maxAgeMs / 1000),
						'ETag': etag(buf),
					}
				};
			}

			function isFresh(req, res) {
				return fresh(req.headers, {
					'etag': res.getHeader('ETag'),
					'last-modified': res.getHeader('Last-Modified')
				});
			}

			readFile(filePath + '/favicon.ico', (err, buf) => {
				if (err) next(err);
				
				const icon = createIcon(buf);

				// Set http-headers
				res.set(icon.headers);

				// Freshness testing
				if (isFresh(req, res)) {
					res.statusCode = 304;
					res.end();
					return;
				}

				res.statusCode = 200;
				res.setHeader('Content-Length', icon.body.length);
				res.setHeader('Content-Type', 'image/x-icon');
				res.end(icon.body);
				return;
			});
		};
	}
}
