import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';
import { UserSessionDAO } from '../dao/UserSessionDAO.js';

export class CheckSessionMiddleware extends BaseMiddleware {
    constructor(g) {
        super(g);
    }

    handler() {
        return (req, res, next) => {
            const logger = this.logger;
            logger.debug('CheckSessionMiddleware -> handler()');

            const sid = req.cookies.sid;

            if (sid) {
                const userSessions = new UserSessionDAO(this.g);

                logger.trace(`Try to found userKey for session [${sid}]`);
                return userSessions
                    .getByKey(sid)
                    .then(userKey => {

                        if (!userKey) {
                            logger.warn(`Not found userKey for sessionID [${sid}]`)
                            return next();
                        }

                        logger.trace(`Set req.session for userKey [${userKey}]`);
                        req.session = Object.freeze({
                            sid,
                            userKey,
                        })

                        const sessionLT = this.g.config.userSessionLT;

                        logger.trace(`Renew session cookie with LT [${sessionLT}] sec.`);
                        userSessions
                            .renew(sid, userKey, sessionLT)
                            .then(rs => {
                                logger.trace('Session has been updated')
                                return next();
                            });

                    }).catch(e => next(e));
            }
            logger.trace('Skipped because session cookie (sid) not found');
            next();
        }
    }
}
