import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';
import { ServerError } from '../subs/errors/ServerError.js';
import { BaseError } from '../core/lib/BaseError.js';
import { NotFoundError } from '../subs/errors/NotFoundError.js';

export class ErrorMiddleware extends BaseMiddleware {
	constructor(g) {
		super(g);
	}

	handler() {
		/**
         * Express requires that all error handle functions
         * should contain 4 arguments (even if not all of this
         * arguments are using)
         */
		// eslint-disable-next-line no-unused-vars
		return (err, req, res, next) => {
			const logger = this.logger;
			logger.debug('ErrorMiddleware -> handler');

			if (err instanceof NotFoundError) {
				/**
                 * Не всегда есть возможность передать
                 * ctx, поэтому данные берутся из
                 * ServerResponse (res)
                 */
				err.response(req, res);
			} else if (err instanceof BaseError) {
				err.response(res);
			} else {
				logger.trace('Unexpected error handling');
				const error = new ServerError(this.g, req);
				error.response(res);
			}
		};
	}
}
