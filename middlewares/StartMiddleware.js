import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';

export class StartMiddleware extends BaseMiddleware {
    constructor(g) {
        super(g);
    }

    handler() {
        return (req, res, next) => {
            this.logger.info(`New ${req.method}-request from ${req.ip} - ${Date.now()}`);
            next();
        }
    }
}