import { BaseMiddleware } from '../core/lib/BaseMiddleware.js';
import { TempUserDAO } from '../dao/TempUserDAO.js';

export class TempUserMiddleware extends BaseMiddleware {
    constructor(g) {
        super(g);
    }

    handler() {
        return (req, res, next) => {
            const logger = this.logger;
            logger.debug('TempUserMiddleware -> handler()');

            if (req.session) {
                logger.trace('Skipped because session exists');
                return next();
            }

            const tsid = req.cookies.tsid;

            if (!tsid) {
                logger.trace('Skipped because tempUser cookies not found');
                return next();
            }

            return new TempUserDAO(this.g)
                .getByKey(tsid)
                .then((res) => {
                    if (!res) {
                        logger.warn(`Not found tempUser with key ${tsid}`);
                        return next();
                    }

                    logger.trace(`Set req.tempUser with key ${tsid}`);
                    req.tempUser = Object.freeze({
                        key: tsid,
                        ...res,
                    })
                    next();
                }).catch(e => next(e));
        }
    }
}
