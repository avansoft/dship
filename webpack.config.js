import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import HtmlWebpackInjector from 'html-webpack-injector';

const srcPath = '/home/nodejs/dship/src';
const dstPath = '/home/nodejs/dship/public';

/**
 * Массив содержит имена страниц. Предполагается,
 * что для каждой страницы должна существовать
 * чанка (файл [ИмяСтраницы].js)
 */
const pages = [
	'MainPage',
	'Err400Page',
	'Err404Page',
	'Err500Page',
	'ActionSuccessPage',
	'FeedbackPage',
	'FeedbackSentPage',
	'ExcepSignupPage',
	'ExcepCookiesNotFoundPage',
	'ExcepUserAlreadyExistsPage',
	'LoginPage',
	'ProfilePage',
	'PasswordResetPage',
	'PasswordResetNextPage',
	'PasswordUpdatePage',
	'PasswordUpdateSuccessPage',
	'SignupPage',
	'SignupNextPage',
	'SignupCompletePage',
	'IssueListPage',
	'IssueCreatePage',
	'IssueItemPage',
];

function getEntryPoints() {
	const obj = {
		index: './entry.js',
	};

	pages.forEach(pageName => {
		obj[pageName] = `./pages/${pageName}/${pageName}.js`;
	});

	return obj;
}


function getPagePlugins() {
	const arr = [];

	pages.forEach(pageName => {
		/**
         * Пушим инстансы HtmlWebpackPlugin, поскольку
         * на каждую страницу требуется отдельный
         * экземпляр
         */
		arr.push(new HtmlWebpackPlugin({
			/** Относительный (от файла entry.js) путь к шаблону.
             * Здесь можно обратить внимание на отсутствие ./
             * Так же не следует забывать про постфикс Templ
             * у всехшаблонов.
             */
			template: `pages/${pageName}/${pageName}Templ.ejs`,
			/**
             * Путь относительно dstPath, в котором указывается
             * абсолютный путь до директории сборки
             */
			filename: `./views/${pageName}.njk`,
			/**
             * Название чанки (второй параметр) должно совпадать
             * с названием страницы (расширение не указывается)
             */
			//TODO сейчас не удается называть файлы по шаблону PageNameFront.js
			chunks: ['index', `${pageName}`],
		}));

		/**
         * Плагин добавляет к странице ссылку на чанку
         */
		arr.push(new HtmlWebpackInjector());
	});

	return arr;
}

export default {
	resolve: {
		fallback: {
			/**
			 * Отключает экспорт модулей из Node API.
			 * Например, во фронте есть ссылки на URI-классы, которые
			 * импортируют модули из области видимости Node. 
			 * */
			'buffer': false,
		}
	},
	context: srcPath,
	entry: getEntryPoints(),
	output: {
		filename: '[name].bundle.js',
		path: dstPath,
	},
	optimization: {
		minimize: false,
		// concatenateModules: true,
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: 'style.css'
		}),
		...getPagePlugins()
	],
	module: {
		rules: [
			{
				test: /.ejs$/,
				use: [
					{
						loader: 'ejs-compiled-loader',
						options: {
							htmlmin: false,
							htmlminOptions: {
								removeComments: true
							}
						}
					}]
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				}
			},
			{
				test: /\.s[ac]ss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			},
			{
				test: /\.woff(2)?$/,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[hash][ext][query]'
				}
			},
			{
				test: /\.(jpeg|jpg|png|gif)?$/,
				type: 'asset/resource',
				generator: {
					filename: 'images/[hash][ext][query]'
				}
			},
			{
				test: /\.ico$/,
				type: 'asset/resource',
				generator: {
					filename: 'images/[name][ext][query]'
				}
			},
			{
				test: /\.svg$/,
				type: 'asset/resource',
				generator: {
					filename: 'svg/[hash][ext][query]'
				}
			},
		]
	}
};