import validator from 'validator';

export default class Normalizer {
    static email(v) {
        return v.toLowerCase().trim();
    }

    static removeSpaces(v) {
        return v.replace('\W+\g', '');
    }
}