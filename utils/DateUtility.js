export default class DateUtility {
    static tomorrow(days) {
        const today = new Date();
        const year = today.getFullYear();
        const month = today.getMonth();
        const day = today.getDate();
        return new Date(year, month, day + days);
    }
}