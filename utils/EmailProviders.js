const domains = {
    'gmail.com': 'https://mail.google.com/mail/u/0/#inbox',
    'mail.ru': 'https://e.mail.ru/inbox/',
    'yandex.ru': 'https://mail.yandex.ru/#inbox',
};

export default class EmailProviders {
    static getDomain(email) {
        return email.replace(/\S+@/, '');
    }

    static getLink(email) {
        return domains[this.getDomain(email)];
    }

    static getLinkByDomain(domain) {
        return domains[domain];
    }
}
