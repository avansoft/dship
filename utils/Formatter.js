export default class Formatter {
    static phone(v) {
        const len = v.length;
        let f = v.split('');
        if (len == 11) {
            f.splice(1, '', '(');
            f.splice(5, '', ')');
            f.splice(9, '', '-');
            f.splice(12, '', '-');
        } else if (len == 12) {
            f.splice(2, '', '(');
            f.splice(6, '', ')');
            f.splice(10, '', '-');
            f.splice(13, '', '-');
        }
        return '+' + f.join('');
    }
}
