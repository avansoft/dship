import crypto from 'crypto';
import { v4 as uuid } from 'uuid';

export default class TokenUtility {
    static getUUID() {
        return uuid();
    }

    static getNumbers(length) {
        // Вернет числовой код для отправки по СМС
        // https://stackoverflow.com/questions/33609404/node-js-how-to-generate-random-numbers-in-specific-range-using-crypto-randomby
    }

    static getRandomString(length) {
        return crypto.randomBytes(length).toString('hex');
    }
}

//console.log(TokenUtility.getEmailValidationToken(15));