import crypto from 'crypto';
import util from 'util';
const cryptoPbkdf2 = util.promisify(crypto.pbkdf2);

export class PasswordUtility {
    constructor(g) {
        this.saltLength = g.config.password.saltLength;
        this.iterations = g.config.password.iterations;
        this.hashLength = g.config.password.hashLength;
        this.algorithm = g.config.password.algorithm;
    }

    async getHashWithSalt(password) {
        // Создаем рандомную соль
        const salt = crypto.randomBytes(this.saltLength).toString('hex');

        // Хеш от пароля с этой солью
        const hash = (await cryptoPbkdf2(password, salt, this.iterations, this.hashLength, this.algorithm)).toString('hex');

        return { hash, salt };
    }

    async getHashBySalt(password, salt) {
        const hash = (await cryptoPbkdf2(password, salt, this.iterations, this.hashLength, this.algorithm)).toString('hex');
        return hash;
    }

    async match(password, masterHash, salt) {
        // Хешируем проверяемый пароль
        const hash = await this.getHashBySalt(password, salt);

        // Полученный хеш должен совпасть с хешем из базы
        return hash === masterHash ? true : false;
    }
}
