const { createHash } = await import('crypto');

export default class HashUtility {
    static async makeHash(data, alg) {
        return createHash(alg)
            .update(data)
            .digest('hex');
    }

    // static async sha1(data) {
    //     const hash
    //     const shaSum = createHash('sha1');
    //     shaSum.update(data);
    //     return shaSum.digest('hex')
    // }
}

// async function test () {
//     const hash = await HashUtility.makeHash('test', 'sha1');
//     return hash;
// }

//console.log(HashUtility.makeHash('test', 'sha256'));