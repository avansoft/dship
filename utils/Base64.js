import { Buffer } from 'buffer';

export default class Base64 {
	static encode(str) {
		return Buffer.from(str).toString('base64');
	}

	static decode(str) {
		return Buffer.from(str, 'base64').toString('ascii');
	}
}
