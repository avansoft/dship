import Minio from 'minio';

export class MinioDriverError extends Error {
	constructor(message) {
		super(message);
	}
}

export class MinioDriver {
	constructor(config) {
		this.client = new Minio.Client(config);
	}

	saveFileFromBuffer(bucketName, objectName, buffer) {
		/**
         * Ошибку сохранения лучше обрабатывать на вызывающей стороне,
         * т.к. при ее появлении происходит удаление записи в БД
         */
		return new Promise((resolve, reject) => {
			// https://docs.min.io/docs/javascript-client-api-reference.html#putObject
			this.client.putObject(bucketName, objectName, buffer, (err, objInfo) => {
				if (err) reject(new MinioDriverError(err.message));
				resolve(objInfo);
			});
		});
	}


	removeFile(bucketName, objectName) {
		return new Promise((resolve, reject) => {
			// https://docs.min.io/docs/javascript-client-api-reference.html#removeObject
			this.client.removeObject(bucketName, objectName, err => {
				if (err) reject(new MinioDriverError(err.message));
				resolve();
			});
		});
	}

	getBucketsList() {
		return new Promise((resolve, reject) => {
			// https://docs.min.io/docs/javascript-client-api-reference.html#listBuckets
			this.client.listBuckets((err, buckets) => {
				if (err) reject(new MinioDriverError(err.message));
				resolve(buckets);
			});
		});
	}
}
