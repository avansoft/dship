import pkg from 'pg';
const { Pool } = pkg;

export class PostgresDriverError extends Error {
	constructor(sql) {
		super(`Uploader Serv -> Postgres Driver: ${sql}`);
	}
}

export class PostgresDriver {
	constructor(config) {
		this.pool = new Pool(config);
		this.pool.on('error', err => {
			console.error('Uploader Service -> Postgres Driver: ', err);
		});
	}

	async getVersion() {
		const sql = 'SELECT version();';

		try {
			const client = await this.pool.connect();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0].version;
		} catch (err) {
			throw new PostgresDriverError(sql);
		}
	}

	async createFileDigest(obj) {
		/**
         * Ошибка пробрасывается на вызывающую сторону (слой бизнес-логики),
         * чтобы иметь возможность удалить объект из файлового хранилища
         */
		const json = JSON.stringify(obj);
		const sql = `CALL srv_uploader.create_file_digest(null, '${json}');`;

		try {
			const client = await this.pool.connect();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0].p_result;
		} catch (err) {
			throw new PostgresDriverError(sql);
		}
	}

	async getFileDigestByHash(hash) {
		const json = JSON.stringify({ hash });
		const sql = `SELECT * FROM srv_uploader.get_file_digest_by_hash('${json}');`;

		try {
			const client = await this.pool.connect();
			const rs = await client.query(sql);
			client.release();
			return rs.rows[0]['get_file_digest_by_hash'];
		} catch (err) {
			throw new PostgresDriverError(sql);
		}
	}

	async getFolderToSave(obj) {
		const json = JSON.stringify(obj);
		const sql = `SELECT * FROM srv_uploader.recof_folder_to_save('${json}');`;

		try {
			const client = await this.pool.connect();
			const rs = await client.query(sql);
			client.release();
			const obj = rs.rows[0];
			return {
				folderId: obj.t_id,
				folderName: obj.t_stored_name,
			};
		} catch (err) {
			throw new PostgresDriverError(sql);
		}
	}
}
