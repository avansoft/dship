import Base64 from '../../utils/Base64.js';

export class UploaderServiceLinker {
	constructor(preset) {
		if (!preset) throw new Error(`${this.constructor.name} -> Preset object is required`);
		this.preset = preset;

		function presetChecker(preset) {
			if (!['sync', 'async', 'auto'].includes(preset.decoding)) throw new Error(
				`${this.constructor.name} -> Param decoding='${preset.decoding}' is unavailable`
			);

			if (!['lazy', 'eager'].includes(preset.loading)) throw new Error(
				`${this.constructor.name} -> Param loading='${preset.loading}' is unavailable`
			);
		}

		presetChecker(this.preset);
	}

	getLinks(fileDigestList) {
		if (!Array.isArray(fileDigestList)) throw new Error(
			`${this.constructor.name} -> getUrls() -> fileDigestList param should be an array`
		);

		return fileDigestList.map(v => {
			return new Object({
				id: v.fileDigestId,
				src: this._getTeaserLink(v.storedName, v.bucketName, v.folderName),
				fs: this._getFullsizeLink(v.storedName, v.bucketName, v.folderName),
				alt: v.altText || '',
				decoding: this.preset.decoding,
				loading: this.preset.loading,
			});
		});
	}

	_getTeaserLink(storedName, bucketName, folderName) {
		const prefix = '/teaser/';
		const paramsPart = this.preset.params ? this.preset.params + '/' : '';
		const folderPart = folderName ? folderName + '/' : '';
		const sourceUrl = Base64.encode('s3://' + bucketName + '/' + folderPart + storedName);
		const extension = this.preset.extension || '';

		return prefix + paramsPart + sourceUrl + extension;
	}

	_getFullsizeLink(storedName, bucketName, folderName) {
		const prefix = '/img/';
		const folderPart = folderName ? folderName + '/' : '';
		const sourceUrl = bucketName + '/' + folderPart + storedName;

		return prefix + sourceUrl;
	}
}