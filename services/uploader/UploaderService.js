import { MinioDriver, MinioDriverError } from './drivers/MinioDriver.js';
import { PostgresDriver, PostgresDriverError } from './drivers/PostgresDriver.js';
import HashUtility from '../../utils/HashUtility.js';

export class UploaderService {
	constructor(config) {
		if (!config) throw new Error('Config is required');

		this.minioDriver = new MinioDriver(config.minio);
		this.postgresDriver = new PostgresDriver(config.postgres);

		/**
         * Имя и идентификатор корневой папки, которая предусмотрена
         * для сервиса в объектном хранилище. Все объекты, с которыми
         * работает сервис, группируются по директориям внутри
         * этого корня
         */
		this.bucketName = config.bucketName;
		if (!this.bucketName) throw new Error('Not found config.bucketName');

		this.bucketId = config.bucketId;
		if (!this.bucketId) throw new Error('Not found config.bucketId');
	}

	async test() {
		console.log('-------- Uploader Service Test --------');
		try {
			// Minio test
			const buckets = await this.minioDriver.getBucketsList();
			console.log('Available buckets: ', buckets);

			// Postgres test
			const version = await this.postgresDriver.getVersion();
			console.log('Postgres version: ', version);

		} catch (err) {
			if (err instanceof MinioDriverError) {
				console.error('Minio test filed: ', err);
			} else if (err instanceof PostgresDriverError) {
				console.error('Postgres test filed: ', err);
			} else {
				console.error('Unknown error: ', err);
			}
		}
	}

	// Upload file from the stream/buffer
	async saveFile(file) {
		// Make hash for file
		const hash = await HashUtility.makeHash(file.buffer, 'sha256');
		let objName;
		let objInfo;

		/**
         * Ошибки драйверов обрабатываются на уровне бизнес-логики
         */
		try {
			/**
             * Если файл с таким хешем существует, возвращаем его ID
             */
			let fileId = await this.postgresDriver.getFileDigestByHash(hash);
			if (fileId) {
				console.log(`File with hash [${hash}] already exsists with id [${fileId}]`);
				return fileId;
			}

			// Get folder name
			const folderObj = await this.postgresDriver.getFolderToSave({
				bucketId: this.bucketId,
			});

			if (!folderObj.folderName) throw new Error('Param folder.name is empty');

			/**
             * Непосредственно имя файла в файловой системе.
             * Для простоты равно хешу.
             */
			const fileName = hash;

			/**
             * В качестве имени объекта можно передавать путь, чтобы поместить
             * файл в нужную директорию. Имя директории вычисляется на стороне
             * БД. Без разбивки на директории все файлы будут сваливаться в корневую
             * папку, что замедлит работу файловой системы
             */
			objName = folderObj.folderName + '/' + fileName;
			objInfo = await this.minioDriver.saveFileFromBuffer(this.bucketName, objName, file.buffer);
			console.log(`File saved to the object storage with etag [${objInfo.etag}] and path [${objName}]`);

			fileId = await this.postgresDriver.createFileDigest({
				hash,
				storedName: fileName,
				bucketId: this.bucketId,
				folderId: folderObj.folderId,
				originalName: file.originalname,
				mimeType: file.mimetype,
				size: file.size,
			});
			console.log(`File digest with id [${fileId}] created`);

			return fileId;

		} catch (err) {
			if (err instanceof PostgresDriverError) {
				/**
                 * Если успели сохранить файл в хранилище,
                 * пробуем удалить его
                 */
				if (objInfo) {
					this._removeFile(this.bucketName, objName);
				}
			}
			throw err;
		}
	}

	async _removeFile(bucketName, objName) {
		try {
			await this.minioDriver.removeFile(bucketName, objName);
			console.log(`Removed file with name [${objName}]`);
		} catch (err) {
			console.error(`Can't remove file with name [${objName}]`, err);
		}
	}
}