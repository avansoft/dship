import nunjucks from 'nunjucks';
import TokenUtility from '../../../utils/TokenUtility.js';

export class BaseEmail {
    constructor(ctx, config, props) {
        this.props = props;
        this.placeholders = {};
        this.lang = ctx.lang;

        this.emailTemplatesPath = config.emailTemplatesPath;
        if (!this.emailTemplatesPath) {
            throw new Error('Not found emailTemplatesPath in config');
        }

        this.defaultDict = {
            emailIdTitle: {
                ru: 'ID письма',
                en: 'E-mail ID',
            },
            spamReportTitle: {
                ru: 'Сообщить о спаме',
                en: 'Spam report',
            },
        };

        if (props.to) {
            if (Array.isArray(props.to)) {
                if (props.to.length < 1) {
                    throw new Error('Array with recivers can\'t be empty');
                }
            } else if (typeof props.to !== 'string') {
                throw new Error('Param "to" should be array or string');
            }
        } else {
            throw new Error('Param "to" is required');
        }

        if (!this.dict.subject) {
            throw new Error('Property subject is necessity for dict');
        }

        // Filling from the instanse end default dictionary
        for (const key in this.dict) {
            this.placeholders[key] = this.dict[key][this.lang] || key;
        }

        for (const key in this.defaultDict) {
            this.placeholders[key] = this.defaultDict[key][this.lang] || key;
        }

        // Adding props
        for (const key in this.props) {
            this.placeholders[key] = this.props[key];
        }

        // Generating email id
        this.placeholders.emailId = TokenUtility.getRandomString(15);

        return {
            from: this.props.from || 'dship <no-reply@dship.ru>',
            to: this.props.to,
            subject: this.dict['subject'][this.lang] || 'A message from dship',
            text: this._render('txt'),
            html: this._render('html'),
        };
    }

    _render(type) {
        const folderName = this.constructor.name;
        const path = this.emailTemplatesPath.concat('/', folderName);
        const compiler = nunjucks.configure(path);

        const getFileName = () => {
            switch (type) {
                case 'txt': return this.constructor.name + 'TextTempl.txt';
                case 'html': return this.constructor.name + 'HtmlTempl.html';
                default: throw new Error('Unknown file type');
            }
        };

        return compiler.render(getFileName(), this.placeholders);
    }
}