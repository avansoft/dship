import { MailgunDriver } from './drivers/MailgunDriver.js';
import { PasswordResetEmail } from './emails/PasswordResetEmail/PasswordResetEmail.js';
import { SignupConfirmEmail } from './emails/SignupConfirmEmail/SignupConfirmEmail.js';
import { WelcomeEmail } from './emails/WelcomeEmail/WelcomeEmail.js';

export class MailerService {
    constructor(config) {
        this.client = new MailgunDriver(config.mailgun);
        this.config = config;
    }

    async sendPasswordResetEmail(ctx, props) {
        return await this.client.send(new PasswordResetEmail(ctx, this.config, props));
    }

    async sendSignupConfirmEmail(ctx, props) {
        return await this.client.send(new SignupConfirmEmail(ctx, this.config, props));
    }

    async sendWelcomeEmail(ctx, props) {
        return await this.client.send(new WelcomeEmail(ctx, this.config, props));
    }
}