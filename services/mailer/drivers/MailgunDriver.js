// For USA server use https://api.mailgun.net/v3/mydomain.com
import Mailgun from 'mailgun.js';
import formData from 'form-data';

const mailgun = new Mailgun(formData);

export class MailgunDriver {
    constructor(config) {
        this.client = mailgun.client(config);
    }

    async send(email) {
        try {
            const result = await this.client.messages.create('dship.ru', email);
            if (result.id) return result.id;
            throw new Error(`E-mail didn't sent, sever response: ${result}`);
        } catch (e) {
            console.log('MailerService Error ->', e.message);
            console.dir(email);
        }
    }
}
