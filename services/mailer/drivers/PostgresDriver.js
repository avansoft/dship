import pkg from 'pg';
import MailServiceConfig from "../MailerServiceConfig.js";

const { Pool } = pkg;

export class PostgresDriver {
    constructor() {
        this.pool = new Pool(MailServiceConfig.postgres);
        this.pool.on('error', (err, client) => {
            console.error('StoreDB Postgres error', err);
        })
    }

    async _getClient() {
        try {
            /**
             * Этот метод обрабатывает только ошибку подключения.
             * Из-за этого приходится переупаковывать промис.
             */
            return await this.pool.connect();
        } catch (e) {
            throw new Error(`MailService -> poolConnect() ->`, e.message);
        }
    }

    async createSentReport(obj) {

    }

    async createSpamReport(obj) {
        this.logger.trace(`${this.constructor.name} -> createSpamReport()`);

        const sql = 'SELECT null;';

        try {
            const client = await this._getClient();
            const rs = await client.query(sql);
            client.release();
            return rs.rows[0];
        } catch (e) {
            throw new Error(`StoreDB -> isTrue() -> ${sql}`);
        }
    }
}
