import { BaseEmail } from '../../abstracts/BaseEmail.js';

export class PasswordResetEmail extends BaseEmail {
    constructor(ctx, config, props) {
        super(ctx, config, props);
    }

    get dict() {
        return {
            subject: {
                ru: 'Новый пароль',
                en: 'New password',
            },
            hello: {
                ru: 'Привет,',
                en: 'Hello',
            },
            message: {
                ru: 'Мы отправили это письмо, потому что получили запрос на смену вашего пароля. Если вы не хотели менять пароль, просто игнорируйте это сообщение. Чтобы завершить процедуру смены пароля, перейдите по ссылке:',
                en: 'We sent this letter, because we received a request for a password change. If you don\'t want to change the password, just ignore this message. To complete the password change procedure, click on this link:',
            },
            btnTitle: {
                ru: 'Сменить пароль',
                en: 'Change password',
            },
        };
    }
}

