import { BaseEmail } from '../../abstracts/BaseEmail.js';

export class SignupConfirmEmail extends BaseEmail {
    constructor(ctx, config, props) {
        super(ctx, config, props);
    }

    get dict() {
        return {
            subject: {
                ru: 'Завершите регистрацию в dship',
                en: 'Finish your registration in dship',
            },
            header: {
                ru: 'Регистрация почти завершена!',
                en: 'Registration is almost completed!',
            },
            content: {
                ru: 'Перейдите по этой ссылке, чтобы подтвердить электронную почту и задать пароль для входа в ваш личный кабинет',
                en: 'Click on below link for confirm your email and set a password for your account',
            },
            buttonTitle: {
                ru: 'Звершить регистрацию',
                en: 'Confirm registration',
            },
        };
    }
}

