import { BaseEmail } from '../../abstracts/BaseEmail.js';

export class WelcomeEmail extends BaseEmail {
    constructor(ctx, config, props) {
        super(ctx, config, props);
    }

    get dict() {
        return {
            subject: {
                ru: 'Добро пожаловать в dship!',
                en: 'Welcome to dship!',
            },
            header: {
                ru: 'Добро пожаловать!',
                en: 'Welcome!',
            },
            content: {
                ru: 'Мы только что создали для вас учетную запись и теперь вы можете пользоваться всеми функциями личного кабинета.',
                en: 'We have just created an account for you and now you can use all the functions of a personal account.',
            },
            btnTitle: {
                ru: 'Открыть сайт',
                en: 'Open the site',
            },
        };
    }
}

