// Formats
// 0 - нормальный
// 1 - жирный
// 2 - полупрозрачный
// 3 - наклонный
// 4 - подчеркнутый
// 7 - инверсия цвета
// 9 - зачеркнутый

// Colors
// 30 - черный
// 31 - красный
// 32 - зеленый
// 33 - желтый
// 34 - темно-голубой
// 35 - пурпурный
// 36 - голубой
// 37 - ярко-белый
// 38 - белый

// 40 - белый на черном
// 41 - белый на красном
// 45 - белый на пурпурном

export class LoggerService {
    constructor() {
        this.stream = process.stdout;

        this.format = {
            whiteNormal: '0;37',
            whiteBold: '1;37',
            whiteBoldUnderline: '4;37',
            darkWhite: '0;38',
            darkWhiteTransparent: '2;38',
            darkWhiteItalic: '3;38',
            redNormal: '0;31',
            redBold: '1;31',
            redNormalUnderline: '4;31',
            darkBlueNormal: '0;34',
            lightBlueNormal: '0;36',
            lightBlueNormalUnderline: '4;36',
            lightGray: '1;30',
            darkGrayBold: '1;30',
            greenNormal: '0;32',
            greenUnderline: '4;32',
            yellowNormal: '0;33',
            yellowUnderline: '4;33',
            whiteOverRed: '0;41',
            whiteOverRedBold: '1;41',
            whiteOverRedItalic: '3;41',
            blackOverYellow: '7;33',
            grayOverBlack: '2;40',
            whiteOverGreen: '1;42',
        }
    }

    _print = (descr, format) => {
        this.stream.write(`\x1b[${format}m${descr}\x1b[0m\n`);
    }

    error(descr) {
        this._print(descr, this.format.redBold);
    }

    info(descr) {
        this._print(descr, this.format.greenUnderline);
    }

    warn(descr) {
        this._print('WARN: ' + descr, this.format.redNormal);
    }

    debug(descr) {
        this._print(descr, this.format.grayOverBlack);
    }

    trace(descr) {
        this._print(descr, this.format.lightGray);
    }

    action(name) {
        this._print(`Start ${name}`, this.format.blackOverYellow);
    }

    step(descr) {
        this._print(descr, this.format.yellowNormal);
    }

    end(name) {
        this._print('Send result as [ ' + name + ' ]', this.format.yellowUnderline);
    }

    test(code) {
        this._print(`This is a color test of code ${code}`, code);
    }

    testColors() {
        for (const key in this.format) {
            this._print(`${key}`, this.format[key]);
        }
    }

    value(variable) {
        if (variable instanceof Object) {
            console.log('Значение объекта:');
            console.dir(variable);
        }
        console.log('Значение переменной: ', variable);
    }
}
